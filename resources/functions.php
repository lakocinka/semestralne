<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 21.05.2017
 * Time: 10:16
 */


function generateTable($lang,$typ){

    require '../../database/config.php';
    $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . ';charset=utf8';
    $user = SQL_USERNAME;
    $password = SQL_PASSWORD;

    try {
        $conn = new PDO($dsn, $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch
    (PDOException $e) {
        die('Connection failed: ' . $e->getMessage());
    }
    $dotaz = $conn->prepare("SELECT `projects`.* FROM `projects` ORDER BY `projects`.`endDate` DESC");
    $dotaz->execute();
    $rows = $dotaz->fetchAll(PDO::FETCH_ASSOC);

    //  var_dump($rows);
    $i=1;

    foreach ($rows as $val ){



        if($lang) {

            if($val["Type"]==$typ) {
                echo "<tr>";

                echo "<td>" . $val["number"] . "</td>
                                <td><a class='" . $val["id"] . "_number' href='#'>" . $val["titleSK"] . "</a></td>
                        <td>" . substr($val["startDate"], 0, 4) . " - " . substr($val["endDate"], 0, 4) . "</td>
                        <td>" . $val["coordinator"] . "</td>";
                echo "</tr>";
            }

        }else{

            if($val["Type"]==$typ) {

                echo "<tr>";
                echo "<td>" . $val["number"] . "</td>
                        <td><a class='" . $val["id"] . "_number' href='#'>" . $val["titleEN"] . "</a></td>
                        <td>" . substr($val["startDate"], 0, 4) . " - " . substr($val["endDate"], 0, 4) . "</td>
                        <td>" . $val["coordinator"] . "</td>";
                echo "</tr>";
            }
        }



        $i++;
    }

    $conn=null;
}

function generateTableInternational($lang){
    require '../../database/config.php';
    //require '../../database/config.php';
    $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . ';charset=utf8';
    $user = SQL_USERNAME;
    $password = SQL_PASSWORD;

    try {
        $conn = new PDO($dsn, $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    } catch
    (PDOException $e) {
        die('Connection failed: ' . $e->getMessage());
    }

    $dotaz = $conn->prepare("SELECT `projects`.* FROM `projects` ORDER BY `projects`.`endDate` DESC");
    $dotaz->execute();
    $rows = $dotaz->fetchAll(PDO::FETCH_ASSOC);

    //  var_dump($rows);
    $i=1;

    foreach ($rows as $val ){

        if($lang) {

            if($val["Type"]!="VEGA" && $val["Type"]!="KEGA" && $val["Type"]!="APVV" && $val["Type"]!="Iné domáce projekty") {

                echo "<tr>";
                echo "<td>" . $val["number"] . "</td><td><a class='". $val["id"]. "_number'>" . $val["titleSK"] . "</a></td><td>" . substr($val["startDate"], 0, 4) . " - " . substr($val["endDate"], 0, 4) . "</td><td>" . $val["coordinator"] . "</td>";
                echo "</tr>";
            }

        }else{

            if($val["Type"]!="VEGA" && $val["Type"]!="KEGA" && $val["Type"]!="APVV" && $val["Type"]!="Iné domáce projekty") {

                echo "<tr>";
                echo "<td>" . $val["number"] . "</a></td>
                        <td><a class='" . $val["id"]. "_number' href='#'>" . $val["titleEN"] . "</a></td>
                        <td>" . substr($val["startDate"], 0, 4) . " - " . substr($val["endDate"], 0, 4) . "</td>
                        <td>" . $val["coordinator"] . "</td>";
                echo "</tr>";
            }
        }



        $i++;
    }

    $conn=null;
}

function generateModal($lang,$t){
   // require '../../database/config.php';
    require '../../database/config.php';
    $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . ';charset=utf8';
    $user = SQL_USERNAME;
    $password = SQL_PASSWORD;

    try {
        $conn = new PDO($dsn, $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    } catch
    (PDOException $e) {
        die('Connection failed: ' . $e->getMessage());
    }

    $dotaz = $conn->prepare("SELECT `projects`.* FROM `projects` ORDER BY `projects`.`endDate` DESC");
    $dotaz->execute();
    $rows = $dotaz->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rows as $value){


            if($value["id"]==$t){

                if($lang) {
                    echo "<div class='title'>" . $value["titleSK"] . "</div>
                                 <div class='coordinator'>" . $value["coordinator"] . "</div>
                                <div class='type'>" . $value["Type"] . "</div>
                                <div class='startDate'>" . $value["startDate"] . "</div>
                                <div class='endDate'>" . $value["endDate"] . "</div>
                                <div class='partners'>" . $value["partners"] . "</div>
                                <div class='web'>" . $value["web"] . "</div>
                                <div class='internalCode'>" . $value["internalCode"] . "</div>
                                <div class='annotation'>" . $value["annotationSK"] . "</div>
                                <div class='lang'>SK</div>";
                }else{

                    echo "<div class='title'>" . $value["titleEN"] . "</div>
                                <div class='coordinator'>" . $value["Type"] . "</div>
                                <div class='type'>" . $value["coordinator"] . "</div>
                                <div class='startDate'>" . $value["startDate"] . "</div>
                                <div class='endDate'>" . $value["endDate"] . "</div>
                                <div class='partners'>" . $value["partners"] . "</div>
                                <div class='web'>" . $value["web"] . "</div>
                                <div class='internalCode'>" . $value["internalCode"] . "</div>
                                <div class='annotation'>" . $value["annotationEN"] . "</div>
                                <div class='lang'>EN</div>";
                }

            }
        }



}