<?php

if(isset($_POST['id'])){
    getShoppingData($_POST['id']);
}

if(isset($_POST['saveContent']) && isset($_POST['contentID'])){
    updateShopping($_POST['contentID'], $_POST['saveContent'], $_POST['title']);
}

if(isset($_POST['deleteShopping'])){
    deleteShopping($_POST['deleteShopping']);
}

function createTableShopping()
{
    require "../../../database/config.php";

    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT id, title, date FROM shopping ORDER BY date DESC ";
    $result = $conn->query($sql);
    $numRow = 1;
    echo "<table id='shoppingTable' class='table table-bordered'><thead><th>#</th><th>Názov</th><th>Dátum</th></thead><tbody class='shoppingTableBody'>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td>$numRow</td><td><a href='#' class='shoppingTitle' 
                                                  data-id='" . $row['id'] . "'
                                                  data-toggle='modal'
                                                  data-target='.universal'
                                                  data-title='".$row['title']."'>" . $row['title'] . "</a></td><td>" . $row['date'] . "</td></tr>";
        $numRow++;
    }
    echo "</tbody></table>";


}

function insertToShoppingTable($title, $content)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "INSERT INTO shopping (title, content) VALUES ('" . $title . "','" . $content . "')";
    $conn->query($sql);
    header("Location: nakupy.php");
    unset($_POST);
}

function getShoppingData($id){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM shopping WHERE id=" . $id;
    $result = $conn->query($sql);

    $res = array();
    $row = $result->fetch_assoc();
    array_push($res,$row['title']);
    array_push($res,$row['content']);
    print_r(json_encode($res));

}

function updateShopping($id, $content, $title){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "UPDATE shopping SET content='". $content . "', title='".$title."' WHERE id=" . $id;
    $conn->query($sql);
}

function deleteShopping($id){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "DELETE FROM shopping WHERE id=". $id;
    $conn->query($sql);
}