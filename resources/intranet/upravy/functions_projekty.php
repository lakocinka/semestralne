<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 25.05.2017
 * Time: 11:54
 */



function createTableAkt()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM projects ORDER BY startDate DESC ";
    $result = $conn->query($sql);
    $numRow = 1;
    echo "<table id='shoppingTable' class='table table-bordered'><thead><th>#</th><th>Názov - slovenský</th><th>Názov - anglický</th><th>Typ projektu</th><th>Začiatok projektu</th><th>Koordinátor</th><th>Číslo projektu</th></thead><tbody class='shoppingTableBody'>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td>$numRow</td><td><a href='#' class='projectTitle' 
                                                  data-id='" . $row['id'] . "'
                                                  data-toggle='modal'
                                                  data-target='.universal'
                                                  data-title='".$row['titleSK']."'>" . $row['titleSK'] . "</a></td><td>" . $row['titleEN'] . "</td><td>".$row["Type"]."</td><td>".$row['startDate']."</td><td>".$row['coordinator']."</td><td>".$row["number"]."</td></tr>";
        $numRow++;
    }
    echo "</tbody></table>";


}

function getProjektData($id){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");


    $sql = "SELECT * FROM projects WHERE id=".$id;
    $result = $conn->query($sql);

    $res = array();
    $row = $result->fetch_assoc();
    //array_push($res,$row['titleSK']);
    // array_push($res,$row['titleEN']);
    //array_push($res,$row['textSK']);
    //array_push($res,$row['textEN']);
    // print_r(json_encode($res));

    //var_dump($res);

    echo "<div class='nadpisSK'>".$row['titleSK']."</div>";
    echo "<div class='nadpisEN'>".$row['titleEN']."</div>";
    echo "<div class='datumEnd'>".$row['endDate']."</div>";
    echo "<div class='coordinator'>".$row['coordinator']."</div>";
    echo "<div class='partners'>".$row['partners']."</div>";
    echo "<div class='web'>".$row['web']."</div>";
    echo "<div class='annotationSK'>".$row['annotationSK']."</div>";
    echo "<div class='annotationEN'>".$row['annotationEN']."</div>";

}
function deleteProjekt($id){
    require "../../../database/config.php";

    echo "toto je ID".$id;

    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "DELETE FROM projects WHERE id=". $id;
    $conn->query($sql);
}
function updateProjekt($titleSK, $contentSK, $titleEN,$contentEN,$id,$coordinator,$partners,$date,$web){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "UPDATE projects SET titleSK='". $titleSK. "', annotationSK='".$contentSK."', titleEN='". $titleEN. "', annotationEN='".$contentEN."', coordinator='".$coordinator."', partners='".$partners."', endDate='".$date."', web='".$web."' WHERE id=" . $id;
    $conn->query($sql);
}
function insertProjekt($titleSK, $titleEN,$contentSK,$contentEN,$cat,$date,$coordinator,$partners,$web,$code,$number,$startDat)
{

    echo $number;
    echo $startDat;

    require "../../../database/config.php";


    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "INSERT INTO projects (titleSK, annotationSK,titleEN,annotationEN,Type,endDate,coordinator,partners,web,internalCode,number,startDate) VALUES ('" . $titleSK . "','" . $contentSK . "','" . $titleEN . "','" . $contentEN . "','" . $cat . "','" .$date . "','" .$coordinator . "','" .$partners . "','" .$web . "','" .$code. "','" .$number. "','" .$startDat. "')";
    $conn->query($sql);




}