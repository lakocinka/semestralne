<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("location: login/login.php");
}
if (!in_array("admin", $_SESSION['role']) && !in_array("hr", $_SESSION['role'])) {
    header("location: ../index.php");
}
require "../functions/roles.php";
require "./functions.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Intranet</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="../css/ltestyle.css">
    <link rel="stylesheet" href="../css/style.css">


    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <a href="../index.php" class="logo">
            <span class="logo-mini"><b>WEB</b></span>
            <span class="logo-lg"><b>WEB</b>tech</span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo getUserImagePath(); ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs">
                                <?php
                                userPanel();
                                ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo getUserImagePath(); ?>" class="img-circle" alt="User Image">

                                <p>
                                    <?php
                                    userMenu();
                                    ?>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="../profile/profile.php" class="btn btn-default btn-flat">Profil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="../login/logout.php" class="btn btn-default btn-flat">Odhlásiť</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">

        <section class="sidebar">

            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo getUserImagePath(); ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php
                        userPanel();
                        ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <ul class="sidebar-menu">
                <li class="header">Main navigation</li>
                <li class="treeview">
                    <a href=../index.php>
                        <i class="fa fa-home"></i> <span>Domov</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../pedagogika/pedagogika.php">
                        <i class="fa fa-graduation-cap"></i> <span>Pedagogika</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../doktorandi/doktorandi.php">
                        <i class="fa fa-group"></i> <span>Doktorandi</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../publikacie/publikacie.php">
                        <i class="fa fa-book"></i> <span>Publikácie</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../sluzobne_cesty/sluzobne_cesty.php">
                        <i class="fa fa-car"></i> <span>Sľužobné cesty</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../nakupy/nakupy.php">
                        <i class="fa fa-shopping-cart"></i> <span>Nákupy</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../dochadzka/dochadzka.php">
                        <i class="fa fa-table"></i> <span>Dochádzka</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../rozdelenie_uloh/rozdelenie_uloh.php">
                        <i class="fa fa-tasks"></i> <span>Rozdelenie úloh</span>
                    </a>
                </li>
                <?php
                if (in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                    echo ' <li class="treeview"><a href="../newsletter/newsletter.php"><i class="fa fa-envelope-o"></i> <span>Newsletter</span></a></li>';
                }
                if (in_array("reporter", $_SESSION['role']) || in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                    echo '<li class="active treeview">
                    <a href="#"><i class="fa fa-pencil-square-o"></i> <span>Úpravy</span></a>
                    <ul class="treeview-menu menu-open" style="display: block;">';
                    if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li class="active"><a href="../upravy/pouzivatelia.php"><i class="fa fa-circle-o"></i>Používatelia</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/aktuality.php"><i class="fa fa-circle-o"></i>Aktuality</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/Projekty.php"><i class="fa fa-circle-o"></i>Projekty</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/fotky.php"><i class="fa fa-circle-o"></i>Fotky</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/videa.php"><i class="fa fa-circle-o"></i>Videá</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role']) || in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/media.php"><i class="fa fa-circle-o"></i>Média</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/oNas.php"><i class="fa fa-circle-o"></i>O nás</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/vyskumne_oblasti.php"><i class="fa fa-circle-o"></i>Výskumné oblasti</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/studium.php"><i class="fa fa-circle-o"></i>Štúdium</a></li>';
                    }
                    echo '</ul></li>';
                }
                ?>


            </ul>
        </section>
    </aside>


    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Používatelia
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-file"></i> Intranet</a></li>
                <li class="active">Používatelia</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <a class="quick-access" name="add_category" id="addUserButton" href="#" data-toggle="modal"
                   data-target=".userModal">
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12 quick-select">
                        <div class="small-info-box">
                            <span class="small-info-box-icon bg-green"><i class="fa ion-plus-round"></i></span>

                            <div class="small-info-box-content">
                                <span class="small-info-box-text">Pridať používateľa</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </a>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="box">
                        <div class="box-body">
                            <div class="col-sm-12" id="tableUsers">
                                <?php
                                createTableUsers();
                                ?>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.01
        </div>

    </footer>

</div>

<div class="modal fade universal" role="dialog" id="createModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h2 class="modal-title"><i>#</i><span id="userID"></span><span> - </span><span id="userName"></span>
                </h2>
            </div>
            <div class="modal-body" id="modalBody">
                <form id="userForm" action="#" method="post">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Meno: <input type="text" class="form-control" name="firstname"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Priezvisko: <input type="text" class="form-control" name="lastname"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Titul pred: <input type="text" class="form-control" name="title1"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Titul za: <input type="text" class="form-control" name="title2"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Ldap: <input type="text" class="form-control" name="ldap"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Fotka: <input type="text" class="form-control" name="photo"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Miestnosť: <input type="text" class="form-control" name="room"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Klapka: <input type="number" class="form-control" name="phone"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Oddelenie: <input type="text" class="form-control" name="department"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Rola: <input type="text" class="form-control" name="staffrole"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Funkcia: <textarea type="text" class="form-control"
                                                      name="function"></textarea></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Aktívny: <input type="checkbox" name="active"></label>
                        </div>

                        <?php
                        if (in_array("admin", $_SESSION['role'])) {
                            echo '<div class="form-group col-md-12" id="prava">
                            <label class="perm">Práva:</label>
                            
                            <label class="perm">Admin: <input type="checkbox" name="admin"></label>
                            
                            <label class="perm">Editor: <input type="checkbox" name="editor"></label>
                            
                            <label class="perm">HR: <input type="checkbox" name="hr"></label>
                            
                            <label class="perm">Reporter: <input type="checkbox" name="reporter"></label>
                            
                            <label class="perm">User: <input type="checkbox" name="user"></label>
                        </div>';
                        }
                        ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?php
                if (in_array("admin", $_SESSION['role'])) {
                    echo "<input type = 'submit' class='btn btn-danger pull-left' id = 'deleteUser' value = 'Vymazať'>";
                }
                ?>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModal">
                    Zavrieť
                </button>
                <?php
                if (in_array("admin", $_SESSION['role']) || in_array("hr", $_SESSION['role'])) {
                    echo '<input type="submit" value="Ulož" class="btn btn-success" id="submitUserModal">';
                }
                ?>

            </div>
        </div>
    </div>
</div>

<div class="modal fade userModal" role="dialog" id="newUserModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h2 class="modal-title"><span> Nový používateľ</span>
                </h2>
            </div>
            <div class="modal-body" id="newUserModalBody">
                <form id="newUserForm" action="#" method="post">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Meno: <input type="text" class="form-control" name="firstname"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Priezvisko: <input type="text" class="form-control" name="lastname"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Titul pred: <input type="text" class="form-control" name="title1"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Titul za: <input type="text" class="form-control" name="title2"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Ldap: <input type="text" class="form-control" name="ldap"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Fotka: <input type="text" class="form-control" name="photo"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Miestnosť: <input type="text" class="form-control" name="room"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Klapka: <input type="number" class="form-control" name="phone"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Oddelenie: <input type="text" class="form-control" name="department"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Rola: <input type="text" class="form-control" name="staffrole"></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Funkcia: <textarea type="text" class="form-control"
                                                      name="function"></textarea></label>
                        </div>
                        <?php
                        if (in_array("admin", $_SESSION['role'])) {
                            echo '<div class="form-group col-md-4" id="prava">
                            <label>Práva:</label>
                            <br>
                            <label>Admin: <input type="checkbox" name="admin"></label>
                            <br>
                            <label>Editor: <input type="checkbox" name="editor"></label>
                            <br>
                            <label>HR: <input type="checkbox" name="hr"></label>
                            <br>
                            <label>Reporter: <input type="checkbox" name="reporter"></label>
                            <br>
                            <label>User: <input type="checkbox" name="user"></label>
                        </div>';
                        }
                        ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="closeNewModal">
                    Zavrieť
                </button>
                <?php
                if (in_array("admin", $_SESSION['role']) || in_array("hr", $_SESSION['role'])) {
                    echo '<input type="submit" value="Ulož" class="btn btn-success" id="submitNewUserModal">';
                }
                ?>

            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- AdminLTE App -->
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.min.js"></script>
<script src="../js/tinymce/tinymce.min.js"></script>
<script src="../js/lte.js"></script>
<script src="../js/pouzivatelia.js"></script>


<script>
    $(function () {
        $("#userTable").DataTable({});
    });
</script>
</body>
</html>

