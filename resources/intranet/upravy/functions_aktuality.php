<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 24.05.2017
 * Time: 17:55
 */



function createTableAkt()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM Aktuality ORDER BY startDate DESC ";
    $result = $conn->query($sql);
    $numRow = 1;
    echo "<table id='shoppingTable' class='table table-bordered'><thead><th>#</th><th>Názov - slovenský</th><th>Názov - anglický</th><th>Kategória</th><th>Dátum priadnia</th><th>Dátum expirácie</th></thead><tbody class='shoppingTableBody'>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td>$numRow</td><td><a href='#' class='shoppingTitle' 
                                                  data-id='" . $row['id'] . "'
                                                  data-toggle='modal'
                                                  data-target='.universal'
                                                  data-title='".$row['titleSK']."'>" . $row['titleSK'] . "</a></td><td>" . $row['titleEN'] . "</td><td>".$row["kategoria"]."</td><td>".$row['startDate']."</td><td>".$row['endDate']."</td></tr>";
        $numRow++;
    }
    echo "</tbody></table>";


}
function getShoppingData($id){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");


    $sql = "SELECT * FROM Aktuality WHERE id=".$id;
    $result = $conn->query($sql);

    $res = array();
    $row = $result->fetch_assoc();
    //array_push($res,$row['titleSK']);
   // array_push($res,$row['titleEN']);
    //array_push($res,$row['textSK']);
    //array_push($res,$row['textEN']);
   // print_r(json_encode($res));

    //var_dump($res);

    echo "<div class='nadpisSK'>".$row['titleSK']."</div>";
    echo "<div class='nadpisEN'>".$row['titleEN']."</div>";
    echo "<div class='contentSK'>".$row['textSK']."</div>";
    echo "<div class='contentEN'>".$row['textEN']."</div>";

}
function updateAkt($titleSK, $contentSK, $titleEN,$contentEN,$id){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "UPDATE Aktuality SET titleSK='". $titleSK. "', textSK='".$contentSK."', titleEN='". $titleEN. "', textEN='".$contentEN."' WHERE id=" . $id;
    $conn->query($sql);
}

function deleteAkt($id){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "DELETE FROM Aktuality WHERE id=". $id;
    $conn->query($sql);
}
function insertAkt($titleSK, $contentSK,$titleEN,$contentEN,$cat,$date)
{


    require "../../../database/config.php";
    require "../functions/newslettr_functions.php";

    echo $_POST["titleSKK"];
    echo $_POST["titleEN"];
    echo $_POST["contentSKK"];
    echo $_POST["contentEN"];
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "INSERT INTO Aktuality (titleSK, textSK,titleEN,textEN,kategoria,endDate) VALUES ('" . $titleSK . "','" . $contentSK . "','" . $titleEN . "','" . $contentEN . "','" . $cat . "','" .$date . "')";
    $conn->query($sql);

    if($titleSK !="" && $titleSK !=null && $contentSK !="" && $contentSK !=null)
    {
        sendNews("sk",$titleSK,$contentSK);
    }
    if($titleEN !="" && $titleEN !=null && $contentEN !="" && $contentEN !=null)
    {
        sendNews("en",$titleEN,$contentEN);
    }



}