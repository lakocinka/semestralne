<?php
session_start();
if (isset($_POST['aboutType'])) {
    loadAboutTextFromDatabase($_POST['aboutType']);
}

if (isset($_POST['saveAboutContent']) && isset($_POST['type'])) {
    saveAboutTextToDatabase($_POST['saveAboutContent'], $_POST['type']);
}

if (isset($_POST['userIdHr'])) {
    getInfoAboutUser($_POST['userIdHr']);
}

if (isset($_POST['changeUserProfile']) && $_POST['userProfileID']) {
    saveUserProfile($_POST['changeUserProfile'], $_POST['userProfileID']);
}

if (isset($_POST['researchTypeSK'])) {
    loadResearchTextFromDatabaseSK($_POST['researchTypeSK']);
}

if (isset($_POST['researchTypeEN'])) {
    loadResearchTextFromDatabaseEN($_POST['researchTypeEN']);
}

if (isset($_POST['saveResearchContentSK']) && isset($_POST['type'])) {
    saveResearchTextToDatabaseSK($_POST['saveResearchContentSK'], $_POST['type']);
}

if (isset($_POST['saveResearchContentEN']) && isset($_POST['type'])) {
    saveResearchTextToDatabaseEN($_POST['saveResearchContentEN'], $_POST['type']);
}
if (isset($_POST['studyTypeSK'])) {
    loadStudyTextFromDatabaseSK($_POST['studyTypeSK']);
}

if (isset($_POST['studyTypeEN'])) {
    loadStudyTextFromDatabaseEN($_POST['studyTypeEN']);
}

if (isset($_POST['saveStudyContentSK']) && isset($_POST['type'])) {
    saveStudyTextToDatabaseSK($_POST['saveStudyContentSK'], $_POST['type']);
}

if (isset($_POST['saveStudyContentEN']) && isset($_POST['type'])) {
    saveStudyTextToDatabaseEN($_POST['saveStudyContentEN'], $_POST['type']);
}

if (isset($_POST['deleteUser'])) {
    deleteUser($_POST['deleteUser']);
}

if (isset($_POST['addUser'])) {
    addUser($_POST['addUser']);
}

function createTableResearch()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM research";
    $result = $conn->query($sql);
    echo "<table id='researchTable' class='table table-bordered'><thead><th>Názov</th><th>Posledná zmena</th></thead><tbody class='shoppingTableBody'>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td><a href='#' class='researchTitle'
                                                  data-typ='" . $row['typ'] . "'
                                                  data-toggle='modal'
                                                  data-target='.universal'
                                                  data-title='" . $row['typ'] . "'>" . $row['typ'] . "</a></td><td>" . $row['den'] . "</td></tr>";

    }
    echo "</tbody></table>";
}

function loadResearchTextFromDatabaseSK($typ)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM research WHERE typ ='" . $typ . "'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    echo $row['text_SK'];

}

function loadResearchTextFromDatabaseEN($typ)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM research WHERE typ ='" . $typ . "'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    echo $row['text_EN'];

}

function saveResearchTextToDatabaseSK($content, $type)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "UPDATE research SET text_SK='" . $content . "' WHERE typ='" . $type . "'";
    echo $sql;
    $conn->query($sql);

}

function saveResearchTextToDatabaseEN($content, $type)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "UPDATE research SET text_EN='" . $content . "' WHERE typ='" . $type . "'";
    echo $sql;
    $conn->query($sql);

}

function createTableStudy()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM study";
    $result = $conn->query($sql);
    echo "<table id='studyTable' class='table table-bordered'><thead><th>Názov</th><th>Posledná zmena</th></thead><tbody class='shoppingTableBody'>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td><a href='#' class='studyTitle'
                                                  data-typ='" . $row['typ'] . "'
                                                  data-toggle='modal'
                                                  data-target='.universal'
                                                  data-title='" . $row['typ'] . "'>" . $row['typ'] . "</a></td><td>" . $row['den'] . "</td></tr>";

    }
    echo "</tbody></table>";
}

function loadStudyTextFromDatabaseSK($typ)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM study WHERE typ ='" . $typ . "'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    echo $row['text_SK'];

}

function loadStudyTextFromDatabaseEN($typ)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM study WHERE typ ='" . $typ . "'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    echo $row['text_EN'];

}

function saveStudyTextToDatabaseSK($content, $type)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "UPDATE study SET text_SK='" . $content . "' WHERE typ='" . $type . "'";
    echo $sql;
    $conn->query($sql);

}

function saveStudyTextToDatabaseEN($content, $type)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "UPDATE study SET text_EN='" . $content . "' WHERE typ='" . $type . "'";
    echo $sql;
    $conn->query($sql);

}

function createTableAbout()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM aboutus";
    $result = $conn->query($sql);
    echo "<table id='aboutTable' class='table table-bordered'><thead><th>Názov</th><th>Posledná zmena</th></thead><tbody class='shoppingTableBody'>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td><a href='#' class='aboutTitle'
                                                  data-typ='" . $row['typ'] . "'
                                                  data-toggle='modal'
                                                  data-target='.universal'
                                                  data-title='" . $row['typ'] . "'>" . $row['typ'] . "</a></td><td>" . $row['den'] . "</td></tr>";

    }
    echo "</tbody></table>";
}

function loadAboutTextFromDatabase($typ)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM aboutus WHERE typ ='" . $typ . "'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    echo $row['text'];
}

function saveAboutTextToDatabase($content, $type)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "UPDATE aboutus SET text='" . $content . "' WHERE typ='" . $type . "'";
    echo $sql;
    $conn->query($sql);

}

function createTableUsers()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM users";
    $result = $conn->query($sql);
    echo "<table id='userTable' class='table table-bordered'><thead><th>Meno a Priezvisko</th><th>LdapLogin</th></thead><tbody class='userTableBody'>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td><a href='#' class='usersTable'
                                                  data-id='" . $row['id'] . "'
                                                  data-toggle='modal'
                                                  data-target='.universal'
                                                  data-firstname='" . $row['firstname'] . "' 
                                                  data-lastname='" . $row['lastname'] . "'>" . $row['firstname'] . " " . $row['lastname'] . "</a></td><td>" . $row['ldapLogin'] . "</td></tr>";

    }
    echo "</tbody></table>";
}

function getInfoAboutUser($id)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "SELECT * FROM users WHERE id=" . $id;
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    print_r(json_encode($row));
}

function saveUserProfile($data, $id)
{
    $params = array();
    parse_str($data, $params);
    var_dump($params);

    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    if ($params['phone'] == null) {
        echo "phone je null";
        $params['phone'] = 'null';
    }

    if ($params['active'] == "on") {
        echo "phone je null";
        $params['active'] = '1';
    } else {
        $params['active'] = 'null';
    }

    $sql = "UPDATE users SET firstname='" . $params['firstname'] . "', 
                             lastname='" . $params['lastname'] . "',
                             title1='" . $params['title1'] . "',
                             title2='" . $params['title2'] . "',
                             ldapLogin='" . $params['ldap'] . "',
                             photo='" . $params['photo'] . "',
                             room='" . $params['room'] . "',
                             phone=" . $params['phone'] . ",
                             department='" . $params['department'] . "',
                             staffRole='" . $params['staffrole'] . "',
                             function='" . $params['function'] . "',
                             active=" . $params['active'] . " WHERE id='" . $id . "'";

    if (in_array("admin", $_SESSION['role'])) {
        $role = getRolePermission($params);

        var_dump($role);
        $sql = "UPDATE users SET firstname='" . $params['firstname'] . "', 
                             lastname='" . $params['lastname'] . "',
                             title1='" . $params['title1'] . "',
                             title2='" . $params['title2'] . "',
                             ldapLogin='" . $params['ldap'] . "',
                             photo='" . $params['photo'] . "',
                             room='" . $params['room'] . "',
                             phone=" . $params['phone'] . ",
                             department='" . $params['department'] . "',
                             staffRole='" . $params['staffrole'] . "',
                             function='" . $params['function'] . "',
                             active=" . $params['active'] . ",
                             user=" . $role['user'] . ",
                             editor=" . $role['editor'] . ",
                             hr=" . $role['hr'] . ",
                             reporter=" . $role['reporter'] . ",
                             admin=" . $role['admin'] . "
                             WHERE id='" . $id . "'";
    }
    echo $sql;
    $conn->query($sql);


}

function getRolePermission($param)
{
    $ret = array();
    if ($param['admin'] == "on") {
        $ret['admin'] = 1;
    } else {
        $ret['admin'] = 'null';
    }

    if ($param['editor'] == "on") {
        $ret['editor'] = 1;
    } else {
        $ret['editor'] = 'null';
    }

    if ($param['hr'] == "on") {
        $ret['hr'] = 1;
    } else {
        $ret['hr'] = 'null';
    }

    if ($param['user'] == "on") {
        $ret['user'] = 1;
    } else {
        $ret['user'] = 'null';
    }

    if ($param['reporter'] == "on") {
        $ret['reporter'] = 1;
    } else {
        $ret['reporter'] = 'null';
    }
    return $ret;
}

function deleteUser($id)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "DELETE FROM users WHERE id=" . $id;
    echo $sql;
    $conn->query($sql);
}

function addUser($data)
{
    require "../../../database/config.php";

    $params = array();
    parse_str($data, $params);
    var_dump($params);

    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "INSERT INTO users (firstname, lastname, title1, title2, ldapLogin, photo, room, phone, department, staffRole, function, user) 
            VALUES ('" . $params['firstname'] . "','" . $params['lastname'] . "','" . $params['title1'] . "','" . $params['title2'] . "','" . $params['ldap'] . "','" . $params['photo'] . "',
            '" . $params['room'] . "'," . $params['phone'] . ",'" . $params['department'] . "','" . $params['staffrole'] . "','" . $params['function'] . "', 1)";
    echo $sql;
    if (in_array("admin", $_SESSION['role'])) {
        $role = getRolePermission($params);
        if ($params['phone'] == null) {
            echo "phone je null";
            $params['phone'] = 'null';
        }
        $sql = "INSERT INTO users (firstname, lastname, title1, title2, ldapLogin, photo, room, phone, department, staffRole, function, user, hr, reporter, editor, admin) 
            VALUES ('" . $params['firstname'] . "','" . $params['lastname'] . "','" . $params['title1'] . "','" . $params['title2'] . "','" . $params['ldap'] . "','" . $params['photo'] . "',
            '" . $params['room'] . "'," . $params['phone'] . ",'" . $params['department'] . "','" . $params['staffrole'] . "','" . $params['function'] . "'," . $role['user'] .
            "," . $role['hr'] . "," . $role['reporter'] . "," . $role['editor'] . "," . $role['admin'] . ")";
        echo $sql;

    }
    $conn->query($sql);
}

function createCustomTable($table)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM $table";
    $result = $conn->query($sql);

    $object = array();

    switch ($table) {
        case 'media':
            echo "<table id='activity_table' class='table table-bordered'><thead><th>Názov</th><th>Typ</th><th>URL/Názov súboru</th><th>Dátum</th></thead><tbody class='activity_table-body'>";
            while ($row = $result->fetch_assoc()) {
                echo "<tr>
                         <td><a href='#' class='activity_data' data-id='" . $row['id'] . "' data-toggle='modal' data-target='.universal'> " . $row['title_sk'] . "</a></td>
                         <td>" . $row['type'] . "</td>
                         <td>" . $row['path'] . "</td>
                         <td>" . $row['date'] . "</td>
                      </tr>";
                array_push($object, $row);
            }
            break;
        case 'videos':
            echo "<table id='activity_table' class='table table-bordered'><thead><th>Názov</th><th>URL adresa</th><th>Dátum</th></thead><tbody class='activity_table-body'>";
            while ($row = $result->fetch_assoc()) {
                echo "<tr>
                         <td><a href='#' class='activity_data' data-id='" . $row['id'] . "' data-toggle='modal' data-target='.universal'> " . $row['title_sk'] . "</a></td>
                         <td>" . $row['url'] . "</td>
                         <td>" . $row['date'] . "</td>
                      </tr>";
                array_push($object, $row);
            }
            break;
        case 'photogallery':
            echo "<table id='activity_table' class='table table-bordered'><thead><th>Názov udalosti</th><th>Názov súboru</th><th>Dátum</th></thead><tbody class='activity_table-body'>";
            while ($row = $result->fetch_assoc()) {
                echo "<tr>
                         <td><a href='#' class='activity_data' data-id='" . $row['id'] . "' data-toggle='modal' data-target='.universal'> " . $row['title_sk'] . "</a></td>
                         <td>" . $row['path'] . "</td>
                         <td>" . $row['date'] . "</td>
                      </tr>";
                array_push($object, $row);
            }
            break;
    }

    echo "</tbody></table>";
    return $object;
}
