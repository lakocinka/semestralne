<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

require "../../../../database/config.php";

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

mysqli_set_charset($conn, 'utf8');

if (isset($_POST['delete'])) {
    delete_event($_POST['delete'], $conn);
} else if (isset($_POST['update'])) {
    update_activity($_POST['update'], $conn);
} else if (isset($_FILES['pictureFile']) && isset($_POST['directory']) && !empty($_FILES["pictureFile"]["name"])) {
    add_photo();
} else if (isset($_POST['delete_file']) && isset($_POST['dir'])) {
    remove_photo($_POST['dir'], $_POST['delete_file']);
} else if (isset($_FILES['photos_upload']) && count($_FILES['photos_upload']['name']) > 0) {
    insert_activity($conn);
}

function delete_event($object, $conn)
{
    $id = $object['id'];
    $sql = "DELETE FROM photogallery WHERE id=" . $id;
    if ($conn->query($sql) === FALSE) {
        echo "Error: " . $sql . "<br>" . $conn->error;
        return;
    }

    $dirname = "../../../../assets/images/activities/" . $object['path'];
    if (is_dir($dirname)) {
        recursive_rmdir($dirname);
    } else {
        echo "Error: " . $object['path'] . " does not exists.";
        return;
    }

    print json_encode("success");
}

function update_activity($object, $conn)
{
    if (!is_dir("../../../../assets/images/activities/" . $object['path'])) {
        echo "Error: " . $object['path'] . " does not exists.";
        return;
    }

    $sql = "UPDATE photogallery as m SET m.title_en='" . $object['title_en'] . "',
                             m.title_sk='" . $object['title_sk'] . "',
                             m.path='" . $object['path'] . "',
                             m.date='" . $object['date'] . "' WHERE m.id='" . $object['id'] . "'";

    if ($conn->query($sql) === FALSE) {
        echo "Error: " . $sql . "<br>" . $conn->error;
        return;
    }
    print json_encode("success");
}

function insert_activity($conn)
{
    $folder = preg_replace('/\s+/', '_', $_POST["path"]);
    $target_dir = "../../../../assets/images/activities/" . $folder . "/";

    if (!file_exists($target_dir)) {
        if (!mkdir($target_dir, 0777, true)) {
            $_SESSION['error_msg'] = "Problém pri vytváraní priečinka.";
            header("location:../fotky.php");
            return;
        }
        chown($target_dir, "xmichnam2");
        chgrp($target_dir, "xmichnam2");
    }

    for ($i = 0; $i < count($_FILES['photos_upload']['name']); $i++) {
        $target_file = $target_dir . preg_replace('/\s+/', '_', basename($_FILES['photos_upload']['name'][$i]));

        if (!move_uploaded_file($_FILES['photos_upload']['tmp_name'][$i], $target_file)) {
            $_SESSION['error_msg'] = "Sorry, there was an error uploading your file.";
            header("location:../fotky.php");
            return;
        }
    }

    $sql = "INSERT INTO photogallery (title_sk, title_en, path, date)
            VALUES ('" . $_POST['title_sk'] . "','" . $_POST['title_en'] . "','" . $folder . "','" . $_POST['date'] . "')";


    if ($conn->query($sql) === FALSE) {
        $_SESSION['error_msg'] = "Error: " . $sql . "<br>" . $conn->error;
        header("location:../fotky.php");
        return;
    }

    $_SESSION['error_msg'] = "Upload prebehol úspešne.";
    header("location:../fotky.php");
}


function add_photo()
{
    $target_file = "../../../../assets/images/activities/" . $_POST['directory'];

    if (file_exists($target_file)) {
        $_SESSION["error_msg"] = "Sorry, file already exists.";
        return;
    }

    if (!move_uploaded_file($_FILES["pictureFile"]["tmp_name"], preg_replace('/\s+/', '_', $target_file))) {
        $_SESSION['error_msg'] = "Sorry, there was an error uploading your file.";
        return;
    }

    print json_encode(preg_replace('/\s+/', '_',$_FILES["pictureFile"]["name"]));
}

function remove_photo($directory, $filename)
{
    $target_dir = "../../../../assets/images/activities/" . $directory . "/";
    $target_file = $target_dir . $filename;

    if (is_file($target_file)) {
        if (!unlink($target_file)) {
            echo "Error with deleting " . $target_file . " file";
            return;
        }
    } else {
        echo "Error: " . $target_file . " does not exists.";
        return;
    }

    print json_encode($filename);
}

function recursive_rmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir . "/" . $object) == "dir") recursive_rmdir($dir . "/" . $object); else unlink($dir . "/" . $object);
            }
        }
        reset($objects);
        rmdir($dir);
    }
}
