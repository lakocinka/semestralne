<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

require "../../../../database/config.php";

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

mysqli_set_charset($conn, 'utf8');

if (isset($_POST['delete'])) {
    delete_activity($_POST['delete'], $conn);
} else if (isset($_POST['update'])) {
    update_activity($_POST['update'], $conn);
} else if (isset($_POST['url']) && isset($_POST['title_sk']) && isset($_POST['title_en']) && isset($_POST['date'])) {
    insert_activity($conn);
}

function delete_activity($object, $conn)
{
    $id = $object['id'];
    $sql = "DELETE FROM videos WHERE id=" . $id;
    if ($conn->query($sql) === FALSE) {
        echo "Error: " . $sql . "<br>" . $conn->error;
        return;
    }
    print json_encode("success");
}

function update_activity($object, $conn)
{
    if (!url_exists($object['url'])) {
        echo "Error: URL address is invalid";
        return;
    }

    $path = get_path($object['url']);
    $sql = "UPDATE videos SET title_en='" . $object['title_en'] . "',
                             title_sk='" . $object['title_sk'] . "',
                             url='" . $object['url'] . "',
                             path='" . $path . "',
                             date='" . $object['date'] . "' WHERE id='" . $object['id'] . "'";

    if ($conn->query($sql) === FALSE) {
        echo "Error: " . $sql . "<br>" . $conn->error;
        return;
    }
    print json_encode("success");
}

function insert_activity($conn)
{

    if (!url_exists($_POST['url'])) {
        echo "Error: URL address is invalid";
        return;
    }

    $path = get_path($_POST["url"]);
    $sql = "INSERT INTO videos (title_sk, title_en, path, url, date) 
            VALUES ('" . $_POST['title_sk'] . "','" . $_POST['title_en'] . "','" . $path . "','" . $_POST["url"] . "','" . $_POST['date'] . "')";


    if ($conn->query($sql) === FALSE) {
        $_SESSION['error_msg'] = "Error: " . $sql . "<br>" . $conn->error;
        header("location:../media.php");
        return;
    }

    $_SESSION['error_msg'] = "Upload prebehol úspešne.";

    header("location:../videa.php");
}

function url_exists($url)
{
    if (!$fp = curl_init($url)) return false;
    return true;
}

function get_path($url)
{
    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
        return '//youtube.com/embed/' . $match[1];
    } else {
        $_SESSION['error_msg'] = "Neplatná URL adresa!";
        header("location:../videa.php");
        return null;
    }
}
