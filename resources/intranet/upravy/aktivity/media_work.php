<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

require "../../../../database/config.php";

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

mysqli_set_charset($conn, 'utf8');

if (isset($_POST['delete'])) {
    delete_activity($_POST['delete'], $conn);
} else if (isset($_POST['update'])) {
    update_activity($_POST['update'], $conn);
} else if (isset($_FILES['file']) && !empty($_FILES["file"]["name"])) {
    insert_activity($conn, true);
} else if (empty($_FILES["file"]["name"]) && isset($_POST['path'])) {
    insert_activity($conn, false);
}

function delete_activity($object, $conn)
{
    $id = $object['id'];
    $sql = "DELETE FROM media WHERE id=" . $id;
    if ($conn->query($sql) === FALSE) {
        echo "Error: " . $sql . "<br>" . $conn->error;
        return;
    }

    if ((strcmp(strtolower($object['type']), 'youtube') != 0
            && strcmp(strtolower($object['type']), 'web') != 0)) {
        $filename = "../../../../assets/media/" . $object['path'];
        if (is_file($filename)) {
            if (!unlink($filename)) {
                echo "Error with deleting " . $object['path'] . " file";
                return;
            }
        } else {
            echo "Error: " . $object['path'] . " does not exists.";
            return;
        }
    }

    print json_encode("success");
}

function update_activity($object, $conn)
{
    if ((strcmp(strtolower($object['type']), 'youtube') != 0
            && strcmp(strtolower($object['type']), 'web') != 0)
        && !is_file("../../../../assets/media/" . $object['path'])
    ) {
        echo "Error: " . $object['path'] . " does not exists.";
        return;
    }

    $sql = "UPDATE media SET title_en='" . $object['title_en'] . "',
                             title_sk='" . $object['title_sk'] . "',
                             path='" . $object['path'] . "',
                             date='" . $object['date'] . "' WHERE id='" . $object['id'] . "'";

    if ($conn->query($sql) === FALSE) {
        echo "Error: " . $sql . "<br>" . $conn->error;
        return;
    }
    print json_encode("success");
}

function insert_activity($conn, $check_path)
{

    if ($check_path) {
        $target_dir = "../../../../assets/media/";
        $target_file = $target_dir . basename($_FILES["file"]["name"]);

        if (file_exists($target_file)) {
            $_SESSION['error_msg'] = "Sorry, file already exists.";
            header("location:../media.php");
            return;
        }

        if (!move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
            $_SESSION['error_msg'] = "Sorry, there was an error uploading your file.";
            header("location:../media.php");
            return;
        }

        $sql = "INSERT INTO media (title_sk, title_en, type, path, date) 
            VALUES ('" . $_POST['title_sk'] . "','" . $_POST['title_en'] . "','" . $_POST['file_type'] . "','" . $_FILES["file"]["name"] . "','" . $_POST['date'] . "')";
    } else {
        $sql = "INSERT INTO media (title_sk, title_en, type, path, date) 
            VALUES ('" . $_POST['title_sk'] . "','" . $_POST['title_en'] . "','" . $_POST['file_type'] . "','" . $_POST["path"] . "','" . $_POST['date'] . "')";
    }

    if ($conn->query($sql) === FALSE) {
        $_SESSION['error_msg'] = "Error: " . $sql . "<br>" . $conn->error;
        header("location:../media.php");
        return;
    }

    $_SESSION['error_msg'] = "Upload prebehol úspešne.";

    header("location:../media.php");
}
