var mesiac;
var rok;
var weekend_days;
var dragStart = 0;
var dragEnd = 0;
var isDragging = false;
var numberOfDays = $("tbody").children()[0].children.length-3;
var inputArray = [];
var outputArray = [];
var inputPersonArray = [];
var outputPersonArray = [];
var perId;


$(document).ready(function(){
    $('#table1').on('click', '.openModalUniversal', function () {
        openModal(this);
    });
    addListeners();
    $("#successAlert").css("display","none");
    $("#saveAlert").css("display","none");
    getAllDaysInput($(".myClass"));

});


function getAllDaysInput(inputData){
    inputArray.length = 0;
    var mes = $("#mesiac").val();
    var r = $("#rok").val();

    for (var i = 0; i <= inputData.length;i++){
        var idPersonOfDay = $($($(inputData[i]).parent()).children()[0]).text();
        var date = r + "-" + mes + "-" + ($(inputData[i]).index()-2);
        var inputDay = new function() {
            this.idPerson = idPersonOfDay;
            this.typeDay = getType(inputData[i]);
            this.dateDay = date;
            inputArray.push(inputDay);
        };
    }
    inputArray.shift();
}

function getAllDaysOutput(inputData){
    outputArray.length = 0;
    var mes = $("#mesiac").val();
    var r = $("#rok").val();

    for (var i = 0; i <= inputData.length;i++){
        var idPersonOfDay = $($($(inputData[i]).parent()).children()[0]).text();
        var date = r + "-" + mes + "-" + ($(inputData[i]).index()-2);
        var inputDay = new function() {
            this.idPerson = idPersonOfDay;
            this.typeDay = getType(inputData[i]);
            this.dateDay = date;
            outputArray.push(inputDay);
        };
    }
    outputArray.shift();
console.log(outputArray);

}

function getAllPersonDaysInput(inputData){
    inputPersonArray.length = 0;
    var mes = $("#mesiac").val();
    var r = $("#rok").val();

    for (var i = 0; i <= inputData.length;i++){
        var idPersonOfDay = $($($(inputData[i]).parent()).children()[0]).text();
        var date = r + "-" + mes + "-" + ($(inputData[i]).index()-2);
        var inputDay = new function() {
            this.idPerson = idPersonOfDay;
            this.typeDay = getType(inputData[i]);
            this.dateDay = date;
            inputPersonArray.push(inputDay);
        };
    }
    inputPersonArray.shift();
}

function getAllPersonDaysOutput(inputData){
    outputPersonArray.length = 0;
    var mes = $("#mesiac").val();
    var r = $("#rok").val();

    for (var i = 0; i <= inputData.length;i++){
        var date = r + "-" + mes + "-" + (i+1);
        var inputDay = new function() {
            this.idPerson = perId;
            this.typeDay = getType(inputData[i]);
            this.dateDay = date;
            outputPersonArray.push(inputDay);
        };
    }
    outputPersonArray.shift();

}

function getType(input){
    var content = $(input).text();
    switch (content){
        case "D":
            return 4;
            break;
        case "PN":
            return 1;
            break;
        case "PD":
            return 5;
            break;
        case "OČR":
            return 2;
            break;
        case "SC":
            return 3;
            break;
        default:
            break;
    }

}

function editTable(){
    var type = $("#type").val();
    $(this).text(type);
    $(this).removeClass();
    $(this).addClass("myClass");
    switch (type){
        case "D":
            $(this).addClass("dov");
            break;
        case "PN":
            $(this).addClass("pn");
            break;
        case "PD":
            $(this).addClass("pd");
            break;
        case "OČR":
            $(this).addClass("ocr");
            break;
        case "SC":
            $(this).addClass("sc");
            break;
        default:
            break;
    }
}

function cancelType(){
    $(this).text("");
    var idx = $(this).index()-2;
    if (weekend_days.indexOf(idx) != -1)
    {
        $(this).removeClass();
        $(this).addClass("myClass");
    }
    else {
        $(this).removeClass();
        $(this).addClass("myClass");
    }
}

function cancelType2(){
    $(this).text("");
    var idx = $(this).index();

    if ((idx  == 5) || (idx  == 6))
    {
        $(this).removeClass();
        $(this).addClass("weekhigh");
        $(this).addClass("weekend");
    }
    else {
        $(this).removeClass();
        $(this).addClass("weekhigh");
        $(this).addClass("week");
    }
}

function addListeners(){
    $("#mesiac").change(function(){
        mesiac = $(this).val();
    });

    $("#rok").change(function(){
        rok = $(this).val();
    });

    $("#edit").click(function(){
        $("#nepritomnost").css("display", "block");
        $("#cancelEdit").css("display", "block");
        $("#edit").css("display", "none");
        $("#save").css("display", "block");
        $(".myClass").click(editTable)
            .dblclick(cancelType)
            .mousedown(rangeMouseDown)
            .mouseup(rangeMouseUp);
    });

    $("#save").click(function (){
        outputArray.length = 0;
        getAllDaysOutput($(".myClass"));
        checkData(inputArray, outputArray);
        getAllDaysInput($(".myClass"));
        caller2();

    });

    $(".employee").click(getEmployeeMonth);

    $("#cancelEdit").click(function(){
        $("#edit").css("display", "block");
        $("#nepritomnost").css("display", "none");
        $("#cancelEdit").css("display", "none");
        $("#save").css("display", "none");
        $(".myClass").unbind();

        loadActualTable();

    });

    $("#closeModal").click(loadActualTable);

    $("#submitModal").click(function(){
        outputPersonArray.length = 0;
        var a = $(".weekhigh");

        getAllPersonDaysOutput($(".weekhigh"));
        checkData(inputPersonArray, outputPersonArray);
        getAllPersonDaysInput($(".weekhigh"));
        caller();

    })
}

function loadActualTable(){
    mesiac = $("#mesiac").val();
    rok = $("#rok").val();
    $.ajax({
        type: "POST",
        url:'./dochadzka.php',
        data: {'mesiac': mesiac, 'rok':rok},
        success: function (data) {
            $("#table1").remove();
            $("#myTable").append($(data).find("#table1"));
            addListeners();
            $('#table1').on('click', '.openModalUniversal', function () {
                openModal(this);
            });
            $(".pagination").remove();
            $('#table1').tablePaginate({navigateType:'navigator'});

        },
        error: function () {
            console.log("failed load");
        }
    });
}

function editPersonalTable(){
    var type = $("#personalType").val();
    $(this).text(type);
    $(this).removeClass();
    $(this).addClass("weekhigh");
    switch (type){
        case "D":
            $(this).addClass("dov");
            break;
        case "PN":
            $(this).addClass("pn");
            break;
        case "PD":
            $(this).addClass("pd");
            break;
        case "OČR":
            $(this).addClass("ocr");
            break;
        case "SC":
            $(this).addClass("sc");
            break;
        default:
            break;
    }
}

function rangeMouseDown(e) {
    if (isRightClick(e)) {
        return false;
    } else {
        var allCells = $(".myClass");
        dragStart = allCells.index($(this));
        isDragging = true;

        if (typeof e.preventDefault != 'undefined') { e.preventDefault(); }
        document.documentElement.onselectstart = function () { return false; };
    }
}
function rangeMouseDown2(e) {
    if (isRightClick(e)) {
        return false;
    } else {
        var allCells = $(".weekhigh");
        dragStart = allCells.index($(this));
        isDragging = true;

        if (typeof e.preventDefault != 'undefined') { e.preventDefault(); }
        document.documentElement.onselectstart = function () { return false; };
    }
}

function rangeMouseUp(e) {
    if (isRightClick(e)) {
        return false;
    } else {
        var allCells = $(".myClass");
        dragEnd = allCells.index($(this));

        isDragging = false;
        if (dragEnd != 0) {
            selectRange();
        }

        document.documentElement.onselectstart = function () { return true; };
    }
}
function rangeMouseUp2(e) {
    if (isRightClick(e)) {
        return false;
    } else {
        var allCells = $(".weekhigh");
        dragEnd = allCells.index($(this));

        isDragging = false;
        if (dragEnd != 0) {
            selectRange2();
        }

        document.documentElement.onselectstart = function () { return true; };
    }
}

function selectRange() {
    $(this).removeClass();
    $(this).addClass('selected');

    var type = $("#type").val();

    $(this).text(type);

    switch (type){
        case "D":
            selectTD("dov", "D");
            break;
        case "PN":
            selectTD("pn", "PN");
            break;
        case "PD":
            selectTD("pd", "PD");
            break;
        case "OČR":
            selectTD("ocr", "OČR");
            break;
        case "SC":
            selectTD("sc", "SC");
            break;
        default:
            break;
    }
}

function selectRange2() {
    $(this).removeClass();
    $(this).addClass('selected');

    var type = $("#personalType").val();

    $(this).text(type);

    switch (type){
        case "D":
            selectTD2("dov", "D");
            break;
        case "PN":
            selectTD2("pn", "PN");
            break;
        case "PD":
            selectTD2("pd", "PD");
            break;
        case "OČR":
            selectTD2("ocr", "OČR");
            break;
        case "SC":
            selectTD2("sc", "SC");
            break;
        default:
            break;
    }
}

function selectTD(classType, text){
    if (dragEnd + 1 < dragStart) { // reverse select
        var bunky = $(".myClass").slice(dragEnd, dragStart + 1);
        bunky.text(text);
        bunky.removeClass();
        bunky.addClass("myClass");
        bunky.addClass(classType);

    } else {
        var bunky = $(".myClass").slice(dragStart, dragEnd + 1);
        bunky.text(text);
        bunky.removeClass();
        bunky.addClass("myClass");
        bunky.addClass(classType);
    }
}
function selectTD2(classType, text){
    if (dragEnd + 1 < dragStart) { // reverse select
        var bunky = $(".weekhigh").slice(dragEnd, dragStart + 1);
        bunky.text(text);
        bunky.removeClass();
        bunky.addClass("weekhigh");
        bunky.addClass(classType);

    } else {
        var bunky = $(".weekhigh").slice(dragStart, dragEnd + 1);
        bunky.text(text);
        bunky.removeClass();
        bunky.addClass("weekhigh");
        bunky.addClass(classType);
    }
}

function isRightClick(e) {
    if (e.which) {
        return (e.which == 3);
    } else if (e.button) {
        return (e.button == 2);
    }
    return false;
}

function openModal(data) {
    var name = $(data).data('name');
    $("#name").empty();
    $("#name").text(name);
    perId = $(data).data('personid');

}

function getEmployeeMonth(){
    $("#modalBody").empty();
    var parPar = $(this).parent();
    var par = parPar.parent();
    var allDays = par.children();
    var table = createTableMonth(allDays);
    $("#modalBody").append(table);
    getAllPersonDaysInput($(".weekhigh"));

}

function createTableMonth(allDays){
    var days = ["Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday", "Sunday"];
    var newtable = document.createElement("table");
    $(newtable).addClass("table2");
    $(newtable).addClass("table");
    $(newtable).addClass("table-bordered");
    var newthead = document.createElement("thead");
    var trthead = document.createElement("tr");
    for(var i=0; i<7; i++){
        var tdthead = document.createElement("td");
        $(tdthead).addClass("table2head")
        $(tdthead).text(days[i]);
        $(trthead).append(tdthead);
    }
    $(newtable).append(trthead);

    var firstDay = $($(".headTable").children().children()[3]).data('day');
    var rows = Math.ceil((firstDay + numberOfDays -1)/7);
    var newtbody = document.createElement("tbody");
    var dx=3;
    for(var i=0; i< rows;i++){
        var newtr = document.createElement("tr");
        for(var j=0; j < 7; j++){
            var newtd = document.createElement("td");

            if((j+7*i < firstDay-1) || (j+7*i >firstDay + numberOfDays - 2)){
                $(newtr).append(newtd);
                continue;
            }

            $(newtd).click(editPersonalTable)
                .dblclick(cancelType2)
                .mousedown(rangeMouseDown2)
                .mouseup(rangeMouseUp2);

            if(j >= 5) {
                $(newtd).addClass("weekend");
            }
            else{
                $(newtd).addClass("week");
            }

            var content = $(allDays[dx++]).text();
            if(content != ""){
                var personDayClass = getPersonDayClass(content);
                $(newtd).removeClass();
                $(newtd).addClass(personDayClass);
                $(newtd).text(content);
            }

            $(newtd).addClass("weekhigh");
            $(newtr).append(newtd);
        }
        $(newtbody).append(newtr);

    }
    $(newtable).append(newtbody);

    return newtable;

}
function getPersonDayClass(content){
    switch (content){
        case "D":
            return("dov");
            break;
        case "PN":
            return("pn");
            break;
        case "PD":
            return("pd");
            break;
        case "OČR":
            return ("ocr")
            break;
        case "SC":
            return ("sc")
            break;
        default:
            return("weekhigh");
            break;
    }
}

function checkData(inA, outA){
    var insertData = [];
    var updateData = [];
    var deleteData = [];
    for(var i = 0; i < outA.length; i++ ){
        if(inA[i].typeDay == outA[i].typeDay){
            continue;
        }
        if((inA[i].typeDay != null) && (outA[i].typeDay == null)){
            //delete item
            deleteData.push(outA[i]);
        }
        if((inA[i].typeDay == null) && (outA[i].typeDay != null)){
            //insert
            insertData.push(outA[i]);
        }
        if((inA[i].typeDay != outA[i].typeDay) && (inA[i].typeDay != null) && (outA[i].typeDay != null)){
            //update
            updateData.push(outA[i]);
        }
    }
    console.log(insertData);
    console.log(updateData);
    console.log(deleteData);

    //console.log(table.row().data());


    if(updateData.length !=0){
        upData(updateData);
    }
    if (deleteData.length != 0){
        delData(deleteData);
    }
    if(insertData.length != 0){
        insData(insertData);
    }
}

function upData(updateData){
    $.ajax({
        type: 'POST',
        url: 'update.php',
        data: {json: JSON.stringify(updateData)},
        success: function(msg){
            console.log("success");
        },
        error: function(msg){
            console.log('fail');
        }
    });
}

function delData(deleteData){
    $.ajax({
        type: 'POST',
        url: 'delete.php',
        data: {json: JSON.stringify(deleteData)},
        success: function(msg){
            console.log("success");

        },
        error: function(msg){
            console.log('fail');
        }
    });
}

function insData(insertData){
    $.ajax({
        type: 'POST',
        url: 'insert.php',
        data: {json: JSON.stringify(insertData)},
        success: function(msg){
            console.log(msg);
            console.log("success");

        },
        error: function(msg){
            console.log('fail');
        }
    });
}


function caller()
{
    $("#successAlert").toggle( "slow", function() {
        // Animation complete.
    });
    setTimeout(function() {
        $("#successAlert").hide();
        }, 5000);
}
function caller2()
{
    $("#saveAlert").toggle( "slow", function() {
        // Animation complete.
    });
    setTimeout(function() {
        $("#saveAlert").hide();
    }, 5000);
}
