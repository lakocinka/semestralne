(function (w, d) {

    var object = {};
    var insertMode = false;

    $("#activity_table").DataTable({});
    $('#datepicker').datepicker({
        dateFormat: "yyyy-dd-mm"
    });

    function ajaxMethod(otherMethod, obj) {
        $.ajax({
            type: "POST",
            url: '../upravy/aktivity/media_work.php',
            data: obj,
            dataType: 'json',
            success: function () {
                otherMethod(obj);
            },
            error: function (data) {
                console.log(JSON.stringify(data));
                alert(JSON.stringify(data));
            }
        });
    }

    $('#eraseActivity').on('click', function () {
        ajaxMethod(remove, {'delete': object});
    });

    var remove = function (data) {
        $('#activity_table .activity_table-body tr').each(function (index, element) {
            if (element.children["0"].firstChild.dataset.id == data.delete.id) {
                $(element).remove();
                return;
            }
        });

        $('#activity_modal').modal('hide');
        alert("Vymazanie prebehlo úspešne.");
    }

    var update = function (data) {
        $('#activity_table .activity_table-body tr').each(function (index, element) {
            if (element.children["0"].firstChild.dataset.id == data.update.id) {
                element.children["0"].firstChild.text = data.update.title_sk;
                element.children["1"].textContent = data.update.type;
                element.children["2"].textContent = data.update.path;
                element.children["3"].textContent = data.update.date;


                $.each(media, function (i, e) {
                    if (e.id == data.update.id) {
                        e.title_sk = data.update.title_sk;
                        e.title_en = data.update.title_en;
                        e.path = data.update.path;
                        e.date = data.update.date;
                    }
                });

                return;
            }
        });

        $('#activity_modal').modal('hide');
        alert("Aktualizácia prebehla úspešne.");
    }

    $('#activity_table').on('click', '.activity_data', function () {
        var dataset = this.dataset;
        var result = $.grep(media, function (e) { //media is global variable defined in media.php
            return Number(e.id) == Number(dataset.id);
        });

        object = result[0];
        fillData();
        insertMode = false;
        $(".hide-input").hide();
    });

    $('#add_button').on('click', function () {
        document.getElementById("activity_form").reset();
        $('#activity_id').empty();
        $('#activity_title').empty();
        $(".hide-input").show().val(null);
        object = {};
        insertMode = true;
    });

    $('#resetModal').on('click', function () {
        fillData();
    });

    $('#submitModal').on('click', function () {
        if (!$("input[name=title_sk]").val() || !$("input[name=title_en]").val()
            || !$("select[name=file_type]").val() || !$("input[name=path]").val()
            || !$("input[name=date]").val()) {
            alert("Chýbajú niektoré povinné položky!");
            return;
        }

        var fileUpload = $("input[name=file]");
        if (fileUpload.val()) {
            var allowedFiles = [".pdf", ".jpg", ".jpeg", ".png", ".mp4", ".avi"];
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
            if (!regex.test(fileUpload.val().toLowerCase())) {
                alert("Podporované typy súborov: " + allowedFiles.join(', '));
                return;
            }
        }

        if (!insertMode) {
            var obj = needUpdate();
            if (!jQuery.isEmptyObject(obj)) {
                var obj = Object.assign({}, object, obj);
                ajaxMethod(update, {'update': obj});
                return;
            } else {
                alert("Nič sa nezmenilo!");
                return;
            }
        }

        document.getElementById('activity_form').submit();

    });

    $('#file').on('change', function () {
        if (this.files.length) {
            $("input[name=path]").prop('disabled', true).val(this.files[0].name);
        } else {
            $("input[name=path]").prop('disabled', false).val("");
        }
    });

    function fillData() {
        $('#activity_id').empty().append(object.id);
        $('#activity_title').empty().append(object.title_sk);
        $("input[name=title_sk]").val(object.title_sk);
        $("input[name=title_en]").val(object.title_en);
        $("select[name=file_type]").val(object.type);
        $("input[name=path]").val(object.path).prop('disabled', false);
        $("input[name=date]").val(object.date);

        $(".hide-input").val(null);
    }

    function needUpdate() {
        var tmp = {};

        if ($("input[name=title_sk]").val() != object.title_sk) {
            tmp['title_sk'] = $("input[name=title_sk]").val();
        }
        if ($("input[name=title_en]").val() != object.title_en) {
            tmp['title_en'] = $("input[name=title_en]").val();
        }
        if ($("select[name=file_type]").val() != object.type) {
            tmp['type'] = $("select[name=file_type]").val();
        }
        if ($("input[name=path]").val() != object.path) {
            tmp['path'] = $("input[name=path]").val();
        }
        if ($("input[name=date]").val() != object.date) {
            tmp['date'] = $("input[name=date]").val();
        }
        return tmp;
    }
}(window, document));
