(function (w, d) {

    var object = {};
    var insertMode = false;
    var path_events = "../../../assets/images/activities/";
    var photo_list = new Array();

    $("#activity_table").DataTable({});
    $('#datepicker').datepicker({
        dateFormat: "yyyy-dd-mm"
    });

    function ajaxMethod(otherMethod, obj) {
        $.ajax({
            url: '../upravy/aktivity/photogallery_work.php',
            method: "POST",
            data: obj,
            dataType: 'json',
            success: function (data) {
                otherMethod(obj);
            },
            error: function (data) {
                console.log(JSON.stringify(data));
                alert(JSON.stringify(data));
            }
        });
    }

    $('#box__photo_list').on('click', '.fa', function (e) {
        var filename = this.parentNode.previousElementSibling.innerText;
        photo_list.length = 0;
        ajaxMethod(afterDeletePhoto, {'dir': object.path, 'delete_file': filename});
    });

    var afterDeletePhoto = function (obj) {
        var index = photo_list.indexOf(obj.delete_file);
        if (index > -1) {
            photo_list.splice(index, 1); //remove from list of the photos
        }

        $('#box__photo_list').children().each(function (index, element) {
            if (element.children["0"].innerText == obj.delete_file) {
                $(element).remove();
                return;
            }
        });

        alert("Fotka: '" + obj.delete_file + "' vymazaná.");
    };

    $('#add_photo').on('click', function () {
        var fname = $("input[name=photo_file]").val().split("\\").pop().replace(/ /g, "_");
        if (jQuery.inArray(fname, photo_list) != -1) {
            alert("Súbor s názvom '" + fname + "' už existuje!");
            return;
        }

        var fileUpload = $("input[name=photo_file]");
        if (fileUpload.val()) {
            var allowedFiles = [".jpg", ".jpeg", ".png", ".gif"];
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
            if (!regex.test(fileUpload.val().toLowerCase())) {
                alert("Podporované typy súborov: " + allowedFiles.join(', '));
                return;
            }
        } else {
            alert("Žiadny súbor");
            return;
        }

        var form_data = new FormData();
        var property = document.getElementById('photo_file').files[0];
        form_data.append('pictureFile', property);
        form_data.append('directory', object.path + "/" + property.name.replace(/ /g, "_"));

        $.ajax({
            url: '../upravy/aktivity/photogallery_work.php',
            method: "POST",
            data: form_data,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function (photo) {
                $('#box__photo_list').append("<li> <span class=\"text\">" + photo + "</span> <div class=\"tools\"> " +
                    "<i class=\"fa fa-trash-o\"></i> </div> </li>");
                $("input[name=photo_file]").val(null);
                photo_list.push(object.path);
            },
            error: function (data) {
                console.log(JSON.stringify(data));
                alert(JSON.stringify(data));
            }
        });
    });

    $('#eraseActivity').on('click', function () {
        ajaxMethod(remove, {'delete': object});
    });

    var remove = function (data) {
        $('#activity_table .activity_table-body tr').each(function (index, element) {
            if (element.children["0"].firstChild.dataset.id == data.delete.id) {
                $(element).remove();
                return;
            }
        });

        $('#activity_modal').modal('hide');
        alert("Vymazanie prebehlo úspešne.");
    }

    var update = function (data) {
        $('#activity_table .activity_table-body tr').each(function (index, element) {
            if (element.children["0"].firstChild.dataset.id == data.update.id) {
                element.children["0"].firstChild.text = data.update.title_sk;
                element.children["1"].textContent = data.update.path;
                element.children["2"].textContent = data.update.date;

                $.each(photos, function (i, e) {
                    if (e.id == data.update.id) {
                        e.title_sk = data.update.title_sk;
                        e.title_en = data.update.title_en;
                        e.path = data.update.path;
                        e.date = data.update.date;
                    }
                });
                return;
            }
        });

        $('#activity_modal').modal('hide');
        alert("Aktualizácia prebehla úspešne.");
    }

    $('#activity_table').on('click', '.activity_data', function () {
        $('.box-primary').show();
        $(".hide-input").hide();
        var dataset = this.dataset;
        var result = $.grep(photos, function (e) { //photos is global variable defined in fotky.php
            return Number(e.id) == Number(dataset.id);
        });

        object = result[0];
        insertMode = false;
        fillData();
    });

    $('#add_button').on('click', function () {
        $("input[name=path]").val(null).prop('disabled', false);
        document.getElementById("activity_form").reset();
        object = {};
        insertMode = true;
        $('#activity_id').empty();
        $('#activity_title').empty();
        $(".hide-input").show().val(null);
        $('.box-primary').hide();
    });

    $('#resetModal').on('click', function () {
        fillData();
    });

    $('#submitModal').on('click', function () {
        if (!$("input[name=title_sk]").val() || !$("input[name=title_en]").val()
            || !$("input[name=path]").val() || !$("input[name=date]").val()
            || (insertMode && !$('#photos_upload').val())) {
            alert("Chýbajú niektoré povinné položky!");
            return;
        }

        if (!insertMode) {
            var obj = needUpdate();
            if (!jQuery.isEmptyObject(obj)) {
                var obj = Object.assign({}, object, obj);
                ajaxMethod(update, {'update': obj});
                return;
            } else {
                alert("Nič sa nezmenilo!");
                return;
            }
        }
        document.getElementById('activity_form').submit();
    });

    function fillData() {
        document.getElementById("activity_form").reset();
        $('#activity_id').empty().append(object.id);
        $('#activity_title').empty().append(object.title_sk);
        $("input[name=title_sk]").val(object.title_sk);
        $("input[name=title_en]").val(object.title_en);
        $("input[name=date]").val(object.date);
        $("input[name=path]").val(object.path).prop('disabled', true);
        $("input[name=photo_file]").val(null);
        $('#photos_upload').val(null);
        $(".hide-input").val(null);

        show_images(object.path);
    }

    function needUpdate() {
        var tmp = {};

        if ($("input[name=title_sk]").val() != object.title_sk) {
            tmp['title_sk'] = $("input[name=title_sk]").val();
        }
        if ($("input[name=title_en]").val() != object.title_en) {
            tmp['title_en'] = $("input[name=title_en]").val();
        }
        if ($("input[name=date]").val() != object.date) {
            tmp['date'] = $("input[name=date]").val();
        }
        return tmp;
    }

    var show_images = function (event) {
        if (insertMode) {
            $('#box__photo_list').empty().hide();
            return;
        }
        $('#box__photo_list').empty();
        $.ajax({
            url: path_events + event,
            success: function (data) {
                $(data).find("a").attr("href", function (i, val) {
                    if (val.toLocaleLowerCase().match(/\.(jpe?g|png|gif)$/)) {
                        $('#box__photo_list').append("<li> <span class=\"text\">" + val + "</span> <div class=\"tools\"> " +
                            "<i class=\"fa fa-trash-o\"></i> </div> </li>");
                        photo_list.push(val);
                    }
                });
            },
            error: function (msg) {
                alert("Errors\n " + JSON.stringify(msg));
            }
        });
    }
}(window, document));
