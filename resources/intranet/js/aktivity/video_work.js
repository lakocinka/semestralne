(function (w, d) {

    var object = {};
    var insertMode = false;

    $("#activity_table").DataTable({});
    $('#datepicker').datepicker({
        dateFormat: "yyyy-dd-mm"
    });

    function ajaxMethod(otherMethod, obj) {
        $.ajax({
            type: "POST",
            url: '../upravy/aktivity/video_work.php',
            data: obj,
            dataType: 'json',
            success: function () {
                otherMethod(obj);
            },
            error: function (data) {
                console.log(JSON.stringify(data));
                alert(JSON.stringify(data));
            }
        });
    }

    $('#eraseActivity').on('click', function () {
        ajaxMethod(remove, {'delete': object});
    });

    var remove = function (data) {
        $('#activity_table .activity_table-body tr').each(function (index, element) {
            if (element.children["0"].firstChild.dataset.id == data.delete.id) {
                $(element).remove();
                return;
            }
        });

        $('#activity_modal').modal('hide');
        alert("Vymazanie prebehlo úspešne.");
    }

    var update = function (data) {
        $('#activity_table .activity_table-body tr').each(function (index, element) {
            if (element.children["0"].firstChild.dataset.id == data.update.id) {
                element.children["0"].firstChild.text = data.update.title_sk;
                element.children["1"].textContent = data.update.url;
                element.children["2"].textContent = data.update.date;

                $.each(videos, function (i, e) {
                    if (e.id == data.update.id) {
                        e.title_sk = data.update.title_sk;
                        e.title_en = data.update.title_en;
                        e.url = data.update.url;
                        e.date = data.update.date;
                    }
                });
                return;
            }
        });

        $('#activity_modal').modal('hide');
        alert("Aktualizácia prebehla úspešne.");
    }

    $('#activity_table').on('click', '.activity_data', function () {
        var dataset = this.dataset;
        var result = $.grep(videos, function (e) { //videos is global variable defined in videa.php
            return Number(e.id) == Number(dataset.id);
        });

        object = result[0];
        fillData();
        insertMode = false;
    });

    $('#add_button').on('click', function () {
        document.getElementById("activity_form").reset();
        $('#activity_id').empty();
        $('#activity_title').empty();
        object = {};
        insertMode = true;
    });

    $('#resetModal').on('click', function () {
        fillData();
    });

    $('#submitModal').on('click', function () {
        if (!$("input[name=title_sk]").val() || !$("input[name=title_en]").val()
            || !$("input[name=url]").val() || !$("input[name=date]").val()) {
            alert("Chýbajú niektoré povinné položky!");
            return;
        }

        if (!insertMode) {
            var obj = needUpdate();
            if (!jQuery.isEmptyObject(obj)) {
                var obj = Object.assign({}, object, obj);
                ajaxMethod(update, {'update': obj});
                return;
            } else {
                alert("Nič sa nezmenilo!");
                return;
            }
        }

        document.getElementById('activity_form').submit();

    });

    function fillData() {
        $('#activity_id').empty().append(object.id);
        $('#activity_title').empty().append(object.title_sk);
        $("input[name=title_sk]").val(object.title_sk);
        $("input[name=title_en]").val(object.title_en);
        $("input[name=url]").val(object.url);
        $("input[name=date]").val(object.date);
    }

    function needUpdate() {
        var tmp = {};

        if ($("input[name=title_sk]").val() != object.title_sk) {
            tmp['title_sk'] = $("input[name=title_sk]").val();
        }
        if ($("input[name=title_en]").val() != object.title_en) {
            tmp['title_en'] = $("input[name=title_en]").val();
        }
        if ($("input[name=url]").val() != object.url) {
            tmp['url'] = $("input[name=url]").val();
        }
        if ($("input[name=date]").val() != object.date) {
            tmp['date'] = $("input[name=date]").val();
        }
        return tmp;
    }
}(window, document));
