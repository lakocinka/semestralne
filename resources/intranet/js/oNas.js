/**
 * Created by Lukas on 22.5.2017.
 */
$(document).ready(function(){
    openModal(".aboutTitle", "#aboutModalTitle");
    $('#submitAboutModal').click(saveAboutText);
});

function openModal(selectorOpenModal, nameModalId) {

    var elementOpenModal,
        elementTitle;

    var init = function () {
        elementOpenModal = $(selectorOpenModal);
        elementTitle = $(nameModalId);

        if (elementOpenModal) {
            elementOpenModal.click(onClick);
        }
    };

    var onCreate = function () {
        $(document).ready(onDocumentReady);
    };

    var onDocumentReady = function () {
        init();
    };

    var onClick = function () {
        if (elementTitle) {
            var title = $(this).data('title');
            elementTitle.text(title);
            var typ = $(this).data('typ');
            loadAboutTextDatabase(typ);

        }
    };

    onCreate();

}
function loadAboutTextDatabase(typ) {
    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'aboutType': typ},
        success: function (data) {
            tinymce.activeEditor.setContent('');
            tinymce.activeEditor.execCommand('mceInsertContent', false, data);
        },
        error: function () {
            console.log("failed load");
        }
    });
}

function saveAboutText() {
    var content = tinymce.activeEditor.getContent();
    var typ = $("#aboutModalTitle").text();

    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'saveAboutContent': content, 'type': typ},
        success: function (data) {
            console.log(data);
            $('#createModal').modal('hide');
        },
        error: function () {
            console.log("failed load");
        }
    });
}