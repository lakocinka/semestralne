/**
 * Created by Lukas on 22.5.2017.
 */
$(document).ready(function(){
    openModal(".researchTitle", "#researchModalTitle");
    $('#submitResearchModal').click(saveResearchText);
});

function openModal(selectorOpenModal, nameModalId) {

    var elementOpenModal,
        elementTitle;

    var init = function () {
        elementOpenModal = $(selectorOpenModal);
        elementTitle = $(nameModalId);

        if (elementOpenModal) {
            elementOpenModal.click(onClick);
        }
    };

    var onCreate = function () {
        $(document).ready(onDocumentReady);
    };

    var onDocumentReady = function () {
        init();
    };

    var onClick = function () {
        if (elementTitle) {
            var title = $(this).data('title');
            elementTitle.text(title);
            var typ = $(this).data('typ');
            loadResearchTextDatabaseSK(typ);
            loadResearchTextDatabaseEN(typ);

        }
    };

    onCreate();

}
function loadResearchTextDatabaseSK(typ) {
    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'researchTypeSK': typ},
        success: function (data) {
            tinymce.get("researchModalSK").setContent('');
            tinymce.get("researchModalSK").execCommand('mceInsertContent', false, data);
            //tinymce.activeEditor.setContent('');
            //tinymce.activeEditor.execCommand('mceInsertContent', false, data);
        },
        error: function () {
            console.log("failed load");
        }
    });
}
function loadResearchTextDatabaseEN(typ) {
    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'researchTypeEN': typ},
        success: function (data) {
            tinymce.get("researchModalEN").setContent('');
            tinymce.get("researchModalEN").execCommand('mceInsertContent', false, data);
            // tinymce.activeEditor.setContent('');
            // tinymce.activeEditor.execCommand('mceInsertContent', false, data);
        },
        error: function () {
            console.log("failed load");
        }
    });
}
function saveResearchText() {
    saveResearchTextSK();
}
function saveResearchTextSK() {
    var content = tinymce.get('researchModalSK').getContent();
    var typ = $("#researchModalTitle").text();

    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'saveResearchContentSK': content, 'type': typ},
        success: function (data) {
            console.log(data);
            $('#createModal').modal('hide');
            saveResearchTextEN();
        },
        error: function () {
            console.log("failed load");
        }
    });
}
function saveResearchTextEN() {
    var content = tinymce.get('researchModalEN').getContent();
    var typ = $("#researchModalTitle").text();

    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'saveResearchContentEN': content, 'type': typ},
        success: function (data) {
            console.log(data);
            $('#createModal').modal('hide');
        },
        error: function () {
            console.log("failed load");
        }
    });
}