$(document).ready(function () {

    $("#submitPublikacieModal").click(saveData);
    $("#deletePublikacie").click(function () {
        $(".remPub").click(removePub);
    });
    $("#deletePublikacieLink").click(function () {
        $(".remLink").click(removeLink);
    });
    $("#addPublikacie").click(function () {
        $("#addCat").css("display", "none");
    });
    $('#deleteModal').on('hidden.bs.modal', function () {
        location.reload();
    });
    $('#deleteLinkModal').on('hidden.bs.modal', function () {
        location.reload();
    });
    $("#plusCat").click(function () {
        $("#addCat").slideToggle();
    });

    $("#vytvorCat").click(appendCategory);

    $("#createLink").click(createLink);


});

function createLink() {
    var linkName = $("#linkName").val();
    var link = $("#link").val();
    if (link && linkName) {
        $.ajax({
            type: "POST",
            url: './publikacieFunctions.php',
            data: {'linkName': linkName, "link": link},
            success: function (data) {
                $("#addLinkModal").modal('hide');
                location.reload();

            },
            error: function () {
                console.log("failed load");
            }
        });
    }
}

function appendCategory() {
    var name = $("#newCategory").val();
    if (name) {
        $.ajax({
            type: "POST",
            url: './publikacieFunctions.php',
            data: {'addCategory': name},
            success: function (data) {
                var op = document.createElement("option");
                $(op).text(name);
                $("#category").append(op);
                $("#addCat").slideToggle();
            },
            error: function () {
                console.log("failed load");
            }
        });

    }
}

function saveData() {
    if ($("#modalTitle").val()) {
        document.getElementById('uploadPublik').submit();
    }
}

function removePub() {
    var id = $(this).data('id');
    var tr = $(this).parent().parent();
    $.ajax({
        type: "POST",
        url: './publikacieFunctions.php',
        data: {'publikacieId': id},
        success: function (data) {
            tr.remove();

        },
        error: function () {
            console.log("failed load");
        }
    });
}

function removeLink() {
    var id = $(this).data('id');
    var tr = $(this).parent().parent();
    $.ajax({
        type: "POST",
        url: './publikacieFunctions.php',
        data: {'publikacieLinkId': id},
        success: function (data) {
            tr.remove();

        },
        error: function () {
            console.log("failed load");
        }
    });
}