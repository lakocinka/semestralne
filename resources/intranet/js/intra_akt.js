/**
 * Created by Tomáš on 24.05.2017.
 */

$(document).ready(function(){

    $(".shoppingTitle").on('click',function (evt) {

        openModal(this);
    });
});
/*
function openModal(selectorOpenModal, nameModalId) {

    var elementOpenModal,
        elementTitle;

    var init = function () {
        elementOpenModal = $(selectorOpenModal);
        elementTitle = $(nameModalId);

        if (elementOpenModal) {
            elementOpenModal.click(onClick);
        }
    };

    var onCreate = function () {
        $(document).ready(onDocumentReady);
    };

    var onDocumentReady = function () {
        init();
    };

    var onClick = function (e) {
        if (elementTitle) {
            var title = $(this).data('title');
            elementTitle.text(title);
            var shoppingId = $(this).data('id');
            $("#shoppingID").empty();
            $("#shoppingID").text(shoppingId);
            loadShoppingDatabase(shoppingId);
            tit = $(this);
        }
    };

    onCreate();

}*/
function openModal(data) {
    console.log(data);
    $('#nameShoppingTitle').text($(data).data('title'));
    var shoppingId = $(data).data('id');
    $("#shoppingID").empty();
    $("#shoppingID").text(shoppingId);
    loadShoppingDatabase(shoppingId);
}

function loadShoppingDatabase(id) {
    console.log("spustam load");
    $.ajax({
        type: "POST",
        url:'../upravy/aktuality.php',
        data: {'id': id},
        success: function (data) {
            //var res = JSON.parse(data);
            console.log(data);
            $("#modalTitle").empty();
           // $("#modalTitle").val(res[0]);
            /*var titleSK=$(".nadpisSK",$(data));
            var titleEN=$(".nadpisEN",$(data));
            var contentSK=$(".contentSK",$(data));
            var contentEN=$(".contentEN",$(data));*/

            var $titleSK=$(data).filter(".nadpisSK");
            var $titleEN=$(data).filter(".nadpisEN");
            var $contentSK=$(data).filter(".contentSK");
            var $contentEN=$(data).filter(".contentEN");

            console.log($contentSK);
            console.log($titleSK);

            $('#modalTitleSK').val($titleSK[0].innerHTML);
            $('#modalTitleEN').val($titleEN[0].innerHTML);


            tinymce.get("shoppingModalSK").setContent('');
            tinymce.get("shoppingModalSK").execCommand('mceInsertContent', false, $contentSK[0].innerHTML);

            tinymce.get("shoppingModalEN").setContent('');
            tinymce.get("shoppingModalEN").execCommand('mceInsertContent', false, $contentEN[0].innerHTML);

        },
        error: function () {
            console.log("failed load");
        }
    });
}

$('#submitShoppingModal').on('click',function (evt) {

    var contentSK = tinymce.get("shoppingModalSK").getContent();
    var contentEN = tinymce.get("shoppingModalEN").getContent();

    var titleSK=$('#modalTitleSK').val();
    var titleEN=$('#modalTitleEN').val();
    var id=$("#shoppingID").text();

    $.ajax({
        type: "POST",
        url: '../upravy/aktuality.php',
        data: {idUpdate: id, titleSK: titleSK, titleEN: titleEN, contentSK: contentSK, contentEN: contentEN},
        success: function (data) {

            $('#createModal').modal('hide');
            console.log(data);
            location.reload();

        }
    })



});

$("#deleteNakup").on('click',function (evt) {

    var id=$("#shoppingID").text();

    $.ajax({
        type: "POST",
        url: '../upravy/aktuality.php',
        data: {deleteID: id},
        success: function (data) {

            $('#createModal').modal('hide');
            location.reload();

        }
    })
});

$('#addButton').on('click',function(){

    console.log("pridavam");
    var contentSK = tinymce.get("addContentSK").getContent();
    var contentEN = tinymce.get("addContentEN").getContent();
    var titleSK=$('#addTitleSK').val();
    var titleEN=$('#addTitleEN').val();

    if(contentEN.includes("'") || contentSK.includes("'") || titleSK.includes("'") || titleSK.includes("'"))
    {
        console.log("true");
        $('.warn').append('<div class="alert alert-danger"> <strong>Pozor!</strong> V nádpise alebo v texte bol zadaný znak \' nahraďte ho znakom " </div>');
       setTimeout(function () {
          $('.warn').empty();
       }, 10000);
       return;
    }
    var kat=$("#event").val();
    if(kat==1)
        kat="propagacia";
    if(kat==2)
        kat="oznamy";
    if(kat==3)
        kat="zoZivota";

    var dat=$("#expiracia").val();
    console.log(dat);

    /*console.log(contentSK);
    console.log(titleSK);
    console.log(contentEN);
    console.log(titleEN);*/
    console.log(kat);


    $.ajax({
        type: "POST",
        url: '../upravy/aktuality.php',
        data: {titleSKK: titleSK, titleEN: titleEN, contentSKK: contentSK, contentEN: contentEN,category:kat,datum:dat},
        success: function (data) {

            $('#createModal').modal('hide');

           location.reload();
           // console.log(data);

        }
    })

});

