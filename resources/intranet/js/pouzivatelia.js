$(document).ready(function () {
    $('#userTable').on('click', '.usersTable', function () {
        openModal(this);
    });
    $("#submitUserModal").click(saveUserProfile);
    addListeners();
    $("#deleteUser").click(deleteUser);
    $("#submitNewUserModal").click(addUser);
    $("#addUserButton").click(clearForm);
});

function openModal(data) {
    var firstname = $(data).data('firstname');
    var lastname = $(data).data('lastname');
    $("#userName").text(firstname + " " + lastname);
    var id = $(data).data('id');
    $("#userID").empty();
    $("#userID").text(id);

    getInfoAboutUser(id);
}

function getInfoAboutUser(id) {
    $.ajax({
        type: "POST",
        url: '../upravy/functions.php',
        data: {'userIdHr': id},
        success: function (data) {
            appentDataToModal(data);
        },
        error: function () {
            console.log("failed load");
        }
    });
}

function appentDataToModal(data) {
    var udaje = $(JSON.parse(data));

    $("input[name=firstname]").val(udaje[0]['firstname']);
    $("input[name=lastname]").val(udaje[0]['lastname']);
    $("input[name=title1]").val(udaje[0]['title1']);
    $("input[name=title2]").val(udaje[0]['title2']);
    $("input[name=ldap]").val(udaje[0]['ldapLogin']);
    $("input[name=photo]").val(udaje[0]['photo']);
    $("input[name=room]").val(udaje[0]['room']);
    $("input[name=phone]").val(udaje[0]['phone']);
    $("input[name=department]").val(udaje[0]['department']);
    $("input[name=staffrole]").val(udaje[0]['staffRole']);
    $("textarea[name=function]").val(udaje[0]['function']);

    console.log(udaje[0]);
    if(udaje[0]['active']){
        $("input[name=active]").attr("checked", "checked");
    }
    else{
        $("input[name=active]").attr("checked", false);
    }




    if ($("#prava").length) {
        $("input[name=admin]").attr("checked", false);
        $("input[name=editor]").attr("checked", false);
        $("input[name=hr]").attr("checked", false);
        $("input[name=reporter]").attr("checked", false);
        $("input[name=user]").attr("checked", false);

        if (udaje[0]['admin'])
            $("input[name=admin]").attr("checked", "checked");
        if (udaje[0]['editor'])
            $("input[name=editor]").attr("checked", "checked");
        if (udaje[0]['hr'])
            $("input[name=hr]").attr("checked", "checked");
        if (udaje[0]['reporter'])
            $("input[name=reporter]").attr("checked", "checked");
        if (udaje[0]['user'])
            $("input[name=user]").attr("checked", "checked");
    }
}

function saveUserProfile() {
    var data = $("#userForm").serialize();
    var id = $("#userID").text();


    $.ajax({
        type: "POST",
        url: '../upravy/functions.php',
        data: {'changeUserProfile': data, "userProfileID": id},
        success: function (data) {
            console.log(data);
            $('#createModal').modal('hide');
            //location.reload();
        },
        error: function () {
            console.log("failed load");
        }
    });

}

function addListeners() {
    var firstname = $("input[name=firstname]");
    var lastname = $("input[name=lastname]");
    var room = $("input[name=room]");
    var departnemt = $("input[name=department]");
    var staffRole = $("input[name=staffrole]");

    firstname.keyup(checkInputs);
    lastname.keyup(checkInputs);
    room.keyup(checkInputs);
    departnemt.keyup(checkInputs);
    staffRole.keyup(checkInputs);

}

function checkInputs() {
    var firstname = $("input[name=firstname]").val();
    var lastname = $("input[name=lastname]").val();
    var room = $("input[name=room]").val();
    var departnemt = $("input[name=department]").val();
    var staffRole = $("input[name=staffrole]").val();
    if (firstname && lastname && room && departnemt && staffRole) {
        $("#submitUserModal").attr('disabled', false);
    }
    else {
        $("#submitUserModal").attr('disabled', 'disabled');
    }

}

function deleteUser() {
    var id = $("#userID").text();

    $.ajax({
        type: "POST",
        url: '../upravy/functions.php',
        data: {'deleteUser': id},
        success: function (data) {
            $('#createModal').modal('hide');
            location.reload();
        },
        error: function () {
            console.log("failed load");
        }
    });

}

function addUser() {
    var data = $("#newUserForm").serialize();
    console.log(data);
    $.ajax({
        type: "POST",
        url: '../upravy/functions.php',
        data: {'addUser': data},
        success: function (data) {
            $('#newUserModal').modal('hide');
            location.reload();

        },
        error: function () {
            console.log("failed load");
        }
    });

}

function clearForm() {
    $("#newUserForm").trigger('reset');
    $("input[name=admin]").attr("checked", false);
    $("input[name=editor]").attr("checked", false);
    $("input[name=hr]").attr("checked", false);
    $("input[name=reporter]").attr("checked", false);
    $("input[name=user]").attr("checked", false);
}
