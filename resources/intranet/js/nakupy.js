var tit;

$(document).ready(function(){
    addListeners();
    openModal(".shoppingTitle", "#nameShoppingTitle");
});

function addListeners() {
    $("#submitShoppingModal").click(saveShopping);
    $("#deleteNakup").click(deleteShopping);


}

function openModal(selectorOpenModal, nameModalId) {

    var elementOpenModal,
        elementTitle;

    var init = function () {
        elementOpenModal = $(selectorOpenModal);
        elementTitle = $(nameModalId);

        if (elementOpenModal) {
            elementOpenModal.click(onClick);
        }
    };

    var onCreate = function () {
        $(document).ready(onDocumentReady);
    };

    var onDocumentReady = function () {
        init();
    };

    var onClick = function (e) {
        if (elementTitle) {
            var title = $(this).data('title');
            elementTitle.text(title);
            var shoppingId = $(this).data('id');
            $("#shoppingID").empty();
            $("#shoppingID").text(shoppingId);
            loadShoppingDatabase(shoppingId);
            tit = $(this);
        }
    };

    onCreate();

}

function loadShoppingDatabase(id) {
    $.ajax({
        type: "POST",
        url:'../nakupy/shoppingFunctions.php',
        data: {'id': id},
        success: function (data) {
            var res = JSON.parse(data);
            $("#modalTitle").empty();
            $("#modalTitle").val(res[0]);
            tinymce.activeEditor.setContent('');
            tinymce.activeEditor.execCommand('mceInsertContent', false, res[1]);
        },
        error: function () {
            console.log("failed load");
        }
    });
}

function saveShopping() {
    var content = tinymce.activeEditor.getContent();
    var title = $("#modalTitle").val();
    var id = $("#shoppingID").text();

    $.ajax({
        type: "POST",
        url:'../nakupy/shoppingFunctions.php',
        data: {'saveContent': content, 'contentID':id, 'title': title},
        success: function (data) {
            $(tit).text(title);
            $('#createModal').modal('hide');
        },
        error: function () {
            console.log("failed load");
        }
    });
}

function deleteShopping(){
    var id = $("#shoppingID").text();
    $.ajax({
        type: "POST",
        url:'../nakupy/shoppingFunctions.php',
        data: {'deleteShopping': id},
        success: function (data) {
            $('#createModal').modal('hide');
            location.reload();
        },
        error: function () {
            console.log("failed load");
        }
    });
}
