/**
 * Created by Lukas on 23.5.2017.
 */
$(document).ready(function(){
    openModal(".studyTitle", "#studyModalTitle");
    $('#submitStudyModal').click(saveStudyText);
});

function openModal(selectorOpenModal, nameModalId) {

    var elementOpenModal,
        elementTitle;

    var init = function () {
        elementOpenModal = $(selectorOpenModal);
        elementTitle = $(nameModalId);

        if (elementOpenModal) {
            elementOpenModal.click(onClick);
        }
    };

    var onCreate = function () {
        $(document).ready(onDocumentReady);
    };

    var onDocumentReady = function () {
        init();
    };

    var onClick = function () {
        if (elementTitle) {
            var title = $(this).data('title');
            elementTitle.text(title);
            var typ = $(this).data('typ');
            loadStudyTextDatabaseSK(typ);
            loadStudyTextDatabaseEN(typ);

        }
    };

    onCreate();

}
function loadStudyTextDatabaseSK(typ) {
    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'studyTypeSK': typ},
        success: function (data) {
            tinymce.get("studyModalSK").setContent('');
            tinymce.get("studyModalSK").execCommand('mceInsertContent', false, data);
            //tinymce.activeEditor.setContent('');
            //tinymce.activeEditor.execCommand('mceInsertContent', false, data);
        },
        error: function () {
            console.log("failed load");
        }
    });
}
function loadStudyTextDatabaseEN(typ) {
    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'studyTypeEN': typ},
        success: function (data) {
            tinymce.get("studyModalEN").setContent('');
            tinymce.get("studyModalEN").execCommand('mceInsertContent', false, data);
            // tinymce.activeEditor.setContent('');
            // tinymce.activeEditor.execCommand('mceInsertContent', false, data);
        },
        error: function () {
            console.log("failed load");
        }
    });
}
function saveStudyText() {
    saveStudyTextSK();
}
function saveStudyTextSK() {
    var content = tinymce.get('studyModalSK').getContent();
    var typ = $("#studyModalTitle").text();

    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'saveStudyContentSK': content, 'type': typ},
        success: function (data) {
            console.log(data);
            $('#createModal').modal('hide');
            saveStudyTextEN();
        },
        error: function () {
            console.log("failed load");
        }
    });
}
function saveStudyTextEN() {
    var content = tinymce.get('studyModalEN').getContent();
    var typ = $("#studyModalTitle").text();

    $.ajax({
        type: "POST",
        url:'../upravy/functions.php',
        data: {'saveStudyContentEN': content, 'type': typ},
        success: function (data) {
            console.log(data);
            $('#createModal').modal('hide');
        },
        error: function () {
            console.log("failed load");
        }
    });
}