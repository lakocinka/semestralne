/**
 * Created by Tomáš on 25.05.2017.
 */
$(document).ready(function(){

    $(".projectTitle").on('click',function (evt) {

        console.log("mier");
        openModal(this);
    });
});

function openModal(data) {
    console.log(data);
    $('#nameShoppingTitle').text($(data).data('title'));
    var shoppingId = $(data).data('id');
    $("#shoppingID").empty();
    $("#shoppingID").text(shoppingId);
    loadShoppingDatabase(shoppingId);

}
function loadShoppingDatabase(id) {
    console.log("spustam load");
    $.ajax({
        type: "POST",
        url:'../upravy/Projekty.php',
        data: {'id': id},
        success: function (data) {
            //var res = JSON.parse(data);
            console.log(data);
            $("#modalTitle").empty();
            // $("#modalTitle").val(res[0]);
            /*var titleSK=$(".nadpisSK",$(data));
             var titleEN=$(".nadpisEN",$(data));
             var contentSK=$(".contentSK",$(data));
             var contentEN=$(".contentEN",$(data));*/

            var $titleSK=$(data).filter(".nadpisSK");
            var $titleEN=$(data).filter(".nadpisEN");
            var $datum=$(data).filter(".datumEnd");
            var $coordinator=$(data).filter(".coordinator");
            var $partners=$(data).filter(".partners");
            var $web=$(data).filter(".web");
            var $annotationSK=$(data).filter(".annotationSK");
            var $annotationEN=$(data).filter(".annotationEN");

         console.log($titleSK);

            $('#modalTitleSK').val($titleSK[0].innerHTML);
            $('#modalTitleEN').val($titleEN[0].innerHTML);
            $('#coordinatorModal').val($coordinator[0].innerHTML);
            $('#partnersModal').val($partners[0].innerHTML);
            $('#dateModal').val($datum[0].innerHTML);
            $('#webodal').val($web[0].innerHTML);

            tinymce.get("projectModalSK").setContent('');
            tinymce.get("projectModalSK").execCommand('mceInsertContent', false, $annotationSK[0].innerHTML);

            tinymce.get("projectModalEN").setContent('');
            tinymce.get("projectModalEN").execCommand('mceInsertContent', false, $annotationEN[0].innerHTML);

        },
        error: function () {
            console.log("failed load");
        }
    });
}
$("#deleteNakup").on('click',function (evt) {

    var id=$("#shoppingID").text();

    $.ajax({
        type: "POST",
        url: '../upravy/Projekty.php',
        data: {deleteID: id},
        success: function (data) {

            $('#createModal').modal('hide');
            location.reload();
            console.log(data);

        }
    })
});

$('#submitShoppingModal').on('click',function (evt) {

    var contentSK = tinymce.get("projectModalSK").getContent();
    var contentEN = tinymce.get("projectModalEN").getContent();

    var titleSK=$('#modalTitleSK').val();
    var titleEN= $('#modalTitleEN').val();
    var coordinator=$('#coordinatorModal').val();
    var partners= $('#partnersModal').val();
    var date=$('#dateModal').val();
    var web=$('#webodal').val();
    var id=$("#shoppingID").text();

    console.log(titleSK);
    console.log(titleEN);
    console.log(coordinator);
    console.log(partners);
    console.log(date);
    console.log(web);
    console.log(id);
    $.ajax({
        type: "POST",
        url: '../upravy/Projekty.php',
        data: {idUpdate: id, uptitleSK: titleSK, uptitleEN: titleEN, upcontentSK: contentSK, upcontentEN: contentEN,upcoordinator:coordinator,uppartners:partners,update:date,upweb:web},
        success: function (data) {

            $('#createModal').modal('hide');
            console.log(data);
            location.reload();

        }
    })



});

$('#addButton').on('click',function(){

    console.log("pridavam");
    var contentSK = tinymce.get("addContentSK").getContent();
    var contentEN = tinymce.get("addContentEN").getContent();
    var titleSK=$('#addTitleSK').val();
    var titleEN=$('#addTitleEN').val();
    var number=$('#number').val();
    var coordinator=$('#addCoordinator').val();
    var partners=$('#addPartners').val();
    var web=$('#addWeb').val();
    var iCode=$('#addCode').val();
    var kat=$("#type").val();
    var dat=$("#addExpiracia").val();
    var datStart=$("#addStart").val();

    if(contentEN.includes("'") || contentSK.includes("'") || titleSK.includes("'") || titleSK.includes("'"))
    {
        console.log("true");
        $('.warn').append('<div class="alert alert-danger"> <strong>Pozor!</strong> V nádpise alebo v texte bol zadaný znak \' nahraďte ho znakom " </div>');
        setTimeout(function () {
            $('.warn').empty();
        }, 10000);
        return;
    }

    //console.log(dat);

    /*console.log(contentSK);
     console.log(titleSK);
     console.log(contentEN);
     console.log(titleEN);*/
    //console.log(kat);

    console.log(contentSK);

    $.ajax({
        type: "POST",
        url: '../upravy/Projekty.php',
        data: { addtitleSK: titleSK,
                addtitleEN: titleEN,
                addcontentSK: contentSK,
                addcontentEN: contentEN,
                addcategory:kat,
                adddatum:dat,
                addcoordinator:coordinator,
                addpartners:partners,
                addweb:web,
                addcode:iCode,
                addnumber:number,
                addStartDat:datStart
        },
        success: function (data) {

            $('#createModal').modal('hide');

            location.reload();
            console.log(data);

        }
    })

});