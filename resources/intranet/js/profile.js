$(document).ready(function () {
    getInfo();
    $("#saveProfile").click(saveData);
});

function getInfo() {
    $.ajax({
        type: "POST",
        url: '../profile/profileFunction.php',
        data: {'getData': "true"},
        success: function (data) {
            var json = JSON.parse(data);
            insertData(json);
        },
        error: function () {
            console.log("failed load");
        }
    });

}

function insertData(udaje) {
    $("input[name=firstname]").val(udaje['firstname']);
    $("input[name=lastname]").val(udaje['lastname']);
    $("input[name=title1]").val(udaje['title1']);
    $("input[name=title2]").val(udaje['title2']);
    $("#photo").attr("src",udaje['photo']);
    //$("input[name=photo]").val(udaje[0]['photo']);
    $("input[name=room]").val(udaje['room']);
    $("input[name=phone]").val(udaje['phone']);
    $("input[name=department]").val(udaje['department']);
    $("input[name=staffRole]").val(udaje['staffRole']);
    $("textarea[name=function]").val(udaje['function']);

}

function saveData() {
    var data = $("#userForm").serialize();
    document.getElementById('userForm').submit();
    // $.ajax({
    //     type: "POST",
    //     url: '../profile/profileFunction.php',
    //     data: {'saveProfile': data},
    //     success: function (data) {
    //         location.reload();
    //     },
    //     error: function () {
    //         console.log("failed load");
    //     }
    // });
}