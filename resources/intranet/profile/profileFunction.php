<?php

session_start();


if (isset($_POST['getData'])) {
    createProfile();
}
if (isset($_POST['saveProfile'])) {
    saveProfile($_POST['saveProfile']);
}
if (isset($_FILES['FileUpload']) && !empty($_FILES['FileUpload']['name'])) {

    savePhoto();

}

function savePhoto()
{
    $type = basename($_FILES['FileUpload']['type']);
    $name = $_POST['firstname'] . "_" . $_POST['lastname'] . "." . $type;
    $dest = "../staffImage/" . $_POST['firstname'] . "_" . $_POST['lastname'] . "." . $type;
    if(file_exists($dest)){
        unlink($dest);
        move_uploaded_file($_FILES['FileUpload']['tmp_name'], $dest);
    }else{
        move_uploaded_file($_FILES['FileUpload']['tmp_name'], $dest);
    }
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "UPDATE users SET  photo='" . $name ."' WHERE ldapLogin='" . $_SESSION['username'] . "' AND lastname='".$_POST['lastname']."'";
    $conn->query($sql);

    header("location: profile.php");

}

function createProfile()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM users WHERE firstname='" . $_SESSION['firstname'] . "' AND lastname='" . $_SESSION['lastname'] . "' AND ldapLogin='" . $_SESSION['username'] . "'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    if ($row['photo']) {
        $row['photo'] = "/webtech/resources/intranet/staffImage/" . $row['photo'];
    } else {
        $row['photo'] = "/webtech/resources/intranet/image/user.jpg";
    }

    print_r(json_encode($row));
}

function saveProfile($data)
{
    $params = array();
    parse_str($data, $params);
    //var_dump($params);

    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "UPDATE users SET  title1='" . $params['title1'] . "',
                             title2='" . $params['title2'] . "',
                             room='" . $params['room'] . "',
                             phone=" . $params['phone'] . ",
                             department='" . $params['department'] . "',
                             staffRole='" . $params['staffRole'] . "',
                             function='" . $params['function'] . "' WHERE ldapLogin='" . $_SESSION['username'] . "'";
    $conn->query($sql);


}