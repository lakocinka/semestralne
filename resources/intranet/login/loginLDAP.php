<?php
require "../../../database/config.php";

if(isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    session_start();

    $ldappass = $password;

    $dn = 'ou=People, DC=stuba, DC=sk';
    $ldaprdn = "uid=" . $username . "," . $dn;

    $ldapconn = ldap_connect("ldap.stuba.sk");
    ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

    if ($ldapconn) {

        $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);


        if ($ldapbind) {
            //echo "LDAP bind successful...";
            $results = ldap_search($ldapconn, $dn, "uid=$username", array("*"));
            $info = ldap_get_entries($ldapconn, $results);

            $conn = new mysqli($servernameDB,$usernameDB,$passwordDB,$dbname);
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            mysqli_set_charset($conn, "utf8");

            $firstname = $info[0]['givenname'][0];
            $lastname = $info[0]['sn'][0];
            $uname = $info[0]['uid'][0];
            $email = $info[0]['mail'][2];
            $aisID = $info[0]['uisid'][0];


            $sql = "SELECT id, firstname, lastname FROM users WHERE ldapLogin='$uname'";
            $result = $conn->query($sql);

            if($result->num_rows > 0)
            {
                $sqlUser = "SELECT * FROM users WHERE ldapLogin='$uname'";
                $res2 = $conn->query($sqlUser);
                $row = $res2->fetch_assoc();

                $role = array();
                if($row['admin'] == "1"){
                    array_push($role, "admin");
                }
                if($row['editor'] == "1"){
                    array_push($role, "editor");
                }
                if($row['hr'] == "1"){
                    array_push($role, "hr");
                }
                if($row['reporter'] == "1"){
                    array_push($role, "reporter");
                }
                if($row['user'] == "1"){
                    array_push($role, "user");
                }

                if($row['active'] == '1'){
                    $_SESSION['role']=$role;
                    $_SESSION['photo']=$row['photo'];
                    $_SESSION['firstname'] = $row['firstname'];
                    $_SESSION['lastname'] = $row['lastname'];
                    $_SESSION['email']=$email;
                    $_SESSION['username']=$uname;
                    header("location: ../index.php");
                }
                else{
                    $_SESSION['message']="User is inactive";
                    header("location: ./login.php");
                }


            }
            else{
                $_SESSION['message']="Username/password incorrect or permission denied";
                header("location: ./login.php");

            }




        } else {
            $_SESSION['message']="Username/password incorrect or permission denied";
            //echo "LDAP bind failed...";
        }

    }
}
?>
