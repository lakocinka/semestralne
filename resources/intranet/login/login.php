<?php
session_start();

if(isset($_POST['loginLDAP'])){
    include "loginLDAP.php";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WEBTech | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../css/ltestyle.css">


    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="../../index.php"><b>Web</b>Tech</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php
            if(isset($_SESSION['message'])){
                echo "<div class='login-box-msg alert-warning'>" . $_SESSION['message'] . "</div>";
            }
        ?>
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="login.php" method="post" id="form1">
            <div class="form-group ">
                <div class="input-group">
                    <input type="text" class="form-control" name="username" placeholder="Username">
                    <a href="#" class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </a>
                </div>
            </div>
            <div class="form-group ">
                <div class="input-group">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                    <a href="#" class="input-group-addon">
                        <i class="fa fa-key"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 pull-right">
                    <button type="submit" name="loginLDAP" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>

        </form>
    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="../js/myscripts.js"></script>
<script src="../js/lte.js"></script>

</body>
</html>
