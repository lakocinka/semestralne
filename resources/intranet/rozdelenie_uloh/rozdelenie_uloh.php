<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("location: ../login/login.php");
}
require "../functions/roles.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Rozdelenie úloh</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="../css/ltestyle.css">
    <link rel="stylesheet" href="../css/style.css">


    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <a href="../index.php" class="logo">
            <span class="logo-mini"><b>WEB</b></span>
            <span class="logo-lg"><b>WEB</b>tech</span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo getUserImagePath(); ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs">
                                <?php
                                userPanel();
                                ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo getUserImagePath(); ?>" class="img-circle" alt="User Image">

                                <p>
                                    <?php
                                    userMenu();
                                    ?>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="../profile/profile.php" class="btn btn-default btn-flat">Profil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="../login/logout.php" class="btn btn-default btn-flat">Odhlásiť</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">

        <section class="sidebar">

            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo getUserImagePath(); ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php
                        userPanel();
                        ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <ul class="sidebar-menu">
                <li class="header">Main navigation</li>
                <li class="treeview">
                    <a href="../index.php">
                        <i class="fa fa-home"></i> <span>Domov</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../pedagogika/pedagogika.php">
                        <i class="fa fa-graduation-cap"></i> <span>Pedagogika</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../doktorandi/doktorandi.php">
                        <i class="fa fa-group"></i> <span>Doktorandi</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../publikacie/publikacie.php">
                        <i class="fa fa-book"></i> <span>Publikácie</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../sluzobne_cesty/sluzobne_cesty.php">
                        <i class="fa fa-car"></i> <span>Služobné cesty</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../nakupy/nakupy.php">
                        <i class="fa fa-shopping-cart"></i> <span>Nákupy</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../dochadzka/dochadzka.php">
                        <i class="fa fa-table"></i> <span>Dochádzka</span>
                    </a>
                </li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-tasks"></i> <span>Rozdelenie úloh</span>
                    </a>
                </li>
                <?php
                if (in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                    echo ' <li class="treeview"><a href="../newsletter/newsletter.php"><i class="fa fa-envelope-o"></i> <span>Newsletter</span></a></li>';
                }
                if (in_array("reporter", $_SESSION['role']) || in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                    echo '<li class="treeview">
                    <a href="#"><i class="fa fa-pencil-square-o"></i> <span>Úpravy</span></a>
                    <ul class="treeview-menu menu-open" style="display: none;">';
                    if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/pouzivatelia.php"><i class="fa fa-circle-o"></i>Používatelia</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/aktuality.php"><i class="fa fa-circle-o"></i>Aktuality</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/Projekty.php"><i class="fa fa-circle-o"></i>Projekty</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/fotky.php"><i class="fa fa-circle-o"></i>Fotky</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/videa.php"><i class="fa fa-circle-o"></i>Videá</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role']) || in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/media.php"><i class="fa fa-circle-o"></i>Média</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/oNas.php"><i class="fa fa-circle-o"></i>O nás</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/vyskumne_oblasti.php"><i class="fa fa-circle-o"></i>Výskumné oblasti</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/studium.php"><i class="fa fa-circle-o"></i>Štúdium</a></li>';
                    }
                    echo '</ul></li>';
                }
                ?>


            </ul>
        </section>
    </aside>


    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Rozdelenie úloh
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-file"></i> Intranet</a></li>
                <li class="active">Rozdelenie úloh</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <table id="tasksTable" class="table table-bordered table-hover">
                        <thead>
                        <th></th>
                        <th>Tomáš Michalica</th>
                        <th>Patrik Zošš</th>
                        <th>Lukáš Molčan</th>
                        <th>Martin Mochnacký</th>
                        <th>Michal Michňa</th>
                        </thead>
                        <tbody>
                        <tr><th>1</th><td class="cellCreate"></td><td class="cellCreate"></td><td class="cellCreate"></td><td class="cellCreate"></td><td class="cellCreate"></td></tr>
                        <tr><th>2</th><td></td><td></td><td></td><td></td><td></td></tr>
                        <tr><th>3</th><td></td><td></td><td class="cellCreate"></td><td></td><td></td></tr>
                        <tr><th>4</th><td></td><td></td><td class="cellCreate"></td><td></td><td></td></tr>
                        <tr><th>5</th><td></td><td></td><td></td><td></td><td></td></tr>
                        <tr><th>6</th><td></td><td class="cellCreate"></td><td></td><td></td><td></td></tr>
                        <tr><th>7</th><td></td><td></td><td></td><td></td><td></td></tr>
                        <tr><th>8</th><td class="cellCreate"></td><td></td><td></td><td></td><td></td></tr>
                        <tr><th>9</th><td class="cellCreate"></td><td></td><td></td><td></td><td></td></tr>
                        <tr><th>10</th><td></td><td class="cellCreate"></td><td></td><td></td><td></td></tr>
                        <tr><th>11</th><td></td><td></td><td></td><td class="cellCreate"></td><td></td></tr>
                        <tr><th>12</th><td></td><td></td><td></td><td class="cellCreate"></td><td></td></tr>
                        <tr><th>13</th><td></td><td></td><td></td><td class="cellCreate"></td><td></td></tr>
                        <tr><th>14</th><td></td><td></td><td></td><td></td><td class="cellCreate"></td></tr>
                        <tr><th>15</th><td></td><td></td><td></td><td></td><td class="cellCreate"></td></tr>


                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.01
        </div>

    </footer>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- AdminLTE App -->
<script src="../js/lte.js"></script>

</body>
</html>

