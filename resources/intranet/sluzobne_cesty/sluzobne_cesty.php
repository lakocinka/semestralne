<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("location: ../login/login.php");
}
require "../functions/roles.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Služobné cesty </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="../css/ltestyle.css">
    <link rel="stylesheet" href="../css/style.css">



    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <a href="../index.php" class="logo">
            <span class="logo-mini"><b>WEB</b></span>
            <span class="logo-lg"><b>WEB</b>tech</span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo getUserImagePath();?>" class="user-image" alt="User Image">
                            <span class="hidden-xs">
                                <?php
                                userPanel();
                                ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo getUserImagePath();?>" class="img-circle" alt="User Image">

                                <p>
                                    <?php
                                    userMenu();
                                    ?>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="../profile/profile.php" class="btn btn-default btn-flat">Profil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="../login/logout.php" class="btn btn-default btn-flat">Odhlásiť</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">

        <section class="sidebar">

            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo getUserImagePath();?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php
                        userPanel();
                        ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <ul class="sidebar-menu">
                <li class="header">Main navigation</li>
                <li class="treeview">
                    <a href="../index.php">
                        <i class="fa fa-home"></i> <span>Domov</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../pedagogika/pedagogika.php">
                        <i class="fa fa-graduation-cap"></i> <span>Pedagogika</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../doktorandi/doktorandi.php">
                        <i class="fa fa-group"></i> <span>Doktorandi</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../publikacie/publikacie.php">
                        <i class="fa fa-book"></i> <span>Publikácie</span>
                    </a>
                </li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-car"></i> <span>Služobné cesty</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../nakupy/nakupy.php">
                        <i class="fa fa-shopping-cart"></i> <span>Nákupy</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../dochadzka/dochadzka.php">
                        <i class="fa fa-table"></i> <span>Dochádzka</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../rozdelenie_uloh/rozdelenie_uloh.php">
                        <i class="fa fa-tasks"></i> <span>Rozdelenie úloh</span>
                    </a>
                </li>
                <?php
                if (in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                    echo ' <li class="treeview"><a href="../newsletter/newsletter.php"><i class="fa fa-envelope-o"></i> <span>Newsletter</span></a></li>';
                }
                if (in_array("reporter", $_SESSION['role']) || in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                    echo '<li class="treeview">
                    <a href="#"><i class="fa fa-pencil-square-o"></i> <span>Úpravy</span></a>
                    <ul class="treeview-menu menu-open" style="display: none;">';
                    if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/pouzivatelia.php"><i class="fa fa-circle-o"></i>Používatelia</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/aktuality.php"><i class="fa fa-circle-o"></i>Aktuality</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/Projekty.php"><i class="fa fa-circle-o"></i>Projekty</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/fotky.php"><i class="fa fa-circle-o"></i>Fotky</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/videa.php"><i class="fa fa-circle-o"></i>Videá</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role']) || in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/media.php"><i class="fa fa-circle-o"></i>Média</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/oNas.php"><i class="fa fa-circle-o"></i>O nás</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/vyskumne_oblasti.php"><i class="fa fa-circle-o"></i>Výskumné oblasti</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/studium.php"><i class="fa fa-circle-o"></i>Štúdium</a></li>';
                    }
                    echo '</ul></li>';
                }
                ?>


            </ul>
        </section>
    </aside>


    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Služobné cesty
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-file"></i> Intranet</a></li>
                <li class="active">Služobné cesty</li>
            </ol>
        </section>

        <section class="content">
            <?php
            if (in_array("editor", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                echo '
            <div class="row">
                <a class="quick-access" name="add_cesty" id="addCesty" href="#" data-toggle="modal"
                   data-target=".cestyModal">
                    <div class="col-md-3 col-sm-6 col-xs-12 quick-select">
                        <div class="small-info-box">
                            <span class="small-info-box-icon bg-green"><i class="fa ion-plus-round"></i></span>

                            <div class="small-info-box-content">
                                <span class="small-info-box-text">Pridať dokument</span>
                            </div>
                        </div>
                    </div>
                </a>
                <a class="quick-access" name="remove_cesty" id="deleteCesty" href="#" data-toggle="modal"
                   data-target=".deleteCesty">
                    <div class="col-md-3 col-sm-6 col-xs-12 quick-select">
                        <div class="small-info-box">
                            <span class="small-info-box-icon bg-red"><i class="fa ion-minus-round"></i></span>

                            <div class="small-info-box-content">
                                <span class="small-info-box-text">Vymazať dokument</span>
                            </div>
                        </div>
                    </div>
                </a>
                <a class="quick-access" name="add_cestyLinks" id="addDoktorandiLink" href="#" data-toggle="modal"
                   data-target=".cestyModalLink">
                    <div class="col-md-3 col-sm-6 col-xs-12 quick-select">
                        <div class="small-info-box">
                            <span class="small-info-box-icon bg-green"><i class="fa ion-plus-round"></i></span>

                            <div class="small-info-box-content">
                                <span class="small-info-box-text">Pridať odkaz</span>
                            </div>
                        </div>
                    </div>
                </a>
                <a class="quick-access" name="delete_CestyLinks" id="deleteCestyLink" href="#" data-toggle="modal"
                   data-target=".deleteCestyLink">
                    <div class="col-md-3 col-sm-6 col-xs-12 quick-select">
                        <div class="small-info-box">
                            <span class="small-info-box-icon bg-red"><i class="fa ion-minus-round"></i></span>

                            <div class="small-info-box-content">
                                <span class="small-info-box-text">Vymazať odkaz</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>';
            }
            ?>
            <?php
            if ($_SESSION['fileExist']) {
                echo "<div class='col-md-6 alert-danger'>" . $_SESSION['fileExist'] . "</div>";
                unset($_SESSION['fileExist']);
            }
            ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3>Dokumenty</h3>
                        </div>
                        <div class="box-body">
                            <?php
                            require "sluzobneCestyFunctions.php";
                            createCestyTable();
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3>Odkazy</h3>
                        </div>
                        <div class="box-body">
                            <?php
                            createTableLinks();
                            ?>
                        </div>
                    </div>
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.01
        </div>

    </footer>

</div>
<?php
if (in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
    echo '<div class="modal fade cestyModal" role="dialog" id="createModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Vložiť nový dokument</span></h2>
            </div>
            <div class="modal-body" id="modalCreateBody">
                <form action="sluzobneCestyFunctions.php" id="uploadCesty" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-4">
                        <label for="title">Názov:</label>
                            <input class="form-control" name="title" id="modalTitle">

                    </div>
                    
                    <div class="form-group col-md-4">
                        <label for="title">Kategória:</label>
                        <div class="input-group">
                        <select class="form-control" id="category" name="category">
                            
                            ';

    getCategory();

    echo '</select>
<a id="plusCat" href="#" class="input-group-addon"><i class="fa fa-plus" style="color: green;"></i></a></div>
</div>
<div class="form-group col-md-4" id="addCat" style="display: none">
<label>Nová kategória</label>
<div class="input-group">
    
    <input type="text" class="form-control" id="newCategory" placeholder="Názov">
    <a href="#" id="vytvorCat" class="input-group-addon btn btn-success" style="color: #1f1f1f">Vytvor</a>
</div>
</div>
<div class="form-group">
    <label for="content">Súbor:
        <input type="file" name="pedagogikaFileUpload" class="form-control">
    </label>
</div>

</form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModal">
        Zavrieť
    </button>
    <input type="submit" value="Ulož" class="btn btn-success" id="submitCestyModal">

</div>
</div>
</div>
</div>';
}

if (in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
    echo '<div class="modal fade deleteCesty" role="dialog" id="deleteModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;</button>
                    <h2 class="modal-title">Vymazať dokument</span></h2>
                </div>
                <div class="modal-body" id="modaldeleteBody">';
    createModalTable();
    echo '      </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModal">
                        Zavrieť
                    </button>
                </div>
            </div>
        </div>
    </div>';
}

if (in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
    echo '<div class="modal fade cestyModalLink" role="dialog" id="addLinkModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Pridať odkaz</h2>
            </div>
            <div class="modal-body" id="modalAddLinkBody">
                <form id="addLink">
                    <div class="form-group">
                        <label>Názov:</label>
                        <input type="text" name="linkname" id="linkName" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Link:</label>
                        <input type="text" name="link" id="link" class="form-control">
                    </div>
                </form> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModal">
                    Zavrieť
                </button>
                <input type="submit" class="btn btn-success" id="createLink" value="Vytvor">
                    
               
            </div>
        </div>
    </div>
</div>';
}
if (in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
    echo '<div class="modal fade deleteCestyLink" role="dialog" id="deleteLinkModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;</button>
                    <h2 class="modal-title">Vymazať odkaz</span></h2>
                </div>
                <div class="modal-body" id="modaldeleteLinkBody">';
    createModalTableLinks();
    echo '      </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="closeLinkModal">
                        Zavrieť
                    </button>
                </div>
            </div>
        </div>
    </div>';
}
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- AdminLTE App -->
<script src="../js/lte.js"></script>
<script src="../js/sluzobneCesty.js"></script>


</body>
</html>

