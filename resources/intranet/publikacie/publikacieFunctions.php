<?php
session_start();

if (isset($_FILES['pedagogikaFileUpload']) && !empty($_FILES['pedagogikaFileUpload']['name'])) {
    saveData();

}
if (isset($_POST['publikacieId'])) {
    deleteData($_POST['publikacieId']);
}

if(isset($_POST['addCategory'])){
    addCategory($_POST['addCategory']);
}

if(isset($_POST['linkName']) && isset($_POST['link'])){
    insertLink($_POST['linkName'], $_POST['link']);
}
if (isset($_POST['publikacieLinkId'])){
    deleteLink($_POST['publikacieLinkId']);
}

function deleteLink($id){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql1 = "DELETE FROM links WHERE id=" . $id;
    $conn->query($sql1);
}

function insertLink($name, $link){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "INSERT INTO links (title, link, category) VALUES ('" . $name . "','".$link."','publikacie')";
    $conn->query($sql);
}

function addCategory($name){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql1 = "SELECT * FROM documentCategory WHERE name=" . $name;
    $result = $conn->query($sql1);

    if ($result->num_rows == 0){
        $sql2 = "INSERT INTO documentCategory (name) VALUES ('" . $name . "')";
        $conn->query($sql2);
    }
}

function deleteData($id)
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql1 = "SELECT * FROM publikacie WHERE id=" . $id;
    $result = $conn->query($sql1);
    $link = $result->fetch_assoc()['link'];
    // unlink($link);


    $sql2 = "DELETE FROM publikacie WHERE id=" . $id;
    $conn->query($sql2);
}

function saveData()
{
    require "../../../database/config.php";
    $dest = "../documents/" . basename($_FILES['pedagogikaFileUpload']['name']);
    move_uploaded_file($_FILES['pedagogikaFileUpload']['tmp_name'], $dest);

    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT id FROM documentCategory WHERE name='" . $_POST['category'] . "'";
    $result = $conn->query($sql);
    $category = $result->fetch_assoc()['id'];
    $name = $_POST['title'];
    $link = "../documents/" . basename($_FILES['pedagogikaFileUpload']['name']);

    $sql2 = "SELECT * FROM publikacie WHERE link='" . $link . "'";
    $result2 = $conn->query($sql2);
    if ($result2->num_rows < 1) {
        $sql3 = "INSERT INTO publikacie (category_id, title, link) VALUES ('" . $category . "','" . $name . "','" . $link . "')";
        $conn->query($sql3);
    } else {
        $_SESSION['fileExist'] = "Nahrávaný súbor už existuje pod názvom " . $result2->fetch_assoc()['title'];
    }
    header("location: publikacie.php");
}

function createPublikacieTable()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM documentCategory";
    $result = $conn->query($sql);
    echo "<table class='table table-bordered' id='mainTable'><thead><th>Dokument</th><th>Príloha</th></thead><tbody>";
    while ($row = $result->fetch_assoc()) {
        $sql2 = "SELECT * FROM publikacie as p JOIN documentCategory as d ON p.category_id=d.id WHERE d.name='" . $row['name'] . "'";
        $result2 = $conn->query($sql2);
        if ($result2->num_rows > 0) {
            echo "<tr><td><b>" . $row['name'] . ":</b></td><td>";
            while ($row2 = $result2->fetch_assoc()) {
                echo "<i class='fa fa-file'></i><a href='" . $row2['link'] . "'>" . $row2['title'] . "</a><br>";
            }
            echo "</td></tr>";
        }
    }
    echo "</tbody></table>";
}

function getCategory()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM documentCategory";
    $result = $conn->query($sql);
    while ($row = $result->fetch_assoc()) {
        echo "<option>" . $row['name'] . "</option>";
    }

}

function createModalTable()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM publikacie";
    $result = $conn->query($sql);
    echo "<table class='table table-bordered'><thead><th>Názov</th><th>Link</th><th>-</th></thead><tbody>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row['title'] . "</td><td>" . $row['link'] . "</td><td><a href='#' data-id='" . $row['id'] . "' class='btn btn-danger remPub'>Vymaž</a></td></tr>";
    }
    echo "</tbody></table>";
}

function createModalTableLinks()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM links WHERE category='publikacie'";
    $result = $conn->query($sql);
    echo "<table class='table table-bordered'><thead><th>Názov</th><th>Link</th><th>-</th></thead><tbody>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row['title'] . "</td><td>" . $row['link'] . "</td><td><a href='#' data-id='" . $row['id'] . "' class='btn btn-danger remLink'>Vymaž</a></td></tr>";
    }
    echo "</tbody></table>";
}

function createTableLinks()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM links WHERE category='publikacie'";
    $result = $conn->query($sql);
    echo "<table class='table table-bordered'><thead><th>Odkazy</th></thead><tbody>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td><a href='".$row['link']."'>".$row['title']."</a></td></tr>";
    }
    echo "</tbody></table>";
}