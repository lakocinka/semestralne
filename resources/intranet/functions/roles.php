<?php

//session_start();


function writeRole($roles)
{
    $ret = "";
    foreach ($roles as $r) {
        $ret = $ret . $r . " - ";
    }
    $ret = substr($ret, 0, -2);
    return $ret;
}

function userMenu()
{
    if (isset($_SESSION['firstname']) && isset($_SESSION['lastname'])) {
        echo $_SESSION['firstname'] . " " . $_SESSION['lastname'];
    } else {
        echo $_SESSION['username'];
    }
    echo "<br>" . writeRole($_SESSION['role']);
}

function userPanel()
{
    if (isset($_SESSION['firstname']) && isset($_SESSION['lastname'])) {
        echo $_SESSION['firstname'] . " " . $_SESSION['lastname'];
    } else {
        echo $_SESSION['username'];
    }

}

function getUserImagePath(){
    if($_SESSION['photo']){
        $path = "/webtech/resources/intranet/staffImage/" . $_SESSION['photo'];
    }
    else{
        $path = "/webtech/resources/intranet/image/user.jpg";

    }
    return $path;
}
