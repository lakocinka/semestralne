<?php
function getUsers(){
    require "../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "SELECT COUNT(*) as c FROM users";
    $result = $conn->query($sql);
    echo $result->fetch_assoc()['c'];
}

function getActiveUsers(){
    require "../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "SELECT COUNT(*) as c FROM users WHERE active=1";
    $result = $conn->query($sql);
    echo $result->fetch_assoc()['c'];
}

function getAdmin(){
    require "../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "SELECT COUNT(*) as c FROM users WHERE admin='1'";
    $result = $conn->query($sql);
    echo $result->fetch_assoc()['c'];
}

function getAktuality(){
    require "../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "SELECT COUNT(*) as c FROM Aktuality";
    $result = $conn->query($sql);
    echo $result->fetch_assoc()['c'];
}