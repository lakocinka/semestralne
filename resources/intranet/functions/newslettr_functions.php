<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 25.05.2017
 * Time: 20:01
 */

function createTableNews()
{
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    $sql = "SELECT * FROM Newsletter";
    $result = $conn->query($sql);
    $numRow = 1;
    echo "<table id='shoppingTable' class='table table-bordered'><thead><th>#</th><th>Email</th><th>Jazyk</th></thead><tbody class='shoppingTableBody'>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr><td>$numRow</td><td>".$row["email"]."</td><td>".$row["lang"]."</td><td><a data-id='".$row['id']."' class='btn btn-danger pull-left deleteNakup'  >Vymazať</a></td></tr>";
        $numRow++;
    }
    echo "</tbody></table>";


}
function deleteSub($id){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");
    $sql = "DELETE FROM Newsletter WHERE id=". $id;
    $conn->query($sql);
}

function sendNews($id,$predmet,$message){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    if($id=="sk"){
        $sql = "SELECT * FROM Newsletter WHERE lang='sk'";
    }else{

        $sql = "SELECT * FROM Newsletter WHERE lang='en'";
    }
    $result = $conn->query($sql);


    while($row = $result->fetch_assoc()){

        mail($row["email"],$predmet,$message);
    }


}