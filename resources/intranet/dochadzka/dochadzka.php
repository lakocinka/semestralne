<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("location: ../login/login.php");
}
require "../functions/roles.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dochádzka</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="../css/ltestyle.css">
    <link rel="stylesheet" href="../css/dochadzka.css">
    <link rel="stylesheet" href="../css/pagination.css">


    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
</head>






<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <a href="../index.php" class="logo">
            <span class="logo-mini"><b>WEB</b></span>
            <span class="logo-lg"><b>WEB</b>tech</span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo getUserImagePath(); ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs">
                                <?php
                                userPanel();
                                ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo getUserImagePath(); ?>" class="img-circle" alt="User Image">

                                <p>
                                    <?php
                                    userMenu();
                                    ?>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="../profile/profile.php" class="btn btn-default btn-flat">Profil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="../login/logout.php" class="btn btn-default btn-flat">Odhlásiť</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">

        <section class="sidebar">

            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo getUserImagePath(); ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php
                        userPanel();
                        ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <ul class="sidebar-menu">
                <li class="header">Main navigation</li>
                <li class="treeview">
                    <a href="../index.php">
                        <i class="fa fa-home"></i> <span>Domov</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../pedagogika/pedagogika.php">
                        <i class="fa fa-graduation-cap"></i> <span>Pedagogika</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../doktorandi/doktorandi.php">
                        <i class="fa fa-group"></i> <span>Doktorandi</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../publikacie/publikacie.php">
                        <i class="fa fa-book"></i> <span>Publikácie</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../sluzobne_cesty/sluzobne_cesty.php">
                        <i class="fa fa-car"></i> <span>Služobné cesty</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../nakupy/nakupy.php">
                        <i class="fa fa-shopping-cart"></i> <span>Nákupy</span>
                    </a>
                </li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-table"></i> <span>Dochádzka</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="../rozdelenie_uloh/rozdelenie_uloh.php">
                        <i class="fa fa-tasks"></i> <span>Rozdelenie úloh</span>
                    </a>
                </li>
                <?php
                if (in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                    echo ' <li class="treeview"><a href="../newsletter/newsletter.php"><i class="fa fa-envelope-o"></i> <span>Newsletter</span></a></li>';
                }
                if (in_array("reporter", $_SESSION['role']) || in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                    echo '<li class="treeview">
                    <a href="#"><i class="fa fa-pencil-square-o"></i> <span>Úpravy</span></a>
                    <ul class="treeview-menu menu-open" style="display: none;">';
                    if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/pouzivatelia.php"><i class="fa fa-circle-o"></i>Používatelia</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/aktuality.php"><i class="fa fa-circle-o"></i>Aktuality</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/Projekty.php"><i class="fa fa-circle-o"></i>Projekty</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/fotky.php"><i class="fa fa-circle-o"></i>Fotky</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/videa.php"><i class="fa fa-circle-o"></i>Videá</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role']) || in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/media.php"><i class="fa fa-circle-o"></i>Média</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/oNas.php"><i class="fa fa-circle-o"></i>O nás</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/vyskumne_oblasti.php"><i class="fa fa-circle-o"></i>Výskumné oblasti</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="../upravy/studium.php"><i class="fa fa-circle-o"></i>Štúdium</a></li>';
                    }
                    echo '</ul></li>';
                }
                ?>


            </ul>
        </section>
    </aside>


    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Dochádzka
            </h1>
            <ol class="breadcrumb">
                <li><a href="../index.php"><i class="fa fa-file"></i> Intranet</a></li>
                <li class="active">Dochádzka</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form id="form1" method="post">
                        <div class="form-group col-md-3">
                            <label>Vyber rok:</label>
                            <select name="rok" class="form-control" id="rok">
                                <option value="2019">2019</option>
                                <option value="2018">2018</option>
                                <option value="2017" selected>2017</option>
                                <option value="2016">2016</option>
                                <option value="2015">2015</option>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label>Vyber mesiac:</label>
                            <select name="mesiac" class="form-control" id="mesiac">
                                <option value="1" selected>Január</option>
                                <option value="2">Február</option>
                                <option value="3">Marec</option>
                                <option value="4">Apríl</option>
                                <option value="5">Máj</option>
                                <option value="6">Jún</option>
                                <option value="7">Júl</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">Október</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label>Načítaj</label><br>
                            <input type="submit" class="btn btn-success" value="Načítaj" id="submit">
                        </div>
                    </form>
                    <?php
                    if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<div class="form-group col-md-2">
                                <label>Upraviť tabuľku</label><br>
                                <button class="btn btn-warning" id="edit">Upraviť</button>
                                <button class="btn btn-danger" id="cancelEdit">Zrušiť úpravy</button>
                            </div>
                            <div class="form-group col-md-2" id="nepritomnost">
                                <label>Vyber typ:</label>
                                <select name="typNeprit" class="form-control" id="type">
                                    <option value="D">Dovolenka</option>
                                    <option value="PN">Práce neschopný</option>
                                    <option value="OČR">Opatera člena rodiny</option>
                                    <option value="SC">Sluzobná cesta</option>
                                    <option value="PD">Plán dovolenky</option>
        
                                </select>
                            </div>';
                    }
                    ?>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 table-responsive" id="myTable">
                    <?php include "func.php"; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group ">
                        <div class="alert alert-success pull-left" role="alert" id="saveAlert">
                            <strong>Hotovo!</strong> Zmeny boli uložené
                        </div>
                        <button class="btn btn-success pull-right" id="save">Uložiť</button>
                    </div>
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.01
        </div>

    </footer>

</div>
<?php
if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("user", $_SESSION['role'])) {

    echo '<div class="modal fade universal" role="dialog" id="createModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">&times;</button>
                <h2 class="modal-title"><span id="name"></span></h2>
                <div class="form-group col-md-6 pull-right">
                    <label>Select type:</label>
                    <select name="personAbsenc" class="form-control" id="personalType">';
    if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
        echo "<option value=\"D\">Dovolenka</option>";
    }
    if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("user", $_SESSION['role'])) {
        echo "<option value=\"PD\">Plán dovolenky</option>";
    }
    if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
        echo "<option value=\"OČR\">Opatera člena rodiny</option>
                        <option value=\"SC\">Sluzobná cesta</option>
                        <option value=\"PN\">Práce neschopný</option>";
    }


    echo '        </select>
                </div>
            </div>
            <div class="modal-body" id="modalBody">

            </div>
            <div class="modal-footer">
                <div class="alert alert-success pull-left" role="alert" id="successAlert">
                    <strong>Hotovo!</strong> Zmeny boli uložené
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModal">
                    Zrušiť
                </button>
                <input type="submit" value="SAVE" class="btn btn-success" id="submitModal">
            </div>
        </div>
    </div>
</div>';
}
?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- AdminLTE App -->
<script src="../js/lte.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.min.js"></script>
<script src="../js/dochadzka.js"></script>
<script src="../js/pagination.js"></script>



<script>
    var js_data = '<?php echo json_encode($weekend_day); ?>';
    weekend_days = JSON.parse(js_data);
</script>
<script type="text/javascript">
    document.getElementById('rok').value = "<?php if ($_POST['rok']) {
        echo $_POST['rok'];
    } else {
        echo "2017";
    }?>";
    document.getElementById('mesiac').value = "<?php if ($_POST['rok']) {
        echo $_POST['mesiac'];
    } else {
        echo "1";
    }?>";
</script>

<script>
    $('#table1').tablePaginate({navigateType: 'navigator'});
</script>

</body>
</html>

