<?php
require('../../../database/config.php');
session_start();

class day{
    public $date;
    public $type;
};

$mesiac=1;
$rok=2017;
$data = array();

if(isset($_POST['rok']) && ($_POST['rok'] != null)&& isset($_POST['mesiac']) && ($_POST['mesiac']!= null)) {
    $rok= $_POST['rok'];
    $mesiac= $_POST['mesiac'];
}

$numberOfDays = cal_days_in_month(CAL_GREGORIAN, $mesiac, $rok);

// Create connection
$conn = new mysqli("localhost", "user", "0000", "sem2017");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
mysqli_set_charset($conn, "utf8");

$sql = "SELECT * FROM users ORDER BY lastname";
$result = $conn->query($sql);

$weekend_day = array();

echo '<table class="table table-responsive" id="table1">';
echo '<colgroup>';
echo '<col span="3" style="background-color:#888888">';
for ($i=1; $i <= $numberOfDays;$i++){
    $time = $rok . "-" . $mesiac . "-" . $i;
    $s = date("D", strtotime($time));
    if($s=="Sun" || $s=="Sat"){
        echo '<col span="1" class="weekend">';
        array_push($weekend_day, $i);
    }
    else{
        echo '<col span="1" class="week">';
    }
}

echo '</colgroup>';

echo '<thead class="headTable"><tr><th>ID</th><th>Lastname</th><th>Firstname</th>';
for ($i=1; $i <= $numberOfDays;$i++){
    $time = $rok . "-" . $mesiac . "-" . $i;
    $s = date("D", strtotime($time));
    echo "<th data-day=" . date("N", strtotime($time)) .">" . $i . "<br>" . $s . "</th>";
}
echo "</tr></thead>";
echo "<tbody>";

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        //echo '<br>';
        $sql2 = "SELECT z.den, n.typ FROM (SELECT doch.den, doch.typ_id, zam.lastname from dochadzka as doch JOIN users as zam ON doch.user_id = zam.id where zam.id=" . $row["id"] . ") z JOIN  nepritomnost as n ON z.typ_id = n.id WHERE Month(z.den) = " . $mesiac . " AND Year(z.den) = " . $rok;
        $result2 = $conn->query($sql2);
        //echo "id: " . $row["id"] . " priezvisko " . $row["priezvisko"] . " meno " . $row["meno"] . "<br>";
        $person_days = array();
        if ($result2->num_rows > 0){
            while($row2 = $result2->fetch_assoc()) {
                //echo "den " . $row2["den"] . "typ " . $row2["typ"] . "<br>";
                $den = new day();
                $den->date = date("d", strtotime($row2["den"]));
                $den->type = $row2["typ"];
                array_push($person_days, $den);
            }

            echo "<tr><td>" . $row["id"] . "</td><td>";
            if ((in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) || (in_array("user", $_SESSION['role']) && $row["lastname"]==$_SESSION['lastname'] && $row["firstname"]==$_SESSION['firstname'])){
                echo "<a href='#' class='openModalUniversal employee' 
                                    data-toggle='modal'
                                    data-target='.universal'
                                    data-name='" . $row["lastname"] . " " . $row["firstname"]."' data-personid='" . $row["id"] ."'>";

            }
            echo $row["lastname"];
            if ((in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) || (in_array("user", $_SESSION['role']) && $row["lastname"]==$_SESSION['lastname'] && $row["firstname"]==$_SESSION['firstname'])){
                echo "</a>";
            }

            echo "</td></td><td>" . $row["firstname"] . "</td>";
            for ($i=1; $i <= $numberOfDays; $i++){
                $e = getE($person_days, $i);
                if($e != null){
                    switch ($e->type){
                        case 'D':
                            echo "<td class='dov myClass'>" . $e->type . "</td>";
                            break;

                        case 'PN':
                            echo "<td class='pn myClass'>" . $e->type . "</td>";
                            break;

                        case 'PD':
                            echo "<td class='pd myClass'>" . $e->type . "</td>";
                            break;

                        case 'SC':
                            echo "<td class='sc myClass'>" . $e->type . "</td>";
                            break;

                        case 'OČR':
                            echo "<td class='ocr myClass'>" . $e->type . "</td>";
                            break;
                        default:
                            echo "<td class='myClass'></td>";
                    }
                }
                else{
                    echo "<td class='myClass'></td>";
                }
            }
            echo "</tr>";
        }
        else{
            echo "<tr><td>" . $row["id"] . "</td><td>";
            if ((in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) || (in_array("user", $_SESSION['role']) && $row["lastname"]==$_SESSION['lastname'] && $row["firstname"]==$_SESSION['firstname'])) {
                echo "<a href='#' class='openModalUniversal employee' 
                                    data-toggle='modal'
                                    data-target='.universal'
                                    data-name='" . $row["lastname"] . " " . $row["firstname"] . "' data-personid='" . $row["id"] . "'>";
                }
                echo $row["lastname"];
            if ((in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) || (in_array("user", $_SESSION['role']) && $row["lastname"]==$_SESSION['lastname'] && $row["firstname"]==$_SESSION['firstname'])) {
                "</a>";
            }
            echo "</td></td><td>" . $row["firstname"] . "</td>";
            for ($i=1; $i <= $numberOfDays; $i++){
                echo "<td class='myClass'></td>";
            }
        }
    }
    echo "</tbody>";
}


$conn->close();
echo '</table>';


function getE($days, $i){
    foreach ($days as $d){
        if($d->date == $i){
            $e = $d;
            return $e;
        }
    }
    return null;
}

?>