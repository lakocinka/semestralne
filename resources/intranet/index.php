<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("location: login/login.php");
}
require "functions/roles.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Intranet</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="css/ltestyle.css">


    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <a href="index.php" class="logo">
            <span class="logo-mini"><b>WEB</b></span>
            <span class="logo-lg"><b>WEB</b>tech</span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo getUserImagePath(); ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs">
                                <?php
                                userPanel();
                                ?>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo getUserImagePath(); ?>" class="img-circle" alt="User Image">

                                <p>
                                    <?php
                                    userMenu();
                                    ?>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="profile/profile.php" class="btn btn-default btn-flat">Profil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="login/logout.php" class="btn btn-default btn-flat">Odhlásiť</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">

        <section class="sidebar">

            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo getUserImagePath(); ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php
                        userPanel();
                        ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <ul class="sidebar-menu">
                <li class="header">Main navigation</li>
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-home"></i> <span>Domov</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="pedagogika/pedagogika.php">
                        <i class="fa fa-graduation-cap"></i> <span>Pedagogika</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="doktorandi/doktorandi.php">
                        <i class="fa fa-group"></i> <span>Doktorandi</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="publikacie/publikacie.php">
                        <i class="fa fa-book"></i> <span>Publikácie</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="sluzobne_cesty/sluzobne_cesty.php">
                        <i class="fa fa-car"></i> <span>Sľužobné cesty</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="nakupy/nakupy.php">
                        <i class="fa fa-shopping-cart"></i> <span>Nákupy</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="dochadzka/dochadzka.php">
                        <i class="fa fa-table"></i> <span>Dochádzka</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="rozdelenie_uloh/rozdelenie_uloh.php">
                        <i class="fa fa-tasks"></i> <span>Rozdelenie úloh</span>
                    </a>
                </li>
                <?php
                if (in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                    echo ' <li class="treeview"><a href="newsletter/newsletter.php"><i class="fa fa-envelope-o"></i> <span>Newsletter</span></a></li>';
                }
                if (in_array("reporter", $_SESSION['role']) || in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                    echo '<li class="treeview">
                    <a href="#"><i class="fa fa-pencil-square-o"></i> <span>Úpravy</span></a>
                    <ul class="treeview-menu menu-open" style="display: none;">';
                    if (in_array("hr", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="upravy/pouzivatelia.php"><i class="fa fa-circle-o"></i>Používatelia</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="upravy/aktuality.php"><i class="fa fa-circle-o"></i>Aktuality</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="upravy/Projekty.php"><i class="fa fa-circle-o"></i>Projekty</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="upravy/fotky.php"><i class="fa fa-circle-o"></i>Fotky</a></li>';
                    }
                    if (in_array("reporter", $_SESSION['role']) || in_array("admin", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="upravy/videa.php"><i class="fa fa-circle-o"></i>Videá</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role']) || in_array("reporter", $_SESSION['role']) || in_array("editor", $_SESSION['role'])) {
                        echo '<li><a href="upravy/media.php"><i class="fa fa-circle-o"></i>Média</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="upravy/oNas.php"><i class="fa fa-circle-o"></i>O nás</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="upravy/vyskumne_oblasti.php"><i class="fa fa-circle-o"></i>Výskumné oblasti</a></li>';
                    }
                    if (in_array("admin", $_SESSION['role'])) {
                        echo '<li><a href="upravy/studium.php"><i class="fa fa-circle-o"></i>Štúdium</a></li>';
                    }
                    echo '</ul></li>';
                }
                ?>


            </ul>
        </section>
    </aside>


    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Intranet
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-file"></i> Intranet</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?php
                                require "functions/forIndex.php";
                                getUsers();
                                ?></h3>
                            <p>Počet používateľov</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-stalker"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?php
                                getAdmin();
                                ?></h3>

                            <p>Počer adminov</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-social-freebsd-devil"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php
                                getAktuality();
                                ?></h3>

                            <p>Počet aktualít</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-fireball"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><?php
                                getActiveUsers();
                                ?></h3>

                            <p>Počet aktívnych používateľov</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-email"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-solid bg-green-gradient">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-calendar"></i>

                            <h3 class="box-title">Kalendár</h3>
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <!-- button with a dropdown -->
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success btn-sm dropdown-toggle"
                                            data-toggle="dropdown">
                                        <i class="fa fa-bars"></i></button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">Pridať udalosť</a></li>
                                        <li><a href="#">Zrušiť udalosti</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Prezrieť kalendár</a></li>
                                    </ul>
                                </div>
                                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i
                                            class="fa fa-times"></i>
                                </button>
                            </div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <!--The calendar -->
                            <div id="calendar" style="width: 100%">
                                <div class="datepicker datepicker-inline">
                                    <div class="datepicker-days" style="display: block;">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th class="prev" style="visibility: visible;">«</th>
                                                <th colspan="5" class="datepicker-switch">Máj 2017</th>
                                                <th class="next" style="visibility: visible;">»</th>
                                            </tr>
                                            <tr>
                                                <th class="dow">Ned</th>
                                                <th class="dow">Pon</th>
                                                <th class="dow">Uto</th>
                                                <th class="dow">Str</th>
                                                <th class="dow">Stv</th>
                                                <th class="dow">Pia</th>
                                                <th class="dow">Sob</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="old day">30</td>
                                                <td class="day">1</td>
                                                <td class="day">2</td>
                                                <td class="day">3</td>
                                                <td class="day">4</td>
                                                <td class="day">5</td>
                                                <td class="day">6</td>
                                            </tr>
                                            <tr>
                                                <td class="day">7</td>
                                                <td class="day">8</td>
                                                <td class="day">9</td>
                                                <td class="day">10</td>
                                                <td class="day">11</td>
                                                <td class="day">12</td>
                                                <td class="day">13</td>
                                            </tr>
                                            <tr>
                                                <td class="day">14</td>
                                                <td class="day">15</td>
                                                <td class="day">16</td>
                                                <td class="day">17</td>
                                                <td class="day">18</td>
                                                <td class="day">19</td>
                                                <td class="day">20</td>
                                            </tr>
                                            <tr>
                                                <td class="day">21</td>
                                                <td class="day">22</td>
                                                <td class="day">23</td>
                                                <td class="day">24</td>
                                                <td class="day">25</td>
                                                <td class="day">26</td>
                                                <td class="day">27</td>
                                            </tr>
                                            <tr>
                                                <td class="day">28</td>
                                                <td class="day">29</td>
                                                <td class="day">30</td>
                                                <td class="day">31</td>
                                                <td class="new day">1</td>
                                                <td class="new day">2</td>
                                                <td class="new day">3</td>
                                            </tr>
                                            <tr>
                                                <td class="new day">4</td>
                                                <td class="new day">5</td>
                                                <td class="new day">6</td>
                                                <td class="new day">7</td>
                                                <td class="new day">8</td>
                                                <td class="new day">9</td>
                                                <td class="new day">10</td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="7" class="today" style="display: none;">Today</th>
                                            </tr>
                                            <tr>
                                                <th colspan="7" class="clear" style="display: none;">Clear</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="datepicker-months" style="display: none;">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th class="prev" style="visibility: visible;">«</th>
                                                <th colspan="5" class="datepicker-switch">2017</th>
                                                <th class="next" style="visibility: visible;">»</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="7"><span class="month">Jan</span><span
                                                            class="month">Feb</span><span
                                                            class="month">Mar</span><span class="month">Apr</span><span
                                                            class="month">May</span><span class="month">Jun</span><span
                                                            class="month">Jul</span><span class="month">Aug</span><span
                                                            class="month">Sep</span><span class="month">Oct</span><span
                                                            class="month">Nov</span><span class="month">Dec</span></td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="7" class="today" style="display: none;">Today</th>
                                            </tr>
                                            <tr>
                                                <th colspan="7" class="clear" style="display: none;">Clear</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="datepicker-years" style="display: none;">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th class="prev" style="visibility: visible;">«</th>
                                                <th colspan="5" class="datepicker-switch">2010-2019</th>
                                                <th class="next" style="visibility: visible;">»</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="7"><span class="year old">2009</span><span
                                                            class="year">2010</span><span class="year">2011</span><span
                                                            class="year">2012</span><span class="year">2013</span><span
                                                            class="year">2014</span><span class="year">2015</span><span
                                                            class="year">2016</span><span class="year">2017</span><span
                                                            class="year">2018</span><span class="year">2019</span><span
                                                            class="year new">2020</span></td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="7" class="today" style="display: none;">Today</th>
                                            </tr>
                                            <tr>
                                                <th colspan="7" class="clear" style="display: none;">Clear</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-black">
                            <div class="row">
                                <div class="col-sm-6">
                                    <!-- Progress bars -->
                                    <div class="clearfix">
                                        <span class="pull-left">Úloha #1</span>
                                        <small class="pull-right">90%</small>
                                    </div>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-green" style="width: 90%;"></div>
                                    </div>

                                    <div class="clearfix">
                                        <span class="pull-left">Úloha #2</span>
                                        <small class="pull-right">70%</small>
                                    </div>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-green" style="width: 70%;"></div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-6">
                                    <div class="clearfix">
                                        <span class="pull-left">Úloha #3</span>
                                        <small class="pull-right">60%</small>
                                    </div>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-green" style="width: 60%;"></div>
                                    </div>

                                    <div class="clearfix">
                                        <span class="pull-left">Úloha #4</span>
                                        <small class="pull-right">40%</small>
                                    </div>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-green" style="width: 40%;"></div>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pripomienky</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                            class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                    <div class="product-img">
                                        <img src="./image/pripomienka.jpg" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="#" class="product-title">Pridanie aktualít
                                            <span class="label label-danger pull-right">1 týždeň</span></a>
                                        <span class="product-description">
                          pridať aktualitu o ústave
                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                                <li class="item">
                                    <div class="product-img">
                                        <img src="./image/pripomienka.jpg" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="#" class="product-title">Skúška druháci
                                            <span class="label label-info pull-right">2 týždne</span></a>
                                        <span class="product-description">
                          pripraviť skúšku z webtech
                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                                <li class="item">
                                    <div class="product-img">
                                        <img src="./image/pripomienka.jpg" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="#" class="product-title">Upratať izbu <span
                                                    class="label label-danger pull-right">hneď</span></a>
                                        <span class="product-description">
                          umyť podlahy
                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                                <li class="item">
                                    <div class="product-img">
                                        <img src="./image/pripomienka.jpg" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="#" class="product-title">Kúpiť darček pre mamu
                                            <span class="label label-success pull-right">2 týždne</span></a>
                                        <span class="product-description">
                          vybrať darček ale nie zase drogériu
                        </span>
                                    </div>
                                </li>
                                <!-- /.item -->
                            </ul>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-center">
                            <a href="#" class="uppercase">Pozrieť všetky pripomienky</a>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-solid bg-teal-gradient">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-th"></i>

                            <h3 class="box-title">Sales Graph</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i
                                            class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body border-radius-none">
                            <div class="chart" id="line-chart"
                                 style="height: 250px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <svg height="250" version="1.1" width="423" xmlns="http://www.w3.org/2000/svg"
                                     style="overflow: hidden; position: relative; left: -0.484375px; top: -0.59375px;">
                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël
                                        2.1.0
                                    </desc>
                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                    <text x="43.8125" y="211" text-anchor="end" font="10px &quot;Arial&quot;"
                                          stroke="none"
                                          fill="#ffffff"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 10px; line-height: normal; font-family: &quot;Open Sans&quot;;"
                                          font-size="10px" font-family="Open Sans" font-weight="normal">
                                        <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan>
                                    </text>
                                    <path fill="none" stroke="#efefef" d="M56.3125,211H398" stroke-width="0.4"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <text x="43.8125" y="164.5" text-anchor="end" font="10px &quot;Arial&quot;"
                                          stroke="none" fill="#ffffff"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 10px; line-height: normal; font-family: &quot;Open Sans&quot;;"
                                          font-size="10px" font-family="Open Sans" font-weight="normal">
                                        <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">5,000
                                        </tspan>
                                    </text>
                                    <path fill="none" stroke="#efefef" d="M56.3125,164.5H398" stroke-width="0.4"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <text x="43.8125" y="118.00000000000001" text-anchor="end"
                                          font="10px &quot;Arial&quot;"
                                          stroke="none" fill="#ffffff"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 10px; line-height: normal; font-family: &quot;Open Sans&quot;;"
                                          font-size="10px" font-family="Open Sans" font-weight="normal">
                                        <tspan dy="3.500000000000014"
                                               style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">10,000
                                        </tspan>
                                    </text>
                                    <path fill="none" stroke="#efefef" d="M56.3125,118.00000000000001H398"
                                          stroke-width="0.4"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <text x="43.8125" y="71.5" text-anchor="end" font="10px &quot;Arial&quot;"
                                          stroke="none"
                                          fill="#ffffff"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 10px; line-height: normal; font-family: &quot;Open Sans&quot;;"
                                          font-size="10px" font-family="Open Sans" font-weight="normal">
                                        <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">15,000
                                        </tspan>
                                    </text>
                                    <path fill="none" stroke="#efefef" d="M56.3125,71.5H398" stroke-width="0.4"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <text x="43.8125" y="25.00000000000003" text-anchor="end"
                                          font="10px &quot;Arial&quot;"
                                          stroke="none" fill="#ffffff"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 10px; line-height: normal; font-family: &quot;Open Sans&quot;;"
                                          font-size="10px" font-family="Open Sans" font-weight="normal">
                                        <tspan dy="3.5000000000000284"
                                               style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">20,000
                                        </tspan>
                                    </text>
                                    <path fill="none" stroke="#efefef" d="M56.3125,25.00000000000003H398"
                                          stroke-width="0.4"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <text x="335.3229804820009" y="223.5" text-anchor="middle"
                                          font="10px &quot;Arial&quot;"
                                          stroke="none" fill="#ffffff"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 10px; line-height: normal; font-family: &quot;Open Sans&quot;;"
                                          font-size="10px" font-family="Open Sans" font-weight="normal"
                                          transform="matrix(1,0,0,1,0,7)">
                                        <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013
                                        </tspan>
                                    </text>
                                    <text x="183.36191521948257" y="223.5" text-anchor="middle"
                                          font="10px &quot;Arial&quot;" stroke="none" fill="#ffffff"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 10px; line-height: normal; font-family: &quot;Open Sans&quot;;"
                                          font-size="10px" font-family="Open Sans" font-weight="normal"
                                          transform="matrix(1,0,0,1,0,7)">
                                        <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012
                                        </tspan>
                                    </text>
                                    <path fill="none" stroke="#efefef"
                                          d="M56.3125,186.2062C65.85764091058681,185.9458,84.94792273176041,187.77498361268403,94.49306364234722,185.1646C104.04252949217761,182.55303361268403,123.14146119183839,166.47413193717279,132.69092704166877,165.3184C142.14091928889675,164.17470693717277,161.04090378335275,178.17383001831084,170.49089603058073,175.9669C179.93656333856512,173.76098001831085,198.82789795453397,149.8676498291961,208.27356526251836,147.667C217.81870617310517,145.4431748291961,236.90898799427876,155.95614898074746,246.45412890486557,158.269C256.003594754696,160.58289898074747,275.10252645435673,177.16221780104715,284.65199230418716,166.174C294.10198455141517,155.30024280104715,313.0019690458711,77.68073023590334,322.4519612930991,70.8211C331.7938300592375,64.03995523590334,350.47756759151434,103.97382603618047,359.81943635765276,111.61090000000002C369.3645772682396,119.41415103618047,388.45485908941316,127.33952500000001,398,132.5824"
                                          stroke-width="2"
                                          style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <circle cx="56.3125" cy="186.2062" r="4" fill="#efefef" stroke="#efefef"
                                            stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="94.49306364234722" cy="185.1646" r="4" fill="#efefef" stroke="#efefef"
                                            stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="132.69092704166877" cy="165.3184" r="4" fill="#efefef" stroke="#efefef"
                                            stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="170.49089603058073" cy="175.9669" r="4" fill="#efefef" stroke="#efefef"
                                            stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="208.27356526251836" cy="147.667" r="4" fill="#efefef" stroke="#efefef"
                                            stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="246.45412890486557" cy="158.269" r="4" fill="#efefef" stroke="#efefef"
                                            stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="284.65199230418716" cy="166.174" r="4" fill="#efefef" stroke="#efefef"
                                            stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="322.4519612930991" cy="70.8211" r="4" fill="#efefef" stroke="#efefef"
                                            stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="359.81943635765276" cy="111.61090000000002" r="4" fill="#efefef"
                                            stroke="#efefef" stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="398" cy="132.5824" r="4" fill="#efefef" stroke="#efefef"
                                            stroke-width="1"
                                            style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                </svg>
                                <div class="morris-hover morris-default-style"
                                     style="left: 86.1909px; top: 93px; display: none;">
                                    <div class="morris-hover-row-label">2011 Q3</div>
                                    <div class="morris-hover-point" style="color: #efefef">
                                        Item 1:
                                        4,912
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer no-border">
                            <div class="row">
                                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                    <div style="display:inline;width:60px;height:60px;">
                                        <canvas width="60" height="60"></canvas>
                                        <input type="text" class="knob" data-readonly="true" value="20" data-width="60"
                                               data-height="60" data-fgcolor="#39CCCC" readonly="readonly"
                                               style="width: 34px; height: 20px; position: absolute; vertical-align: middle; margin-top: 20px; margin-left: -47px; border: 0px; background: none; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Arial; text-align: center; color: rgb(57, 204, 204); padding: 0px; -webkit-appearance: none;">
                                    </div>

                                    <div class="knob-label">Mail-Orders</div>
                                </div>
                                <!-- ./col -->
                                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                    <div style="display:inline;width:60px;height:60px;">
                                        <canvas width="60" height="60"></canvas>
                                        <input type="text" class="knob" data-readonly="true" value="50" data-width="60"
                                               data-height="60" data-fgcolor="#39CCCC" readonly="readonly"
                                               style="width: 34px; height: 20px; position: absolute; vertical-align: middle; margin-top: 20px; margin-left: -47px; border: 0px; background: none; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Arial; text-align: center; color: rgb(57, 204, 204); padding: 0px; -webkit-appearance: none;">
                                    </div>

                                    <div class="knob-label">Online</div>
                                </div>
                                <!-- ./col -->
                                <div class="col-xs-4 text-center">
                                    <div style="display:inline;width:60px;height:60px;">
                                        <canvas width="60" height="60"></canvas>
                                        <input type="text" class="knob" data-readonly="true" value="30" data-width="60"
                                               data-height="60" data-fgcolor="#39CCCC" readonly="readonly"
                                               style="width: 34px; height: 20px; position: absolute; vertical-align: middle; margin-top: 20px; margin-left: -47px; border: 0px; background: none; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Arial; text-align: center; color: rgb(57, 204, 204); padding: 0px; -webkit-appearance: none;">
                                    </div>

                                    <div class="knob-label">In-Store</div>
                                </div>
                                <!-- ./col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-success">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-comments-o"></i>

                            <h3 class="box-title">Chat</h3>

                            <div class="box-tools pull-right" data-toggle="tooltip" title=""
                                 data-original-title="Status">
                                <div class="btn-group" data-toggle="btn-toggle">
                                    <button type="button" class="btn btn-default btn-sm active"><i
                                                class="fa fa-square text-green"></i>
                                    </button>
                                    <button type="button" class="btn btn-default btn-sm"><i
                                                class="fa fa-square text-red"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="slimScrollDiv"
                             style="position: relative; overflow: hidden; width: auto; height: 250px;">
                            <div class="box-body chat" id="chat-box"
                                 style="overflow: hidden; width: auto; height: 250px;">
                                <!-- chat item -->
                                <div class="item">
                                    <img src="./image/user.jpg" alt="user image" class="online">

                                    <p class="message">
                                        <a href="#" class="name">
                                            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 20:18
                                            </small>
                                            Majkky
                                        </a>
                                        Ideme dnes na pivo do grilu?
                                    </p>

                                </div>
                                <!-- /.item -->
                                <!-- chat item -->
                                <div class="item">
                                    <img src="./image/user.jpg" alt="user image" class="offline">

                                    <p class="message">
                                        <a href="#" class="name">
                                            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 20:20
                                            </small>
                                            Martinez
                                        </a>
                                        Jasne webové hotové tak o 5 minút dolu
                                    </p>
                                </div>
                                <!-- /.item -->
                                <!-- chat item -->
                                <div class="item">
                                    <img src="./image/user.jpg" alt="user image" class="offline">

                                    <p class="message">
                                        <a href="#" class="name">
                                            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 20:30
                                            </small>
                                            Gott
                                        </a>
                                        Ja prídem za vami do grilu

                                    </p>
                                </div>
                                <!-- /.item -->
                            </div>
                            <div class="slimScrollBar"
                                 style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 189.97px;"></div>
                            <div class="slimScrollRail"
                                 style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                        </div>
                        <!-- /.chat -->
                        <div class="box-footer">
                            <div class="input-group">
                                <input class="form-control" placeholder="Poslať správu...">

                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.01
        </div>

    </footer>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- AdminLTE App -->
<script src="js/lte.js"></script>


</body>
</html>

