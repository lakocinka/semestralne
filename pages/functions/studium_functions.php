<?php
function zobrazStudiumInfo($divId, $lang)
{
    require "../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, "utf8");

    if ($lang == 'sk') {
        $sql = "SELECT text_SK FROM study WHERE typ = '" . $divId . "'";
    } elseif ($lang == 'en') {
        $sql = "SELECT text_EN FROM study WHERE typ = '" . $divId . "'";
    }
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        if ($lang == 'sk') {
            echo "<div>" . $row['text_SK'] . "</div>";
        } elseif ($lang == 'en') {
            echo "<div>" . $row['text_EN'] . "</div>";
        }
    } else {
        echo "Ziadny vysledok";
    }
}

if (isset($_POST['b'])) {

    $string = "http://is.stuba.sk/pracoviste/prehled_temat.pl?lang=sk;pracoviste=642";
    //$res=$string.$result;
    //echo $id_prac;
    //echo $res;
    $ch = curl_init($string);

    // zostavenie dat pre POST dopyt
    $data = array(
        'filtr_typtemata2' => 1,
        'pracoviste' => 642,
        'lang' => 'sk',
        'omezit_temata2' => 'Obmedziť'
    );

    // nastavenie curl-u pre pouzitie POST dopytu
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    // nastavenie curl-u pre ulozenie dat z odpovede do navratovej hodnoty z volania curl_exec
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    // vykonanie dopytu
    $result = curl_exec($ch);
    curl_close($ch);

    // parsovanie odpovede pre ziskanie pozadovanych dat
    $doc = new DOMDocument();
    libxml_use_internal_errors(true);
    $doc->loadHTML($result);
    $xPath = new DOMXPath($doc);
    $tableRows = $xPath->query('//html/body/div/div/div/form/table[last()]/tbody/tr');
    createTable($tableRows);
}

function createTable($tableRows)
{
    echo "<div class='col-md-12'>";
    echo '<table class="table table-bordered table-responsive" id="tabulka25"> <thead><tr><th class="zarovnastred">Typ</th><th class="zarovnastred">Nazov</th><th class="zarovnastred">Veduci</th><th class="zarovnastred">Odbor</th></tr></thead><tbody>';

    foreach ($tableRows as $row) {
        $tmp = 0;
        $result2="";
        foreach ($row->childNodes as $child) {
            if ($tmp == 1) {
                $result2=$result2."<tr><td>" . $child->nodeValue . "</td>";
            } elseif ($tmp == 2) {
               $result2=$result2."<td>" . $child->nodeValue . "</td>";
            } elseif ($tmp == 3) {
                $result2=$result2."<td>" . $child->nodeValue . "</td>";
            } elseif ($tmp == 5) {
                $result2=$result2."<td>" . $child->nodeValue . "</td></tr></div>";
            } elseif ($tmp == 8) {
                if (substr($child->nodeValue,0,1)==0){echo $result2;}
            } else {
            }

            $tmp++;
        }

    }

    echo "</tbody></table>";
    echo "</div>";
}

if (isset($_POST['d'])) {

    $string = "http://is.stuba.sk/pracoviste/prehled_temat.pl?lang=sk;pracoviste=642";
    //$res=$string.$result;
    //echo $id_prac;
    //echo $res;
    $ch = curl_init($string);

    // zostavenie dat pre POST dopyt
    $data = array(
        'filtr_typtemata2' => 2,
        'pracoviste' => 642,
        'lang' => 'sk',
        'omezit_temata2' => 'Obmedziť'
    );

    // nastavenie curl-u pre pouzitie POST dopytu
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    // nastavenie curl-u pre ulozenie dat z odpovede do navratovej hodnoty z volania curl_exec
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    // vykonanie dopytu
    $result = curl_exec($ch);
    curl_close($ch);

    // parsovanie odpovede pre ziskanie pozadovanych dat
    $doc = new DOMDocument();
    libxml_use_internal_errors(true);
    $doc->loadHTML($result);
    $xPath = new DOMXPath($doc);
    $tableRows = $xPath->query('//html/body/div/div/div/form/table[last()]/tbody/tr');
    createTable2($tableRows);
}

function createTable2($tableRows)
{
    echo "<div class='col-md-12'>";
    echo '<table class="table table-bordered table-responsive" id="tabulka26"> <thead><tr><th class="zarovnastred">Typ</th><th class="zarovnastred">Nazov</th><th class="zarovnastred">Veduci</th><th class="zarovnastred">Odbor</th></tr></thead><tbody>';

    foreach ($tableRows as $row) {
        $tmp = 0;

        $result="";
        foreach ($row->childNodes as $child) {
            if ($tmp == 1) {
                $result=$result."<tr><td>" . $child->nodeValue . "</td>";
                //echo $result;
            } elseif ($tmp == 2) {
                $result=$result."<td>" . $child->nodeValue . "</td>";
                //echo $result;
            } elseif ($tmp == 3) {
                $result=$result."<td>" . $child->nodeValue . "</td>";
            } elseif ($tmp == 5) {
                $result= $result."<td>" . $child->nodeValue . "</td></tr>";

            }  elseif ($tmp == 8) {
                if (substr($child->nodeValue,0,1)==0)echo $result;
            } else {

            }

            //var_dump( $result) ;
            $tmp++;
        }

    }

    echo "</tbody></table>";
    echo "</div>";
}

