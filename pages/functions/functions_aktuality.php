<?php
/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 21.05.2017
 * Time: 18:30
 */

require "../database/config.php";



function generateAllBlog($pageNum,$lan){

    $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . ';charset=utf8';
    $user = SQL_USERNAME;
    $password = SQL_PASSWORD;

    try {
        $conn = new PDO($dsn, $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch
    (PDOException $e) {
        die('Connection failed: ' . $e->getMessage());
    }

    $dotaz = $conn->prepare("SELECT * FROM `Aktuality` WHERE `Aktuality`.`endDate` > CURDATE() ORDER BY `Aktuality`.`startDate` DESC");
    $dotaz->execute();
    $rows = $dotaz->fetchAll(PDO::FETCH_ASSOC);


   // var_dump($rows);


    if($pageNum==1){
        $star=1;
        $end=5;
    }else{


        $end=$pageNum*5;
        $star=$end-4;
    }



    $i=1;

    foreach ($rows as $val){


        if($i>=$star && $i<=$end) {
            if($lan){
                if($val["titleSK"]== null || $val["titleSK"]==""){
                    continue;
                }
            }else{
                if($val["titleEN"]== null || $val["titleEN"]==""){
                    continue;
                }

            }

            echo " <div class=\"hookArt row\">
            <div class=\"col-md12\">
                <article class=\"aktualita\">";

            if($lan) {
                echo '  <h3 class="clanokNad">' . $val["titleSK"] . '</h3>';
            }else{
                echo '  <h3 class="clanokNad">' . $val["titleEN"] . '</h3>';
            }
            echo '<div class="info"><span class="aktinfo">' . $val["kategoria"] . '</span><span class="aktinfo"> / ' . $val["startDate"] . '</span></div>';

            if($lan){
                echo '  <p>' . $val["textSK"] . '</p>';
            }else{
                echo '  <p>' . $val["textEN"] . '</p>';
            }
            echo "   </article></div></div>";
        }

        $i++;

    }

    $articleNumber=$i;
    $pageNumber=ceil(($i/5));


echo "<div class=\"page-nav row\">
    <div class=\"col-md-12\">
        <nav aria-label=\"Page navigation\">
            <ul class=\"pagination\">";

for ($p=0;$p<$pageNumber;$p++){

    $l=$p+1;
    echo "<li><a href='?page=".$l."'>".$l."</a></li>";
}
//                <li><a href=\"?page1\">1</a></li>
//                <li><a href=\"?page1\">2</a></li>
//                <li><a href=\"?page1\">3</a></li>
//                <li><a href=\"?page1\">4</a></li>
//                <li><a href=\"?page1\">5</a></li>

               echo "</ul>
    </div>
</div>";

}
function generateFilterBlog($category,$num,$pageNum,$lan)
{


    $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . ';charset=utf8';
    $user = SQL_USERNAME;
    $password = SQL_PASSWORD;

    try {
        $conn = new PDO($dsn, $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch
    (PDOException $e) {
        die('Connection failed: ' . $e->getMessage());
    }

    if($category==0){

        if($num==1){

            $dotaz = $conn->prepare("SELECT * FROM `Aktuality` WHERE `Aktuality`.`endDate` >= CURDATE() ORDER BY `Aktuality`.`startDate` DESC");
            $dotaz->execute();
        }else{



            $dotaz = $conn->prepare("SELECT * FROM `Aktuality` ORDER BY `Aktuality`.`startDate` DESC");
            $dotaz->execute();
        }

    }else {
        if ($category == 1) {
            $cat = "propagacia";
        }
        if ($category == 2) {
            $cat = "oznamy";
        }
        if ($category == 3) {
            $cat = "zoZivota";
        }

        if($num==1){

            $dotaz = $conn->prepare("SELECT * FROM `Aktuality` WHERE `Aktuality`.`endDate` >= CURDATE() AND `Aktuality`.`kategoria` = :category ORDER BY `Aktuality`.`startDate` DESC");



        }else{

            $dotaz = $conn->prepare("SELECT * FROM `Aktuality` WHERE `Aktuality`.`kategoria` = :category ORDER BY `Aktuality`.`startDate` DESC");

        }

        $dotaz->execute(array(":category"=>$cat));
    }


    //$dotaz = $conn->prepare("SELECT * FROM `Aktuality` WHERE `Aktuality`.`endDate` > CURDATE() ORDER BY `Aktuality`.`startDate` DESC");

    $rows = $dotaz->fetchAll(PDO::FETCH_ASSOC);

    //var_dump($rows);

    if($pageNum==1){
        $star=1;
        $end=5;
    }else{


        $end=$pageNum*5;
        $star=$end-4;
    }

   // echo "star".$star;
    //echo "end".$end;

    $i=1;
$j=1;


    foreach ($rows as $val){

        if($lan){
            if($val["titleSK"]== null || $val["titleSK"]==""){
                continue;
            }
        }else{
            if($val["titleEN"]== null || $val["titleEN"]==""){
                continue;
            }

        }

        if($i>=$star && $i<=$end) {
            echo " <div class=\"hookArt row\">
            <div class=\"col-md12\">
                <article class=\"aktualita\">";

            if($lan) {
                echo '  <h3 class="clanokNad">' . $val["titleSK"] . '</h3>';
            }else{
                echo '  <h3 class="clanokNad">' . $val["titleEN"] . '</h3>';
            }
            echo '<div class="info"><span class="aktinfo">' . $val["kategoria"] . '</span><span class="aktinfo"> / ' . $val["startDate"] . '</span></div>';

            if($lan) {
                echo '  <p>' . $val["textSK"] . '</p>';
            }else{
                echo '  <p>' . $val["textEN"] . '</p>';

            }

            echo "   </article></div></div>";

            $j++;
        }

        $i++;
    }

    //echo "j".$j;

    $pageNumber=ceil(($i/5));

    echo "<div class=\"page-nav row\">
    <div class=\"col-md-12\">
        <nav aria-label=\"Page navigation\">
            <ul class=\"pagination\">";

    for ($p=0;$p<$pageNumber;$p++){

        $l=$p+1;
        echo "<li><a href='?page=".$l."&category=".$category."&opt=".$num."'>".$l."</a></li>";
    }
//                <li><a href=\"?page1\">1</a></li>
//                <li><a href=\"?page1\">2</a></li>
//                <li><a href=\"?page1\">3</a></li>
//                <li><a href=\"?page1\">4</a></li>
//                <li><a href=\"?page1\">5</a></li>

    echo "</ul>
    </div>
</div>";

}
function createNewsUser($login,$pass,$lan){

    $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . '';
    $user = SQL_USERNAME;
    $password = SQL_PASSWORD;

    try {
        $conn = new PDO($dsn, $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch
    (PDOException $e) {
        die('Connection failed: ' . $e->getMessage());
    }

    if(isInDb($login)){

        if($lan) {
            echo "Email sa už nachádza v Databáze";
        }else{

            echo "Email is used";
        }
        return;
    }

    $hash = password_hash($pass, PASSWORD_DEFAULT);

    if($lan){

        $lang="sk";

    }else{
        $lang="en";
    }

    $dotaz = $conn->prepare("INSERT INTO `Newsletter`(`email`,`password`,`lang`) VALUES (?,?,?)");
    $dotaz->execute(array(
        $login,
        $hash,
        $lang

    ));

    if($lan) {
        echo "Ďakujeme za odoberanie nášho Newsletteru";
    }else{

        echo "Thanks for subs";

    }


}

function removeUser($login,$pass,$lan){


    $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . '';
    $user = SQL_USERNAME;
    $password = SQL_PASSWORD;

    try {
        $conn = new PDO($dsn, $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch
    (PDOException $e) {
        die('Connection failed: ' . $e->getMessage());
    }

    if(!isInDb($login)){

        if($lan) {
            echo "Ešte neodoberáte náš newsletter";
        }else{

            echo "You are not subscriber!";
        }
        return;
    }

    $dotaz = $conn->prepare("SELECT * FROM `Newsletter` WHERE `email`=:mail");
    $dotaz->execute(array(
        ":mail"=>$login
    ));
    $users = $dotaz->fetchAll(PDO::FETCH_ASSOC);

    $hash_pass = $users[0]["password"];

    if (password_verify($pass, $hash_pass)) {

        deleteFromDb($login,$lan);

    } else {
        if($lan) {
            echo "Zle heslo";
        }else{

            echo "Bad password";
        }
    }




}

function deleteFromDb($login,$lan){


    $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . '';
    $user = SQL_USERNAME;
    $password = SQL_PASSWORD;

    try {
        $conn = new PDO($dsn, $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch
    (PDOException $e) {
        die('Connection failed: ' . $e->getMessage());
    }


    $dotaz = $conn->prepare("DELETE FROM `Newsletter` WHERE `Newsletter`.`email`=:mail");
    $dotaz->execute(array(
        ":mail"=>$login

    ));

    if($lan) {
        echo "Email bol vymazany";
    }else{
        echo "Email was deleted";
    }



}

function isInDb($mail){

    $dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . '';
    $user = SQL_USERNAME;
    $password = SQL_PASSWORD;

    try {
        $conn = new PDO($dsn, $user, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch
    (PDOException $e) {
        die('Connection failed: ' . $e->getMessage());
    }


    $dotaz = $conn->prepare("SELECT `Newsletter`.`email` FROM `Newsletter` WHERE `email`=:mail");
    $dotaz->execute(array(
        ":mail"=>$mail

    ));
    $users = $dotaz->fetchAll(PDO::FETCH_ASSOC);

    if(sizeof($users)>0){

        return true;
    }else{
        return false;
    }
}