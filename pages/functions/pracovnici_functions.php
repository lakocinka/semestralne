<?php
/**
 * Created by PhpStorm.
 * User: patrik
 * Date: 23.05.2017
 * Time: 10:10
 */

if (isset($_POST['k'])) {
    $tmp = $_POST['k'];
    require "../../database/config.php";

    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);
    mysqli_set_charset($conn, "utf8");

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT title1,title2,firstname,lastname,photo,department,staffRole,phone,room,ldapLogin FROM users WHERE id=$tmp";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row

        $row = $result->fetch_assoc();
        if ($row['photo']) {
            $row['photo'] = "/webtech/resources/intranet/staffImage/" . $row['photo'];
        } else $row['photo'] = "/webtech/resources/intranet/image/user.jpg";
        print_r(json_encode($row));
    } else {
        echo "0 results";
    }
    $conn->close();

}

if (isset($_POST['l'])) {
    $login = $_POST['l'];
    //echo $login;
    $dn = 'ou=People, DC=stuba, DC=sk';
    $ldaprdn = "uid=" . $login . "," . $dn;

    $ldapconn = ldap_connect("ldap.stuba.sk");
    ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

    $results = ldap_search($ldapconn, $dn, "uid=$login", array("*"));
    $info = ldap_get_entries($ldapconn, $results);

    //echo $info[0][12];
    //print_r($info[0]['uisid'][0]);
    $aisid = $info[0]['uisid'][0];
    $string = "http://is.stuba.sk/lide/clovek.pl?lang=sk;zalozka=5;id=";

    $res = $string . $aisid;
    //echo $id_prac;
    //echo "Toto je php:".$res;
    $ch = curl_init($res);

    // zostavenie dat pre POST dopyt
    $data = array(
        'zalozka' => '5',
        'id' => $aisid,
        'rok' => '1',
        'lang' => 'sk'
    );

    // nastavenie curl-u pre pouzitie POST dopytu
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    // nastavenie curl-u pre ulozenie dat z odpovede do navratovej hodnoty z volania curl_exec
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    // vykonanie dopytu
    $result = curl_exec($ch);
    if ($result) {
        //print_r($result);
        curl_close($ch);

        // parsovanie odpovede pre ziskanie pozadovanych dat
        //print_r(json_decode($result, true));

        $doc = new DOMDocument();

        $doc->loadHTML($result);
        $xPath = new DOMXPath($doc);

        //print_r($xPath);
        $tableRows = $xPath->query('//html/body/div/div/div/table[last()]/tbody/tr');
        //var_dump($tableRows);
        createUserTable($tableRows);
    }
}

function createUserTable($tableRows)
{
    //echo "preslo";
    //echo '<table class="table" id="tabulka2"> <thead><tr><td>Publikacia</td><td>Druh</td><td>Rok</td></tr></thead><tbody>';
    echo "<div class='col-md-12 table-responsive'>";
    echo '<table class="table table-bordered table-responsive" id="tabulecka"> <thead><tr><th class="zarovnastred">Publikacia</th><th class="zarovnastred">Druh</th><th class="zarovnastred">Rok</th></tr></thead><tbody>';

    foreach ($tableRows as $row) {
        // var_dump($row);
        $tmp = 0;
        foreach ($row->childNodes as $child) {
            if ($tmp == 1) {
                echo "<tr><td>" . $child->nodeValue . "</td>";
            } elseif ($tmp == 2) {
                echo "<td>" . $child->nodeValue . "</td>";
            } elseif ($tmp == 3) {
                echo "<td>" . $child->nodeValue . "</td></tr></div>";
            } else {
            }

            $tmp++;
        }
    }
    echo "</tbody></table>";
    echo "</div>";
}
