<?php

function zobrazUvod(){
    require "../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn,"utf8");

    $sql = "SELECT text FROM aboutus WHERE typ = 'UVOD'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "<div>".$row['text']."</div>";
    } else {
        echo "Ziadny vysledok";
    }
}
function zobrazHistoria(){
    require "../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn,"utf8");

    $sql = "SELECT text FROM aboutus WHERE typ = 'HISTORIA'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "<div>".$row['text']."</div>";
    } else {
        echo "Ziadny vysledok";
    }
}
function zobrazVedenieUstavu(){
    require "../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn,"utf8");

    $sql = "SELECT text FROM aboutus WHERE typ = 'VEDENIE_USTAVU'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "<div>".$row['text']."</div>";
    } else {
        echo "Ziadny vysledok";
    }
}
function zobrazOddelenia(){
    require "../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn,"utf8");

    $sql = "SELECT text FROM aboutus WHERE typ = 'ODDELENIA'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "<div>".$row['text']."</div>";
    } else {
        echo "Ziadny vysledok";
    }
}
function zobrazOAMM(){
    require "../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn,"utf8");

    $sql = "SELECT text FROM aboutus WHERE typ = 'OAMM'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "<div>".$row['text']."</div>";
    } else {
        echo "Ziadny vysledok";
    }
}
function zobrazOIKR(){
    require "../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn,"utf8");

    $sql = "SELECT text FROM aboutus WHERE typ = 'OIKR'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "<div>".$row['text']."</div>";
    } else {
        echo "Ziadny vysledok";
    }
}
function zobrazOEMP(){
    require "../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn,"utf8");

    $sql = "SELECT text FROM aboutus WHERE typ = 'OEMP'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "<div>".$row['text']."</div>";
    } else {
        echo "Ziadny vysledok";
    }
}
function zobrazOEAP(){
    require "../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn,"utf8");

    $sql = "SELECT text FROM aboutus WHERE typ = 'OEAP'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo "<div>".$row['text']."</div>";
    } else {
        echo "Ziadny vysledok";
    }
}