<?php
function zobrazInfoLedKocka($lang){
    require "../../../database/config.php";
    $conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn,"utf8");

    if ($lang == 'sk'){
        $sql = "SELECT text_SK FROM research WHERE typ = 'KOCKA'";
    } elseif ($lang == 'en') {
        $sql = "SELECT text_EN FROM research WHERE typ = 'KOCKA'";
    }
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        if ($lang == 'sk'){
            echo "<div>".$row['text_SK']."</div>";
        } elseif ($lang == 'en'){
            echo "<div>".$row['text_EN']."</div>";
        }

    } else {
        echo "Ziadny vysledok";
    }
}