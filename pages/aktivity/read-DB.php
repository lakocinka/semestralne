<?php
require '../../database/config.php';

session_start();

if (!isset($_POST["table"])) {
    echo "0 results";
}

$conn = new mysqli($servernameDB, $usernameDB, $passwordDB, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

mysqli_set_charset($conn, "utf8");
$table = $_POST["table"];

$sql = "SELECT id, title_en as title, path, date FROM $table ORDER BY date DESC";

if (strcmp($_SESSION['lang'], 'sk') == 0) {
    $sql = "SELECT id, title_sk as title, path, date FROM $table ORDER BY date DESC";
}

if (strcmp($table, "media") == 0) {
    $sql = "SELECT id, title_en as title, path, type, date FROM $table ORDER BY date DESC";
    if (strcmp($_SESSION['lang'], 'sk') == 0) {
        $sql = "SELECT id, title_sk as title, path, type, date FROM $table ORDER BY date DESC";
    }
}

$result = $conn->query($sql);
$array_send = array();

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        array_push($array_send, $row);
    }
}

$conn->close();
session_abort();
print json_encode($array_send);
