<?php
session_start();
require "../../languages/lukas_functions.php";
$lang_file = setLang();
require "../../languages/$lang_file";
?>
<!DOCTYPE html>
<!--
Template Name: Kelaby
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<html>
<head>


    <title><?php echo $lang['MENU_ACTIVITIES_GALERY']; ?> | <?php echo $lang['PAGE_TITLE']; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="icon" href="../../assets/images/favicon.png">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link href="../../assets/css/layout.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="../../assets/css/activity.css">

</head>
<body id="top">
<div class="wrapper row0">
    <div id="topbar" class="hoc clear">
        <div class="fl_right">
            <ul>
                <li><a href="#"><i class="fa fa-lg fa-lock"></i></a></li>
                <li><a href="../../resources/intranet/index.php"><?php echo $lang['LOGIN_TITLE']; ?></a></li>
                <li class="lang_sk <?php if ($_SESSION['lang'] == 'sk') echo "fa-lg" ?>"><a
                        href="<?php echo $_SERVER['PHP_SELF'] ?>?lang=sk">SK</a></li>
                <li class="lang_en <?php if ($_SESSION['lang'] == 'en') echo "fa-lg" ?>"><a
                        href="<?php echo $_SERVER['PHP_SELF'] ?>?lang=en">EN</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="wrapper row1">
    <header id="header" class="hoc clear">
        <div id="logo" class="fl_left">
            <a class="col-md-4" href="../../index.php"><img src="../../assets/images/logo_uamt.png" alt="UAMT_img"></a>
            <h2 class="col-md-8 col-md-offset-2"><?php echo $lang['PAGE_TITLE'] ?></h2>

        </div>
    </header>
</div>
<div class="wrapper row2">
    <nav id="mainav" class="hoc clear">
        <ul class="clear">
            <li><a href="../../index.php"><?php echo $lang['MENU_HOME'] ?></a></li>
            <li><a href="../aktuality.php"><?php echo $lang['MENU_NEWS'] ?></a></li>
            <li><a class="drop" href="#"><?php echo $lang['MENU_RESEARCH'] ?></a>
                <ul>
                    <li><a href="../../pages/vyskum/projekty.php"><?php echo $lang['MENU_PROJECTS'] ?></a></li>
                    <li><a class="drop" href="#"><?php echo $lang['MENU_RESEARCH_TOPICS'] ?></a>
                        <ul>
                            <li>
                                <a href="../vyskum/vyskumne_oblasti/elektricka_motokara.php"><?php echo $lang['MENU_RESEARCH_TOPICS_ELECTRIC_KART'] ?></a>
                            </li>
                            <li>
                                <a href="../vyskum/vyskumne_oblasti/autonomne_vozidlo.php"><?php echo $lang['MENU_RESEARCH_TOPICS_AUTONOMOUS_VEHICLE'] ?></a>
                            </li>
                            <li>
                                <a href="../vyskum/vyskumne_oblasti/led_kocka.php"><?php echo $lang['MENU_RESEARCH_TOPICS_3D_LED_CUBE'] ?></a>
                            </li>
                            <li>
                                <a href="../vyskum/vyskumne_oblasti/biomechatronika.php"><?php echo $lang['MENU_RESEARCH_TOPICS_BIOMACHATRONICS'] ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="active"><a class="drop" href="#"><?php echo $lang['MENU_ACTIVITIES'] ?></a>
                <ul>
                    <li class="active"><a href="fotogaleria.php"><?php echo $lang['MENU_ACTIVITIES_GALERY'] ?></a></li>
                    <li><a href="videa.php"><?php echo $lang['MENU_ACTIVITIES_VIDEO'] ?></a></li>
                    <li><a href="media.php"><?php echo $lang['MENU_ACTIVITIES_MEDIA'] ?></a></li>
                    <li><a class="drop" href="#"><?php echo $lang['MENU_ACTIVITIES_TEMATIC_WEBSITES'] ?></a>
                        <ul>
                            <li>
                                <a href="http://www.e-mobilita.fei.stuba.sk"><?php echo $lang['MENU_ACTIVITIES_TEMATIC_WEBSITES_ELEKTROMOBILITY'] ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="../pracovnici.php"><?php echo $lang['MENU_STAFF'] ?></a></li>
            <li><a href="../studium.php"><?php echo $lang['MENU_STUDY'] ?></a></li>
            <li><a href="../kontakt.php"><?php echo $lang['MENU_CONTACT'] ?></a></li>
            <li><a href="../oNas.php"><?php echo $lang['MENU_ABOUT'] ?></a></li>
        </ul>
    </nav>
</div>
<div class="bgded overlay" style="background-image:url('../../assets/images/activities/event002/2015-09-25-6477.jpg'); background-position: center; background-size: cover;">
    <div id="breadcrumb" class="hoc clear">
        <h6 class="heading"><?php echo $lang['MENU_ACTIVITIES_GALERY'] ?></h6>
        <ul>
            <li><a href="../../">Home</a></li>
            <li><a href="#"><?php echo $lang['MENU_ACTIVITIES'] ?></a></li>
            <li><a href="fotogaleria.php"><?php echo $lang['MENU_ACTIVITIES_GALERY'] ?></a></li>
        </ul>
    </div>
</div>
<div class="wrapper row3">
    <!-- main body -->
    <div class="container gallery-container">
        <div class="tz-gallery">
            <!--JS take care of gallery filling-->
        </div>
        <!-- / main body -->
        <div class="row gallery_more" id="gallery_more_photos">
            <a class="btn btn-primary btn-rounded"><?php echo $lang['MENU_ACTIVITIES_SHOW_MORE'] ?></a>
        </div>
        <div class="clear"></div>
        </main>
    </div>
</div>

<div class="wrapper row0">
    <footer id="footer" class="hoc clear">
        <!-- ################################################################################################ -->
        <div class="center">
            <h6 class="heading" style="color: white;"><?php echo $lang['IMPORTANT_WEBSITES']; ?>:</h6>
            <a class="col-md-3 foothov" href="http://is.stuba.sk/">AIS STU</a>
            <a class="col-md-3 foothov" href="http://aladin.elf.stuba.sk/rozvrh/"><?php echo $lang['TIMETABLE']; ?></a>
            <a class="col-md-3 foothov" href="http://elearn.elf.stuba.sk/moodle/">Moodle FEI</a>
            <a class="col-md-3 foothov" href="http://www.sski.sk/webstranka/">SSKI</a>
            <a class="col-md-3 foothov" href="https://www.jedalen.stuba.sk/WebKredit/"><?php echo $lang['DINNING_ROOM']; ?></a>

            <a class="col-md-3 foothov" href="https://webmail.stuba.sk/">Webmail STU</a>
            <a class="col-md-3 foothov" href="https://kis.cvt.stuba.sk/i3/epcareports/epcarep.csp?ictx=stu&language=1"><?php echo $lang['PUBLICATIONS_EVIDENCE']; ?></a>
            <a class="col-md-3 foothov" href="http://okocasopis.sk/"><?php echo $lang['NEWSPAPER_OKO']; ?></a>

            <ul class="faico clear">
                <li><a class="faicon-facebook" href="https://www.facebook.com/UAMTFEISTU"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a class="faicon-google-plus" href="https://www.youtube.com/channel/UCo3WP2kC0AVpQMIiJR79TdA"><i
                                class="fa fa-youtube"></i></a></li>
            </ul>
        </div>


        <!-- ################################################################################################ -->
    </footer>
</div>
<div class="wrapper row5">
    <div id="copyright" class="hoc clear">
        <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">Domain Name</a></p>
        <p class="fl_right">Template by <a target="_blank" href="http://www.os-templates.com/"
                                           title="Free Website Templates">OS Templates</a></p>
    </div>
</div>

<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../../assets/js/jquery.min.js"></script>
<script src="../../assets/js/jquery.backtotop.js"></script>
<script src="../../assets/js/jquery.mobilemenu.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script src="../../assets/js/activity.js"></script>
<script src="../../assets/js/pata.js"></script>
</body>
</html>
