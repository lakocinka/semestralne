<?php
session_start();
require "../languages/lukas_functions.php";
$lang_file = setLang();
require "../languages/$lang_file";
require "functions/functions_aktuality.php";

?>
<!DOCTYPE html>
<!--
Template Name: Kelaby
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<html>
<head>


    <title><?php echo $lang['MENU_NEWS']; ?> | <?php echo $lang['PAGE_TITLE']; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://dl.dropboxusercontent.com/u/86701580/mypersonalcdn/renda/renda-icon-font.css">

    <link rel="icon" href="../assets/images/favicon.png">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link href="../assets/css/layout.css" rel="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" href="../assets/css/akt.css">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
    <div id="topbar" class="hoc clear">
        <!-- ################################################################################################ -->

        <div class="fl_right">
            <ul>
                <li><a href="#"><i class="fa fa-lg fa-lock"></i></a></li>
                <li><a href="../resources/intranet/index.php"><?php echo $lang['LOGIN_TITLE']; ?></a></li>
                <li class="lang_sk <?php if ($_SESSION['lang'] == 'sk') echo "fa-lg" ?>"><a
                            href="<?php echo $_SERVER['PHP_SELF'] ?>?lang=sk">SK</a></li>
                <li class="lang_en <?php if ($_SESSION['lang'] == 'en') echo "fa-lg" ?>"><a
                            href="<?php echo $_SERVER['PHP_SELF'] ?>?lang=en">EN</a></li>
            </ul>
        </div>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
    <header id="header" class="hoc clear">
        <!-- ################################################################################################ -->
        <div id="logo" class="fl_left">
            <a class="col-md-4" href="../index.php"><img src="../assets/images/logo_uamt.png" alt="UAMT_img"></a>
            <h2 class="col-md-8 col-md-offset-2"><?php echo $lang['PAGE_TITLE']; ?></h2>

        </div>
        <!-- ################################################################################################ -->
    </header>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row2">
    <nav id="mainav" class="hoc clear">
        <!-- ################################################################################################ -->
        <ul class="clear">
            <li><a href="../index.php"><?php echo $lang['MENU_HOME']; ?></a></li>
            <li class="active"><a href="aktuality.php"><?php echo $lang['MENU_NEWS']; ?></a></li>
            <li><a class="drop" href="#"><?php echo $lang['MENU_RESEARCH']; ?></a>
                <ul>
                    <li><a href="../pages/vyskum/projekty.php"><?php echo $lang['MENU_PROJECTS']; ?></a></li>
                    <li><a class="drop" href="#"><?php echo $lang['MENU_RESEARCH_TOPICS']; ?></a>
                        <ul>
                            <li><a href="vyskum/vyskumne_oblasti/elektricka_motokara.php"><?php echo $lang['MENU_RESEARCH_TOPICS_ELECTRIC_KART']; ?></a></li>
                            <li><a href="vyskum/vyskumne_oblasti/autonomne_vozidlo.php"><?php echo $lang['MENU_RESEARCH_TOPICS_AUTONOMOUS_VEHICLE']; ?></a></li>
                            <li><a href="vyskum/vyskumne_oblasti/led_kocka.php"><?php echo $lang['MENU_RESEARCH_TOPICS_3D_LED_CUBE']; ?></a></li>
                            <li><a href="vyskum/vyskumne_oblasti/biomechatronika.php"><?php echo $lang['MENU_RESEARCH_TOPICS_BIOMACHATRONICS']; ?></a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a class="drop" href="#"><?php echo $lang['MENU_ACTIVITIES'] ?></a>
                <ul>
                    <li><a href="aktivity/fotogaleria.php"><?php echo $lang['MENU_ACTIVITIES_GALERY']; ?></a></li>
                    <li><a href="aktivity/videa.php"><?php echo $lang['MENU_ACTIVITIES_VIDEO']; ?></a></li>
                    <li><a href="aktivity/media.php"><?php echo $lang['MENU_ACTIVITIES_MEDIA']; ?></a></li>
                    <li><a class="drop" href="#"><?php echo $lang['MENU_ACTIVITIES_TEMATIC_WEBSITES']; ?></a>
                        <ul>
                            <li><a href="http://www.e-mobilita.fei.stuba.sk"><?php echo $lang['MENU_ACTIVITIES_TEMATIC_WEBSITES_ELEKTROMOBILITY']; ?></a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="pracovnici.php"><?php echo $lang['MENU_STAFF']; ?></a></li>
            <li><a href="studium.php"><?php echo $lang['MENU_STUDY']; ?></a></li>
            <li><a href="kontakt.php"><?php echo $lang['MENU_CONTACT']; ?></a></li>
            <li><a href="oNas.php"><?php echo $lang['MENU_ABOUT']; ?></a></li>
        </ul>
        <!-- ################################################################################################ -->
    </nav>
</div>
<div class="bgded overlay" style="background-image:url('../assets/images/backgrounds/news_image.jpg'); background-size: cover; background-position: top;">
    <div id="breadcrumb" class="hoc clear">
        <!-- ################################################################################################ -->
        <h6 class="heading"><?php echo $lang['MENU_NEWS']; ?></h6>
        <!-- ################################################################################################ -->
        <ul>
            <li><a href="../index.php"><?php echo $lang['MENU_HOME']; ?></a></li>
            <li><a href="../pages/aktuality.php"><?php echo $lang['MENU_NEWS']; ?></a></li>
        </ul>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
    <main class="hoc container clear">
        <!-- main body -->
        <!-- ################################################################################################ -->
<?php
$lan=$_SESSION['lang'];
if($lan=="sk"){
    $lan=true;
}else{
    $lan=false;
}
?>
      <?php
      if($lan){
      ?>
        <div class="row header-input">
            <div class="header-filter col-md-4">
                <label for="event">Kategória:</label>
                <select class="form-control" id="event">
                    <option value="0" selected>Všetky</option>
                    <option value="1">Propagácia</option>
                    <option value="2">Oznamy</option>
                    <option value="3">Zo života ústavu</option>

                </select>
            </div>

            <div class="subFilter col-md-5">

                <label class="radio-inline"><input id="opt1" type="radio" name="optradio" checked>Len aktuálne články</label>
                <label class="radio-inline"><input id="opt2" type="radio" name="optradio">Zobraziť všetky články</label>

            </div>

            <div class="col-md-3">
                <button type="button" class="btnFilter btn btn-primary">SUBMIT</button>
            </div>

        </div>
        <?php }else{ ?>

        <div class="row header-input">
            <div class="header-filter col-md-4">
                <label for="event">Category:</label>
                <select class="form-control" id="event">
                    <option value="0" selected>All</option>
                    <option value="1">Propagation</option>
                    <option value="2">announcements</option>
                    <option value="3">From the life of the institute</option>

                </select>
            </div>

            <div class="subFilter col-md-5">

                <label class="radio-inline"><input id="opt1" type="radio" name="optradio" checked>Current articles</label>
                <label class="radio-inline"><input id="opt2" type="radio" name="optradio">All articles</label>

            </div>

            <div class="col-md-3">
                <button type="button" class="btnFilter btn btn-primary">SUBMIT</button>
            </div>

        </div>
            <?php }?>

        <?php


        if(isset($_GET["category"]) && isset($_GET["opt"])){

        if(isset($_GET["page"])) {
            generateFilterBlog($_GET["category"], $_GET["opt"],$_GET["page"],$lan);
        }else{

            generateFilterBlog($_GET["category"], $_GET["opt"],1,$lan);
        }

        }else{

            if(isset($_GET["page"])){

                generateAllBlog($_GET["page"],$lan);

            }else {
                generateAllBlog(1,$lan);
            }

        }
        ?>
<!--<div class="row">-->
<!--    <div class="col-md-12">-->
<!--        <nav aria-label="Page navigation">-->
<!--            <ul class="pagination">-->
<!--                <li>-->
<!--                    <a href="#" aria-label="Previous">-->
<!--                        <span aria-hidden="true">&laquo;</span>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li><a href="#">1</a></li>-->
<!--                <li><a href="#">2</a></li>-->
<!--                <li><a href="#">3</a></li>-->
<!--                <li><a href="#">4</a></li>-->
<!--                <li><a href="#">5</a></li>-->
<!--                <li>-->
<!--                    <a href="#" aria-label="Next">-->
<!--                        <span aria-hidden="true">&raquo;</span>-->
<!--                    </a>-->
<!--                </li>-->
<!--            </ul>-->
<!--    </div>-->
<!--</div>-->



        <!-- ################################################################################################ -->
        <!-- / main body -->
        <div class="clear"></div>
    </main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper coloured overlay bgded" style="background-image:url('../assets/images/backgrounds/aktuality_pata.jpg');">
    <div class="stmavene"></div>
    <div class="hoc container clear">
        <div id="testimonial">
            <!-- ################################################################################################ -->
            <h1>Newsletter!!!</h1>
            <p></p>

            <form class="newsletter" action="" method="post">


                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i></span>
                            <?php

                            if($lan){
                                echo "<input type=\"text\" class=\"form-control input-lg\" name=\"email\" id=\"email\"  placeholder=\"Vložte emialovu adresu\"/>";
                            }else{
                                echo '<input type="text" class="form-control input-lg" name="email" id="email"  placeholder="Enter your Email"/>';
                            }

                            ?>

                        </div>
                    </div>
                </div>

                <div class="inputPas row">
                    <div class="col-md-4"></div>
                    <div class=" col-md-4 ">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-text-background" aria-hidden="true"></i></span>
                            <?php

                            if($lan){

                                echo '<input type="password" class="form-control input-lg" name="pass" id="pass"  placeholder="Heslo pre unsubscribe"/>';

                            }else{

                                echo '<input type="password" class="form-control input-lg" name="pass" id="pass"  placeholder="Password for unsubscribe"/>';
                            }

                            ?>

                        </div>
                    </div>
                </div>

                <div class="inputSub row">
                    <div class="col-md-4 col-md-offset-4">
                        <?php

                        if($lan){
                            echo '<button id="subBut" class="warn btn btn-warning btn-lg">Odoberať!</button>';
                        }else{

                            echo '<button id="subBut" class="warn btn btn-warning btn-lg">Subscribe Now!</button>';
                        }

                        ?>

                    </div>
                </div>

                <?php

                if($lan){
                    echo '<p class="pUnsub">alebo</p>';
                    echo '<a class="unsub" href="#">Zruš odber</a>';
                }else{

                    echo '<p class="pUnsub">or</p>';
                    echo '<a class="unsub" href="#">Unsubscribe</a>';
                }
                ?>

                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="res">

                            <?php

                            if((isset($_POST["newsMail"]) && isset($_POST["newsPass"])) && $_POST["newsMail"]!=null && $_POST["newsPass"]!=null){




                                createNewsUser($_POST["newsMail"],$_POST["newsPass"],$lan);

                            }
                            if(isset($_POST["removeMail"]) && isset($_POST["removePass"])){




                                removeUser($_POST["removeMail"],$_POST["removePass"],$lan);

                            }

                            ?>
                        </div>
                    </div>
                </div>

            </form>


        </div>
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<div class="wrapper row0">
    <footer id="footer" class="hoc clear">
        <!-- ################################################################################################ -->
        <div class="center">
            <h6 class="heading" style="color: white;"><?php echo $lang['IMPORTANT_WEBSITES']; ?>:</h6>
            <a class="col-md-3 foothov" href="http://is.stuba.sk/">AIS STU</a>
            <a class="col-md-3 foothov" href="http://aladin.elf.stuba.sk/rozvrh/"><?php echo $lang['TIMETABLE']; ?></a>
            <a class="col-md-3 foothov" href="http://elearn.elf.stuba.sk/moodle/">Moodle FEI</a>
            <a class="col-md-3 foothov" href="http://www.sski.sk/webstranka/">SSKI</a>
            <a class="col-md-3 foothov" href="https://www.jedalen.stuba.sk/WebKredit/"><?php echo $lang['DINNING_ROOM']; ?></a>

            <a class="col-md-3 foothov" href="https://webmail.stuba.sk/">Webmail STU</a>
            <a class="col-md-3 foothov" href="https://kis.cvt.stuba.sk/i3/epcareports/epcarep.csp?ictx=stu&language=1"><?php echo $lang['PUBLICATIONS_EVIDENCE']; ?></a>
            <a class="col-md-3 foothov" href="http://okocasopis.sk/"><?php echo $lang['NEWSPAPER_OKO']; ?></a>

            <ul class="faico clear">
                <li><a class="faicon-facebook" href="https://www.facebook.com/UAMTFEISTU"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a class="faicon-google-plus" href="https://www.youtube.com/channel/UCo3WP2kC0AVpQMIiJR79TdA"><i
                            class="fa fa-youtube"></i></a></li>
            </ul>
        </div>


        <!-- ################################################################################################ -->
    </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
    <div id="copyright" class="hoc clear">
        <!-- ################################################################################################ -->
        <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">Domain Name</a></p>
        <p class="fl_right">Template by <a target="_blank" href="http://www.os-templates.com/"
                                           title="Free Website Templates">OS Templates</a></p>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/jquery.backtotop.js"></script>
<script src="../assets/js/jquery.mobilemenu.js"></script>
<script src="../assets/js/jquery.flexslider-min.js"></script>
<script src="../assets/js/akt.js"></script>
<script src="../assets/js/pata.js"></script>
</body>
</html>
