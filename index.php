<?php
session_start();
require "languages/lukas_functions.php";
$lang_file = setLang();
require "languages/$lang_file";
?>
<!DOCTYPE html>
<!--
Template Name: Kelaby
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<html>
<head>
        <title><?php echo $lang['MENU_HOME']; ?> | <?php echo $lang['PAGE_TITLE']; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="assets/css/layout.css" rel="stylesheet" type="text/css" media="all">
    <link rel="icon" href="assets/images/favicon.png">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
    <div id="topbar" class="hoc clear">
        <!-- ################################################################################################ -->
        <div class="fl_right">
            <ul>
                <li><a href="#"><i class="fa fa-lg fa-lock"></i></a></li>
                <li><a href="resources/intranet/index.php"><?php echo $lang['LOGIN_TITLE']; ?></a></li>
                <li class="lang_sk <?php if ($_SESSION['lang'] == 'sk') echo "fa-lg" ?>"><a
                        href="<?php echo $_SERVER['PHP_SELF'] ?>?lang=sk">SK</a></li>
                <li class="lang_en <?php if ($_SESSION['lang'] == 'en') echo "fa-lg" ?>"><a
                        href="<?php echo $_SERVER['PHP_SELF'] ?>?lang=en">EN</a></li>
            </ul>
        </div>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
    <header id="header" class="hoc clear">
        <!-- ################################################################################################ -->
        <div id="logo" class="fl_left">
            <a class="col-md-4" href="index.php"><img src="assets/images/logo_uamt.png" alt="UAMT_img"></a>
            <h2 class="col-md-8 col-md-offset-2"><?php echo $lang['PAGE_TITLE']; ?></h2>

        </div>
        <!-- ################################################################################################ -->
    </header>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row2">
    <nav id="mainav" class="hoc clear">
        <!-- ################################################################################################ -->
        <ul class="clear">
            <li class="active"><a href="index.php"><?php echo $lang['MENU_HOME']; ?></a></li>
            <li><a href="pages/aktuality.php"><?php echo $lang['MENU_NEWS']; ?></a></li>
            <li><a class="drop" href="#"><?php echo $lang['MENU_RESEARCH']; ?></a>
                <ul>
                    <li><a href="pages/vyskum/projekty.php"><?php echo $lang['MENU_PROJECTS']; ?></a></li>
                    <li><a class="drop" href="#"><?php echo $lang['MENU_RESEARCH_TOPICS']; ?></a>
                        <ul>
                            <li>
                                <a href="pages/vyskum/vyskumne_oblasti/elektricka_motokara.php"><?php echo $lang['MENU_RESEARCH_TOPICS_ELECTRIC_KART']; ?></a>
                            </li>
                            <li>
                                <a href="pages/vyskum/vyskumne_oblasti/autonomne_vozidlo.php"><?php echo $lang['MENU_RESEARCH_TOPICS_AUTONOMOUS_VEHICLE']; ?></a>
                            </li>
                            <li>
                                <a href="pages/vyskum/vyskumne_oblasti/led_kocka.php"><?php echo $lang['MENU_RESEARCH_TOPICS_3D_LED_CUBE']; ?></a>
                            </li>
                            <li>
                                <a href="pages/vyskum/vyskumne_oblasti/biomechatronika.php"><?php echo $lang['MENU_RESEARCH_TOPICS_BIOMACHATRONICS']; ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a class="drop" href="#"><?php echo $lang['MENU_ACTIVITIES'] ?></a>
                <ul>
                    <li><a href="pages/aktivity/fotogaleria.php"><?php echo $lang['MENU_ACTIVITIES_GALERY']; ?></a></li>
                    <li><a href="pages/aktivity/videa.php"><?php echo $lang['MENU_ACTIVITIES_VIDEO']; ?></a></li>
                    <li><a href="pages/aktivity/media.php"><?php echo $lang['MENU_ACTIVITIES_MEDIA']; ?></a></li>
                    <li><a class="drop" href="#"><?php echo $lang['MENU_ACTIVITIES_TEMATIC_WEBSITES']; ?></a>
                        <ul>
                            <li>
                                <a href="http://www.e-mobilita.fei.stuba.sk"><?php echo $lang['MENU_ACTIVITIES_TEMATIC_WEBSITES_ELEKTROMOBILITY']; ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="pages/pracovnici.php"><?php echo $lang['MENU_STAFF']; ?></a></li>
            <li><a href="pages/studium.php"><?php echo $lang['MENU_STUDY']; ?></a></li>
            <li><a href="pages/kontakt.php"><?php echo $lang['MENU_CONTACT']; ?></a></li>
            <li><a href="pages/oNas.php"><?php echo $lang['MENU_ABOUT']; ?></a></li>
        </ul>
        <!-- ################################################################################################ -->
    </nav>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="bgded overlay" style="background-image:url('assets/images/backgrounds/01.jpg');">
    <div id="pageintro" class="hoc clear">
        <!-- ################################################################################################ -->
        <div class="flexslider basicslider">
            <ul class="slides">
                <li>
                    <article>
                        <p>Nec imperdiet luctus</p>
                        <h3 class="heading">Odio nunc lobortis nibh ornare velit metus tortor.</h3>
                        <footer>
                            <ul class="nospace inline pushright">
                                <li><a class="btn" href="#">Vitae suspendisse</a></li>
                                <li><a class="btn inverse" href="#">Molestie semper</a></li>
                            </ul>
                        </footer>
                    </article>
                </li>
                <li>
                    <article>
                        <p>Quam risus blandit</p>
                        <h3 class="heading">felis quisque consequat porttitor elementum.</h3>
                        <footer>
                            <ul class="nospace inline pushright">
                                <li><a class="btn" href="#">Tincidunt orci</a></li>
                                <li><a class="btn inverse" href="#">Euismod a nunc</a></li>
                            </ul>
                        </footer>
                    </article>
                </li>
                <li>
                    <article>
                        <p>Ante vulputate maximus</p>
                        <h3 class="heading">Aliquam rutrum varius justo et pretium ex vestibulum.</h3>
                        <footer>
                            <ul class="nospace inline pushright">
                                <li><a class="btn" href="#">Magna consectetur</a></li>
                                <li><a class="btn inverse" href="#">Semper hendrerit</a></li>
                            </ul>
                        </footer>
                    </article>
                </li>
            </ul>
        </div>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
    <main class="hoc container clear">
        <!-- main body -->
        <!-- ################################################################################################ -->
        <ul class="nospace group services" style="margin:15px!important;">
            <li class="one_third first">
                <article>
                    <h6 class="heading font-x3"><?php echo $lang['WELCOME_TITLE']; ?></h6>
                    <p><?php echo $lang['WELCOME_INFO']; ?></p>
                    <footer><a class="btn" href="pages/oNas.php"><?php echo $lang['CLOSE_INFO']; ?></a></footer>
                </article>
            </li>
            <li class="one_third">
                <article class="bgded overlay" style="background-image:url('assets/images/backgrounds/aktuality.jpg');">
                    <div class="txtwrap"><i class="block fa fa-4x fa-ioxhost"></i>
                        <h6 class="heading"><?php echo $lang['MENU_NEWS']; ?></h6>
                        <p><?php echo $lang['NEWS_MORE']; ?></p>
                        <footer><a href="pages/aktuality.php">More &raquo;</a></footer>
                    </div>
                </article>
            </li>
            <li class="one_third">
                <article class="bgded overlay" style="background-image:url('assets/images/backgrounds/motokara.png');">
                    <div class="txtwrap"><i class="block fa fa-4x fa-ioxhost"></i>
                        <h6 class="heading"><?php echo $lang['MENU_RESEARCH_TOPICS_ELECTRIC_KART']; ?></h6>
                        <p><?php echo $lang['KART_MORE']; ?></p>
                        <footer><a href="pages/vyskum/vyskumne_oblasti/elektricka_motokara.php">More &raquo;</a></footer>
                    </div>
                </article>
            </li>
            <li class="one_third first">
                <article class="bgded overlay" style="background-image:url('assets/images/backgrounds/auto.jpg');">
                    <div class="txtwrap"><i class="block fa fa-4x fa-jsfiddle"></i>
                        <h6 class="heading"><?php echo $lang['MENU_RESEARCH_TOPICS_AUTONOMOUS_VEHICLE']; ?></h6>
                        <p><?php echo $lang['AUTONOMOUS_MORE']; ?></p>
                        <footer><a href="pages/vyskum/vyskumne_oblasti/autonomne_vozidlo.php">More &raquo;</a></footer>
                    </div>
                </article>
            </li>
            <li class="one_third">
                <article class="bgded overlay" style="background-image:url('assets/images/backgrounds/pracovnici.jpg');">
                    <div class="txtwrap"><i class="block fa fa-4x fa-language"></i>
                        <h6 class="heading"><?php echo $lang['MENU_STAFF']; ?></h6>
                        <p><?php echo $lang['STAFF_MORE']; ?></p>
                        <footer><a href="pages/pracovnici.php">More &raquo;</a></footer>
                    </div>
                </article>
            </li>
            <li class="one_third">
                <article class="bgded overlay" style="background-image:url('assets/images/backgrounds/kontakt.jpg');">
                    <div class="txtwrap"><i class="block fa fa-4x fa-lastfm"></i>
                        <h6 class="heading"><?php echo $lang['MENU_CONTACT']; ?></h6>
                        <p><?php echo $lang['CONTACT_MORE']; ?></p>
                        <footer><a href="pages/kontakt.php">More &raquo;</a></footer>
                    </div>
                </article>
            </li>
        </ul>
        <!-- ################################################################################################ -->
        <!-- / main body -->
        <div class="clear"></div>
    </main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('assets/images/backgrounds/02.jpg');">
    <div class="stmavene"></div>
    <section class="hoc container clear">
        <!-- ################################################################################################ -->
        <div class="sectiontitle center">
            <h3 class="heading"><?php echo $lang['WHY_MECHATRONICS']; ?></h3>
            <p><?php echo $lang['RESPONS_MECHATRONICS']; ?></p>
        </div>
        <ul class="nospace group center" style="margin:15px">
            <li class="one_third first">
                <article><a><i class="btmspace-30 icon fa fa-leaf"></i></a>
                    <h6 class="heading font-x1"><?php echo $lang['DEMAND_FOR_GRADUATES']; ?></h6>
                    <p><?php echo $lang['DEMAND_FOR_GRADUATES_INFO']; ?></p>
                </article>
            </li>
            <li class="one_third">
                <article><a><i class="btmspace-30 icon fa fa-leanpub"></i></a>
                    <h6 class="heading font-x1"><?php echo $lang['KNOWLEDGE_COMBINATION']; ?></h6>
                    <p><?php echo $lang['KNOWLEDGE_COMBINATION_INFO']; ?></p>
                </article>
            </li>
            <li class="one_third">
                <article><a><i class="btmspace-30 icon fa fa-anchor"></i></a>
                    <h6 class="heading font-x1"><?php echo $lang['CONNECTION_INFORMATION_TECH']; ?></h6>
                    <p><?php echo $lang['CONNECTION_INFORMATION_TECH_INFO']; ?> </p>
                </article>
            </li>
        </ul>
        <!-- ################################################################################################ -->
    </section>

</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
    <section class="hoc container clear">
        <div class="sectiontitle center">
            <h6 class="heading"><?php echo $lang['RICH_TITLE']; ?></h6>
        </div>
        <div class="group team">
            <figure class="one_quarter first"><img class="povedali" src="assets/images/backgrounds/musk.jpg" alt="musk.jpg" title="elon musk">
                <figcaption>
                    <h6 class="heading">Elon Musk</h6>
                    <em>Canadian-American business magnate, engineer and inventor.</em>
                    <footer style="margin-bottom: 30px">
                        <quote class="povedalipata">"Dessert powder toffee macaroon wafer gingerbread. Icing tootsie roll jujubes croissant."</quote>
                    </footer>
                </figcaption>
            </figure>
            <figure class="one_quarter"><img class="povedali" src="assets/images/backgrounds/gates.jpg" alt="Bill Gates" title="Bill Gates">
                <figcaption>
                    <h6 class="heading">Bill Gates</h6>
                    <em>American business magnate, investor, author and philanthropist</em>
                    <footer style="margin-bottom: 30px">
                        <quote class="povedalipata">"Sesame snaps wafer marzipan candy canes tart.  Chupa chups chocolate bar cupcake pudding."</quote>
                    </footer>
                </figcaption>
            </figure>
            <figure class="one_quarter"><img class="povedali" src="assets/images/backgrounds/wozniak.jpg" alt="Steve Wozniak" title="Steve Wozniak">
                <figcaption>
                    <h6 class="heading">Steve Wozniak</h6>
                    <em>American inventor, electronics engineer, programmer, philanthropist</em>
                    <footer style="margin-bottom: 30px">
                        <quote class="povedalipata">"Sugar plum jelly chupa chups cake. Apple pie halvah tiramisu jujubes chocolate bar."</quote>
                    </footer>
                </figcaption>
            </figure>
            <figure class="one_quarter"><img class="povedali" src="assets/images/backgrounds/robert.jpg" alt="Robert Downey Jr." title="Robert Downey Jr.">
                <figcaption>
                    <h6 class="heading">Robert Downey Jr.</h6>
                    <em>American actor, author and philanthropist</em>
                    <footer style="margin-bottom: 30px">
                        <quote class="povedalipata">"Sugar plum jelly chupa chups cake. Apple pie halvah tiramisu jujubes chocolate bar."</quote>
                    </footer>
                </figcaption>
            </figure>
        </div>
        <!-- ################################################################################################ -->
    </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper coloured overlay bgded" style="background-image:url('assets/images/backgrounds/03.jpg');">
    <div class="hoc container clear" style="margin-left: 40%;padding-bottom: 6%">
        <div id="testimonial">
            <!-- ################################################################################################ -->
            <blockquote style="border-left: none!important;"><?php echo $lang['QUOTE']; ?></blockquote>
            <strong>Robert Kiyosaki</strong> <em>American investor, businessman, self-help author and motivational speakere</em>
            <!-- ################################################################################################ -->
        </div>
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row0">
    <footer id="footer" class="hoc clear">
        <!-- ################################################################################################ -->
        <div class="center">
            <h6 class="heading" style="color: white;"><?php echo $lang['IMPORTANT_WEBSITES']; ?>:</h6>
            <a class="col-md-3 foothov" href="http://is.stuba.sk/">AIS STU</a>
            <a class="col-md-3 foothov" href="http://aladin.elf.stuba.sk/rozvrh/"><?php echo $lang['TIMETABLE']; ?></a>
            <a class="col-md-3 foothov" href="http://elearn.elf.stuba.sk/moodle/">Moodle FEI</a>
            <a class="col-md-3 foothov" href="http://www.sski.sk/webstranka/">SSKI</a>
            <a class="col-md-3 foothov" href="https://www.jedalen.stuba.sk/WebKredit/"><?php echo $lang['DINNING_ROOM']; ?></a>

            <a class="col-md-3 foothov" href="https://webmail.stuba.sk/">Webmail STU</a>
            <a class="col-md-3 foothov" href="https://kis.cvt.stuba.sk/i3/epcareports/epcarep.csp?ictx=stu&language=1"><?php echo $lang['PUBLICATIONS_EVIDENCE']; ?></a>
            <a class="col-md-3 foothov" href="http://okocasopis.sk/"><?php echo $lang['NEWSPAPER_OKO']; ?></a>

            <ul class="faico clear">
                <li><a class="faicon-facebook" href="https://www.facebook.com/UAMTFEISTU"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a class="faicon-google-plus" href="https://www.youtube.com/channel/UCo3WP2kC0AVpQMIiJR79TdA"><i
                            class="fa fa-youtube"></i></a></li>
            </ul>
        </div>


        <!-- ################################################################################################ -->
    </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
    <div id="copyright" class="hoc clear">
        <!-- ################################################################################################ -->
        <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">Domain Name</a></p>
        <p class="fl_right">Template by <a target="_blank" href="http://www.os-templates.com/"
                                           title="Free Website Templates">OS Templates</a></p>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.backtotop.js"></script>
<script src="assets/js/jquery.mobilemenu.js"></script>
<script src="assets/js/pata.js"></script>
</body>
</html>
