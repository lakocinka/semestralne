<?php
function setLang(){
    header('Cache-control: private'); // IE 6 FIX
    if (isset($_GET['lang'])) {
        $lang = $_GET['lang'];
        $_SESSION['lang'] = $lang;
    } else if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
    } else {
        $_SESSION['lang'] = 'sk';
        $lang = 'sk';
    }

    switch ($lang) {
        case 'en':
            $lang_file = 'lang.en.php';
            break;
        case 'sk':
            $lang_file = 'lang.sk.php';
            break;
        default:
            $lang_file = 'lang.sk.php';
    }
    return $lang_file;
}
