<?php

$lang = array();
$lang['PAGE_TITLE'] = 'Institute of Automotive Mechatronics FEI STU';
$lang['LOGIN_TITLE'] = 'Login';
// Menu
$lang['MENU_HOME'] = 'Home';
$lang['MENU_NEWS'] = 'News';
// Menu - Vyskum - dropdown
$lang['MENU_RESEARCH'] = 'Research';
$lang['MENU_PROJECTS'] = 'Projects';
$lang['MENU_RESEARCH_TOPICS'] = 'Research topics';
$lang['MENU_RESEARCH_TOPICS_ELECTRIC_KART'] = 'Electric kart';
$lang['MENU_RESEARCH_TOPICS_AUTONOMOUS_VEHICLE'] = 'Autonomous vehicle';
$lang['MENU_RESEARCH_TOPICS_3D_LED_CUBE'] = '3D LED cube';
$lang['MENU_RESEARCH_TOPICS_BIOMACHATRONICS'] = 'Biomechatronics';
// Menu - Aktivity - dropdown
$lang['MENU_ACTIVITIES'] = 'Activities';
$lang['MENU_ACTIVITIES_GALERY'] = 'Photos';
$lang['MENU_ACTIVITIES_MEDIA'] = 'Media';
$lang['MENU_ACTIVITIES_VIDEO'] = 'Videos';
$lang['MENU_ACTIVITIES_SHOW_MORE'] = 'Show more events';
$lang['MENU_ACTIVITIES_SHOW_VIDEOS'] = 'Show more videos';
$lang['MENU_ACTIVITIES_SHOW_MEDIA'] = 'Show more records';
$lang['MENU_ACTIVITIES_TEMATIC_WEBSITES'] = 'Our thematic websites';
$lang['MENU_ACTIVITIES_TEMATIC_WEBSITES_ELEKTROMOBILITY'] = 'Elektromobility';
// Menu - Pracovnici
$lang['MENU_STAFF'] = 'Staff';
// Menu - Studium
$lang['MENU_STUDY'] = 'Study';
// Menu - Kontakt
$lang['MENU_CONTACT'] = 'Contact';
$lang['SCHOOL'] = 'FEI STU';
$lang['ADDRESS'] = '812 19 Bratislava, Slovak republic';
$lang['ROOM'] = 'Room number: D116';
$lang['MAP_TITLE'] = 'You will find us';
$lang['MAP_BOTTOMTITLE'] = 'We are looking forward to you';
// Menu - O nas
$lang['MENU_ABOUT'] = 'About us';
// Sidebar - Studium
$lang['SIDEBAR_CANDIDATE'] = 'For candidates';
$lang['BACH_STUDY'] = 'Bachelor\'s degree';
$lang['MASTER_ENG_STUDY'] = 'Master of Engineering';
$lang['PHD_STUDY'] = 'Doctor\'s degree';
$lang['GENERAL_INFO'] = 'General information';
$lang['BC_THESES'] = 'Bachelor\'s theses';
$lang['ENG_THESES'] = 'Graduation theses';
$lang['INSTRUCTIONS'] = 'Instructions';
$lang['FREE_THESES'] = 'Available theses';
// Footer
$lang['IMPORTANT_WEBSITES'] = 'Important websites';
$lang['NEWSPAPER_OKO'] = 'Newspaper OKO';
$lang['DINNING_ROOM'] = 'Dinning room STU';
$lang['TIMETABLE'] = 'Timetable FEI';
$lang['PUBLICATIONS_EVIDENCE'] = 'Evidence of publications STU';
// Staff table
$lang['TABLE_NAME'] = 'Name';
$lang['TABLE_ROOM'] = 'Room';
$lang['TABLE_CLACK'] = 'Clack';
$lang['TABLE_DEPARTMENT'] = 'Department';
$lang['TABLE_ROLE'] = 'Role';
$lang['TABLE_FUNCTION'] = 'Function';
$lang['PH_DEP'] = 'Find by depart.';
$lang['PH_ROLE'] = 'Find by role';
$lang['MODAL_TITLE_STAFF_INFO'] = 'Staff member info';
// Homepage
$lang['QUOTE'] = '"Your real education begins when you step out the door"';
$lang['RICH_TITLE'] = 'They said about us';
$lang['DEMAND_FOR_GRADUATES'] = 'Demand for graduates';
$lang['DEMAND_FOR_GRADUATES_INFO'] = 'Graduates in mechatronics and automation are still relatively rare on the market. Employers are aware of their professional qualities and way of thinking.';
$lang['KNOWLEDGE_COMBINATION'] = 'Knowledge combination';
$lang['KNOWLEDGE_COMBINATION_INFO'] = 'Graduates in mechatronics and automation are still relatively rare on the market. Employers are aware of their professional qualities and way of thinking.';
$lang['CONNECTION_INFORMATION_TECH'] = 'Connection to information technology';
$lang['CONNECTION_INFORMATION_TECH_INFO'] = 'Yes! Mechatronics combines information and communication technologies, electronic systems, mechanics and automation into one unit. Therefore, without mechatronics, mechatronic equipment is not used today.';
$lang['WHY_MECHATRONICS'] = 'Why study mechatronics?';
$lang['RESPONS_MECHATRONICS'] = 'Because mechatronics is <strong>cool</strong>!';
$lang['NEWS_MORE'] = 'For news detail click here';
$lang['KART_MORE'] = 'For electric kart detail click here';
$lang['AUTONOMOUS_MORE'] = 'For autonomous vehicle detail click here';
$lang['STAFF_MORE'] = 'For staff detail click here';
$lang['CONTACT_MORE'] = 'For contact detail click here';
$lang['CLOSE_INFO'] = 'More info';
$lang['WELCOME_INFO'] = 'Who are we? History, Leadership of the Constitution and constitutions are found here.';
$lang['WELCOME_TITLE'] = 'Welcome to UAMT website';








