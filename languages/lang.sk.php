<?php

$lang = array();
$lang['PAGE_TITLE'] = 'Ústav automobilovej mechatroniky FEI STU';
$lang['LOGIN_TITLE'] = 'Prihlásiť sa';
// Menu
$lang['MENU_HOME'] = 'Domov';
$lang['MENU_NEWS'] = 'Aktuality';
// Menu - Vyskum - dropdown
$lang['MENU_RESEARCH'] = 'Výskum';
$lang['MENU_PROJECTS'] = 'Projekty';
$lang['MENU_RESEARCH_TOPICS'] = 'Výskumné oblasti';
$lang['MENU_RESEARCH_TOPICS_ELECTRIC_KART'] = 'Elektrická motokára';
$lang['MENU_RESEARCH_TOPICS_AUTONOMOUS_VEHICLE'] = 'Autonómne vozidlo';
$lang['MENU_RESEARCH_TOPICS_3D_LED_CUBE'] = '3D LED kocka';
$lang['MENU_RESEARCH_TOPICS_BIOMACHATRONICS'] = 'Biomechatronika';
// Menu - Aktivity - dropdown
$lang['MENU_ACTIVITIES'] = 'Aktivity';
$lang['MENU_ACTIVITIES_GALERY'] = 'Fotogaléria';
$lang['MENU_ACTIVITIES_MEDIA'] = 'Média';
$lang['MENU_ACTIVITIES_VIDEO'] = 'Videá';
$lang['MENU_ACTIVITIES_SHOW_MORE'] = 'Viac udalostí';
$lang['MENU_ACTIVITIES_SHOW_VIDEOS'] = 'Viac videí';
$lang['MENU_ACTIVITIES_SHOW_MEDIA'] = 'Viac záznamov';
$lang['MENU_ACTIVITIES_TEMATIC_WEBSITES'] = 'Naše tématické web stránky';
$lang['MENU_ACTIVITIES_TEMATIC_WEBSITES_ELEKTROMOBILITY'] = 'Elektromobilita';
// Menu - Pracovnici
$lang['MENU_STAFF'] = 'Pracovníci';
// Menu - Studium
$lang['MENU_STUDY'] = 'Štúdium';
// Menu - Kontakt
$lang['MENU_CONTACT'] = 'Kontakt';
$lang['SCHOOL'] = 'FEI STU';
$lang['ADDRESS'] = '812 19 Bratislava, Slovenská republika';
$lang['ROOM'] = 'Číslo miestnosti: D116';
$lang['MAP_TITLE'] = 'Nájdete nás';
$lang['MAP_BOTTOMTITLE'] = 'Tešíme sa na vás';
// Menu - O nas
$lang['MENU_ABOUT'] = 'O nás';
//Sidebar - Studium
$lang['SIDEBAR_CANDIDATE'] = 'Pre uchádzačov';
$lang['BACH_STUDY'] = 'Bakalárske štúdium';
$lang['MASTER_ENG_STUDY'] = 'Inžinierske štúdium';
$lang['PHD_STUDY'] = 'Doktorandské štúdium';
$lang['GENERAL_INFO'] = 'Všeobecné informácie';
$lang['BC_THESES'] = 'Bakalárske práce';
$lang['ENG_THESES'] = 'Diplomové práce';
$lang['INSTRUCTIONS'] = 'Pokyny';
$lang['FREE_THESES'] = 'Voľné témy';
// Footer
$lang['IMPORTANT_WEBSITES'] = 'Dôležité stránky';
$lang['NEWSPAPER_OKO'] = 'Časopis OKO';
$lang['DINNING_ROOM'] = 'Jedáleň STU';
$lang['TIMETABLE'] = 'Rozvrh hodín FEI';
$lang['PUBLICATIONS_EVIDENCE'] = 'Evidencia publikácií STU';
// Staff table
$lang['TABLE_NAME'] = 'Meno';
$lang['TABLE_ROOM'] = 'Miestnosť';
$lang['TABLE_CLACK'] = 'Klapka';
$lang['TABLE_DEPARTMENT'] = 'Oddelenie';
$lang['TABLE_ROLE'] = 'Zaradenie';
$lang['TABLE_FUNCTION'] = 'Funkcia';
$lang['PH_DEP'] = 'Zadaj oddelenie';
$lang['PH_ROLE'] = 'Zadaj zaradenie';
$lang['MODAL_TITLE_STAFF_INFO'] = 'Informácie o pracovníkovi';
// Homepage
$lang['QUOTE'] = '"Vaše skutočné vzdelanie začína v momente, keď opustíte brány školy"';
$lang['RICH_TITLE'] = 'Povedali o nás';
$lang['DEMAND_FOR_GRADUATES'] = 'Dopyt po absolventoch';
$lang['DEMAND_FOR_GRADUATES_INFO'] = 'Absolventi mechatroniky a automatizácie sú na trhu stále pomerne ojedinelí. Zamestnávatelia sú si však vedomí ich odborných kvalít a spôsobu zmýšľania.';
$lang['KNOWLEDGE_COMBINATION'] = 'Kombinácia vedomostí';
$lang['KNOWLEDGE_COMBINATION_INFO'] = 'Osvojenie si diskrétnych informatických a zároveň spojitých automatizérskych prístupov je pre pracovný trh priam vražedná kombinácia. Z absolventa sa takto stáva univerzálnejší človek.';
$lang['CONNECTION_INFORMATION_TECH'] = 'Prepojenie s informatikou';
$lang['CONNECTION_INFORMATION_TECH_INFO'] = 'Áno! Mechatronika spája informačno-komunikačné technológie, elektronické systémy, mechaniku a automatizáciu do jedného celku. Bez informatiky sa teda mechatronické zariadenie v dnešnej dobe nezaobíde.';
$lang['WHY_MECHATRONICS'] = 'Prečo študovať mechatroniku?';
$lang['RESPONS_MECHATRONICS'] = 'Pretože mechatronika je <strong>cool</strong>!';
$lang['NEWS_MORE'] = 'Pre detailné aktuality kliknite sem';
$lang['KART_MORE'] = 'Pre detail elektrickej motokóry kliknite sem';
$lang['AUTONOMOUS_MORE'] = 'Pre detail autonómneho vozidla kliknite sem';
$lang['STAFF_MORE'] = 'Pre detail pracovníkov kliknite sem';
$lang['CONTACT_MORE'] = 'Pre detail kontaktu kliknite sem';
$lang['CLOSE_INFO'] = 'Bližšie informácie';
$lang['WELCOME_INFO'] = 'Kto vlasnte sme? História, vedenie ústavu a jednotlivé oddelenia nájdete po kliknutí na tlačidlo nižšie.';
$lang['WELCOME_TITLE'] = 'Vitajte na stránke UAMT';



