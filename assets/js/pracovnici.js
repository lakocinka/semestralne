/**
 * Created by patrik on 21.05.2017.
 */
$(document).ready(function () {

});
function sortTable(tableindex) {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("tabulka");
    switching = true;
    /*Make a loop that will continue until
     no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
         first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
             one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[tableindex];
            y = rows[i + 1].getElementsByTagName("TD")[tableindex];
            //check if the two rows should switch place:
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
             and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}


function myFunction() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("input1");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabulka");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[3];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function myFunction2() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("input2");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabulka");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[4];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

$(".myBtn").click(function (e) {
    printdata($(this).data("id"));
});

function printdata(klapka) {
    //console.log("https://restcountries.eu/rest/v2/name/"+stat);
    $.ajax({
        url: "../pages/functions/pracovnici_functions.php",
        data: {k: klapka},
        type: "POST",
        success: function (response) {
            //$("#photo").append("sASs");
            var json = JSON.parse(response);
            $('#photo').empty();
            console.log(json['photo']);
            $('#photo').append("<img style='width: 70%;display: block;margin-left: auto;margin-right: auto;' id='theImg' src='" + json['photo'] + "'>");
            //<img src="https://www.w3schools.com/images/w3schools_green.jpg" alt="W3Schools.com">
            $('#meno').empty();
            if (json['title1']) {
                $("#meno").append(json['title1'] + " " + json['firstname'] + " " + json['lastname']);
            } else  $("#meno").append(json['firstname'] + " " + json['lastname']);

            if (json['title2']) {
                $("#meno").append(" " + json['title2']);
            }

            $('#zaradenie').empty();
            $("#zaradenie").append(json['staffRole']);
            $('#oddelenie').empty();
            $("#oddelenie").append(json['department']);
            $('#telefon').empty();
            $("#telefon").append("+421 2 60291 " + json['phone']);
            $('#miestnost').empty();
            $("#miestnost").append(json['room']);

            //console.log(json['ldapLogin']);
            getpublikacie(json['ldapLogin']);


        },
        error: function () {
            console.log("Chyba");
        }
    });
}

function getpublikacie(ldap) {
    $.ajax({
        url: "../pages/functions/pracovnici_functions.php",
        data: {"l": ldap},
        type: "POST",
        success: function (response) {
            //console.log(response);
            if (response) {
                $("#publikacie").empty();
                $("#publikacie").append(response);
                $("#publikacie").css("display", "block");
            } else {
                $("#publikacie").css("display", "none");
            }
            $("#tabulecka").DataTable();
        },
        error: function () {
            console.log("Chyba");
        }
    });
}
