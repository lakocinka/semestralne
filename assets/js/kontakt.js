/**
 * Created by patrik on 20.05.2017.
 */


function initMap() {
    var myLatLng = {lat: 48.151965, lng: 17.072995};


    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: myLatLng
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
    });
}

