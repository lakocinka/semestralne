(function (w, d) {

    var path = "../../assets/images/activities/";
    var path_media = "../../assets/media/";
    var data_array = new Array();
    var order = 0;

    $('#gallery_more_photos').click(function () {
        galleryMethod(data_array);
    });

    $('#gallery_more_videos').click(function () {
        videoMethod(data_array);
    });

    $('#gallery_more_media').click(function () {
        mediaMethod(data_array);
    });

    function ajaxMethod(tableName, otherMethod) {
        $.ajax({
            type: 'POST',
            url: 'read-DB.php',
            data: {'table': tableName},
            dataType: 'json',
            success: function (data) {
                data_array = data;
                otherMethod(data);
            },
            error: function (msg) {
                alert("Error\n " + JSON.stringify(msg));
                console.log(JSON.stringify(msg));
            }
        });
    }

    var galleryMethod = function (gallery) {
        if (gallery.length == 0) {
            $(".tz-gallery").append("<div class=\"row\"> <div class=\"col-sm-12 col-md-12\">" +
                "<h1 class='gallery__title'> No gallery was found </h1></div></div>");
            $('#gallery_more_photos').fadeOut();
            return;
        }

        if (order < 1 && data_array.length <= 2) {
            $('#gallery_more_photos').fadeOut();
        }

        var events = [];

        gallery.forEach(function (element, index) {
            ++order;
            if (index > 1 || (index > 0 && order - index >= 2)) {
                order--;
                return;
            }

            $.ajax({
                url: path + element.path,
                success: function (data) {
                    var row = "<div class=\"col-sm-12 col-md-12\">" +
                        "<h1 class='gallery__title'> " + element.title + "<small> - " + element.date + "</small></h1></div>";
                    var data_row = "";
                    $(data).find("a").attr("href", function (i, val) {
                        if (val.toLocaleLowerCase().match(/\.(jpe?g|png|gif)$/)) {
                            data_row += "<div class=\"col-sm-12 col-md-4\"><div class='gallery__image'>" +
                                "<a class=\"lightbox\" href=\"" + path + element.path + '/' + val + "\">" +
                                "<img src=\"" + path + element.path + '/' + val + "\" alt=\"" + element.title + "\"></a></div></div>";
                        }
                    });

                    if (data_row) {
                        $(".tz-gallery").append("<div class=\"row row--" + order + "\">" + row + data_row + "</div>");
                        baguetteBox.run('.tz-gallery', {
                            captions: function (element) {
                                return element.getElementsByTagName('img')[0].alt;
                            },
                            buttons: 'auto',
                            async: false,
                            preload: 2,
                            animation: 'slideIn'
                        });
                    }

                    data_array.shift(); //remove first element

                    if (data_array.length == 0) {
                        $('#gallery_more_photos').fadeOut();
                    }
                },
                error: function (msg) {
                    alert("Errors\n " + JSON.stringify(msg));
                }
            });
        });
    }

    var videoMethod = function (videos) {
        var data_row = "";
        if (data_array.length <= 2) {
            $('#gallery_more_videos').hide();
        }

        $.each(videos, function (i, element) {
            if (i >= 2) {
                return;
            }
            data_row += "<div class=\"col-md-6 frame\">" +
                "<h1> " + element.title + " -&nbsp;<small>" + element.date + "</small></h1>" +
                "<div class=\"embed-responsive embed-responsive-16by9\">" +
                "<iframe src=\"" + element.path + "\" frameborder='0' allowfullscreen='1'></iframe></div></div>";
        });

        data_array = data_array.slice(2, data_array.length);

        if (data_array.length == 0) {
            $('#gallery_more_videos').hide();
        }
        if (data_row) {
            $(".video-gallery").append("<div class='row'>" + data_row + "</div>");
        }
    }

    var mediaMethod = function (media) {
        if (data_array.length <= 5) {
            $('#gallery_more_media').hide();
        }
        var data_row = [];
        $.each(media, function (i, element) {
            if (i >= 6) {
                return;
            }
            var col_md = 6;
            var content = "";
            if (element.title.length > 35) {
                col_md = 12;
                if (data_row.length > 0 && data_row[i - 1] == 6 && data_row[i - 2] != 6) {
                    data_row[i - 1].data_row = data_row[i - 1].data_row.replace(new RegExp("[0-9]"), "12");
                    data_row[i - 1].col_md = 12;
                }
            }
            var add_path = path_media;
            if (element.type.toLocaleLowerCase() == "youtube" || element.type.toLocaleLowerCase() == "web") {
                add_path = "";
            }

            var object = {
                "col_md": col_md,
                "data_row": "<div class='col-md-" + col_md + " list-group'>" +
                "<a href=\"" + add_path + element.path + "\" class=\"list-group-item list-group-item-action flex-column align-items-start\">" +
                " <div class=\"d-flex w-100 justify-content-between\">" +
                " <h5 class=\"mb-1\">" + element.title + "</h5>" +
                " <span class=\"badge badge-default badge-pill\"> " + element.type + " </span>" +
                " <small>" + element.date + "</small>" + content + "</div> </a> </div>"
            };
            data_row.push(object);
            ++order;
        });

        data_array = data_array.slice(6, data_array.length);

        if (data_array.length == 0) {
            $('#gallery_more_videos').hide();
        }
        if (data_row.length) {
            $.each(data_row, function (i, e) {
                $(".media-gallery .row").append(e.data_row);
            });
        }
    }

    switch (w.location.pathname.split("/").pop()) {
        case 'videa.php':
            ajaxMethod('videos', videoMethod);
            break;
        case 'fotogaleria.php':
            ajaxMethod('photogallery', galleryMethod);
            break;
        case 'media.php':
            ajaxMethod('media', mediaMethod);
            break;
        default:
            break;
    }

}(window, document));
