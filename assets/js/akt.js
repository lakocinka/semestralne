/**
 * Created by Tomáš on 21.05.2017.
 */


$(document).ready(function () {



    var categor = getParameterByName('category'); // "lorem"
    var opti = getParameterByName('opt'); // "" (present with empty value)
    var page = getParameterByName('page'); // "" (present with empty value)


   if(categor !=null && opti !=null){

       $("#event").val(categor);
       if(opti==1){

           $("#opt1").attr("checked",true);

       }else {
           $("#opt2").attr("checked", true);

       }
   }

   if(page !=null){

       var t=$(".pagination li:nth-child("+page+")");
       t=t.children();
       console.log(t);
       t.addClass("select")

   }else{

       var t=$(".pagination li:nth-child(1)");
       t=t.children();
       t.addClass("select");
   }

    var numNav=$(".pagination").children().length;

    if(numNav>5){

        var start =page-3;
        var end=page+1;
        for(var j=0;j<numNav;j++)
        {
            if(j>start && j<end) {
                continue;
            }else {
                var tmp = $(".pagination").children()[j];
                $(tmp).hide();
            }
        }
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $('.btnFilter').on('click', function (evt) {
        var kat = $('#event').val();
        var opt1 = $('#opt1').is(':checked');
        var opt2 = $('#opt2').is(':checked');
        var num;
        console.log(opt1);
        console.log(opt2);

        if (opt1) {

            num = 1;


        } else {

            num = 2;

        }
        console.log("funfilter");

        console.log("toto je kat v js " + kat);
        console.log("toto je num v js " + num);

        $.ajax({
            url: "../pages/aktuality.php",
            type: "GET",
            data: {
                category: kat,
                opt: num
            },
            success: function (response) {

               // console.log($(".hookArt", $(response)));
               //  $(".hookArt").remove();
               //  $(".page-nav").remove();
               //
               //  var resp = $(".hookArt", $(response));
               //  var pageNav = $(".page-nav", $(response));
               //
               //  console.log(pageNav);
               //
               //
               //  for (var i = 0; i < resp.length; i++) {
               //      $(".header-input").after("<div class='hookArt row'>" + resp["" + i].innerHTML + "</div>");
               //
               //  }
               //
               //  $('.hookArt').last().after("<div class='page-nav row'>" + pageNav["0"].innerHTML + "</div>");
               //
               //  var categor = getParameterByName('category'); // "lorem"
               //  var opti = getParameterByName('opt'); // "" (present with empty value)
               //  var page = getParameterByName('page'); // "" (present with empty value)

                /* var numNav=$(".pagination").children().length;

                 if(numNav>5){

                 var start =pageNav-3;
                 var end=pageNav+1;
                 for(var j=0;j<numNav)

                 }*/

                var href=getBaseUrl();

                href=href+"aktuality.php?page=1&category="+kat+"&opt="+num;

                window.location.replace(href);


            }


        })

    });

    $('#subBut').on('click',function (evt) {

        var mail=$('#email').val();
        var pass=$('#pass').val();

        evt.preventDefault();

        if(mail == "" || pass == ""){

            var lang=getParameterByName("lang");

            $('.res').empty();
            if(lang=="sk") {
                $('.res').append('<div class="alert alert-danger"> <strong>Pozor!</strong> Nezadané heslo alebo email. </div>');

                setTimeout(function(){ $('.res').empty();}, 5000);
            }else{
                $('.res').append('<div class="alert alert-danger"> <strong>Warning!</strong>  Fill email and password!  </div>');
                setTimeout(function(){ $('.res').empty();}, 5000);
            }

            return;
        }
        $.ajax({
            url:"../pages/aktuality.php",
            type:"POST",
            data:{
                newsMail:mail,
                newsPass:pass
            },
            success:function (response) {

                //console.log(response);
                var data=$('.res',$(response));
                 console.log(data[0].innerHTML);

                 $('.res').empty();
                 $('.res').append('<div class="alert alert-info">'+data[0].innerHTML+'</div>');
                setTimeout(function(){ $('.res').empty();}, 5000);

            }
        })

    });

    $('.unsub').on('click',function (evt) {


        evt.preventDefault();

        var mail=$('#email').val();
        var pass=$('#pass').val();



        if(mail == "" || pass == ""){

            var lang=getParameterByName("lang");

            $('.res').empty();
            if(lang=="sk") {
                $('.res').append('<div class="alert alert-danger"> <strong>Pozor!</strong> Nezadané heslo alebo email. </div>');
                setTimeout(function(){ $('.res').empty();}, 5000);
            }else{
                $('.res').append('<div class="alert alert-danger"> <strong>Warning!</strong> Fill email and password! </div>');
                setTimeout(function(){ $('.res').empty();}, 5000);
            }

            return;
        }
        $.ajax({
            url:"../pages/aktuality.php",
            type:"POST",
            data:{
                removeMail:mail,
                removePass:pass
            },
            success:function (response) {

                //console.log(response);
                var data=$('.res',$(response));
               // console.log(data[0].innerHTML);
                console.log(response);
                $('.res').empty();
                $('.res').append('<div class="alert alert-info">'+data[0].innerHTML+'</div>');
                setTimeout(function(){ $('.res').empty();}, 5000);

            }
        })

    });


    function getBaseUrl() {
        var re = new RegExp(/^.*\//);
        return re.exec(window.location.href);
    }
});