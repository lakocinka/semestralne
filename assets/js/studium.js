/**
 * Created by Lukas on 24.5.2017.
 */

function openInfo(evt, infoName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(infoName).style.display = "block";
    evt.currentTarget.className += " active";
}
tabcontent = document.getElementsByClassName("tabcontent");
for (var i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
}
document.getElementById("defaultOpen").click();

$(document).ready(function () {
    print_bakalarky(1);
    print_diplomovky(2);
});

function print_bakalarky(bak) {
    $.ajax({
        url: "../pages/functions/studium_functions.php",
        data: {b: bak},
        type: "POST",
        success: function (response) {
            $("#bakalarky").empty();
            $("#bakalarky").append(response);

            $("#tabulka25").DataTable();
        },
        error: function () {
            console.log("Chyba");
        }
    });
}

function print_diplomovky(dip) {
    $.ajax({
        url: "../pages/functions/studium_functions.php",
        data: {d: dip},
        type: "POST",
        success: function (response) {
            $("#diplomovky").empty();
            $("#diplomovky").append(response);

            $("#tabulka26").DataTable();
        },
        error: function () {
            console.log("Chyba");
        }
    });
}
